## Version info
VERSION="1.2-2"
DATE=2025-01-09

## POSIX compliant extraction of the year (${DATE:0:4} is not)
YEAR=$(echo "${DATE}" | awk '{ s = substr($0, 1, 4); print s; }')

## Immutable global variables
SCRIPTNAME=$(basename -- "$0")
PREFIX="$(cd -- "$(dirname "$0")/.." >/dev/null 2>&1 || exit; pwd -P)"
DOC_REQUEST=70

## Global variables exported to be available to all child processes.
export ROGER_WD="${PWD}"
export ROGER_ERRORPROMPT="! "
export ROGER_WARNINGPROMPT="> "
export ROGER_NOTEPROMPT="  "
export ROGER_ERRORCODE=71
export ROGER_WARNINGCODE=72
export ROGER_NOTECODE=73

## Path to validation and grading modules. The modules reside in
## system and user libraries identified by the environment variables
## 'ROGER_LIBS' and 'ROGER_LIBS_USER', respectively.
##
## By default, 'ROGER_LIBS' is set to 'PREFIX/lib/roger', where this
## script is in 'PREFIX/bin' (usually '/usr/local').
if [ -z "${ROGER_LIBS:+x}" ]
then
    export ROGER_LIBS="${PREFIX}/lib/${SCRIPTNAME}"
fi

###
### Main documentation
###
if [ $# -eq 0 ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]
then
    ## main script usage information
    print_msg "$(eval_gettext "Usage: \${SCRIPTNAME} [options]
   or: \${SCRIPTNAME} checkreq|clone|grade|harvest|push|switch|validate
             [arguments]

Start Roger the Omni Grader with the specified options, or invoke one
of its tools.

Options:
  -h, --help     Print this help text
  --help-config  Print help on configuration
  --help-module  Print help on grading and validation modules
  --version      Print version info

Tools:
  checkreq    Check for the availability of grading tools
  clone       Clone Git repositories to grade
  grade       Grade projects
  harvest     Collect and summarize grading results
  push        Push grading results back to Git repositories
  switch      Switch branches in Git repositories
  validate    Validate the repository and files of a project

Please use '\${SCRIPTNAME} tool --help' to obtain further information about
the usage of 'tool'.")\n"
    exit ${DOC_REQUEST}
fi

###
### Configuration documentation
###
if [ "$1" = "--help-config" ]
then
    ## configuration information
    print_msg "$(eval_gettext "GRADING AND VALIDATION CONFIGURATION

The configuration file of \${SCRIPTNAME} lays the grading and validation maps
using a hierarchical key-value system.

The general format of the configuration file is as follows:

node
  key: value

Nodes can contain many key-value pairs and can be nested any number of
levels deep. Terminal nodes require a key-value pair. An indent of
*two* spaces is mandatory to create a new node level.

Lines starting with the symbol '#' (possibly preceded by whitespace) 
and blank lines are ignored in mapping. Quoting rules of the Unix 
shell apply. In particular, any single quote (or apostrophe) or double
quote that should be retained as part of a node, key or value name 
needs to be escaped with the symbol '\'.

The configuration must contain a special node named '__global'
structured as follows:

__global
  title: <title of the project>
  repository
    check_repos: <true|false>
    project_name: <name>
    pattern_name: <pattern>
    main_branch: <name>
    tag_name: <name>
  defaults
    check_exists_laden: <true|false>
    check_contents: <true|false>
    check_contents_module: <filename> [<args>]
    check_encoding: <true|false>

The 'title' key specifies the title of the project.

The presence of the 'repository' node indicates that the directory is
a Git repository. The key 'check_repos' indicates to check that the
directory is a valid Git repository, that is, at the minimum: the main
branch exists; the repository is not empty. The 'project_name' key
indicates to check that the repository is hosted in a Git project of
the specified name. The 'pattern_name' key indicates to check that the
name of the repository matches the specified extended regular
expression pattern. The key 'main_branch' indicates the name of the
main branch, if different from 'main' or 'master'. The key 'tag_name'
indicates to process the repository in its state at a specific tag.

The 'defaults' node allows to specify default values for keys when
they are omitted from node configuration. The keys are documented
below.

The other nodes of the configuration identify elements to grade or
validate using the following structure:

<node>
  description: <description>
  recursive: <true|false>
  subtotal: <true|false>
  check_branch: <true|false>
  check_exists_laden: <true|false>
  check_contents: <true|false>
  check_contents_module: <filename> [<args>]
  check_encoding: <true|false>
  points_encoding: <value>
  <criteria>
    description: <description>
    grade_criteria_module: <filename> [<args>]
    tests: <filename>
    points: <value>

The 'description' key, if present, replaces the name of a node in
results. The key 'subtotal' indicates that the node is a subtotal in
aggregate results; see '\${SCRIPTNAME} harvest --help' for details. The key
'recursive' indicates that the node is recursive; see below. The key
'check_branch' indicates that the node is a branch that should exist
in the Git repository. The key 'check_exists_laden' indicates that the
node is a file or a directory that should exist, not be empty, and be
tracked by Git, if applicable. The key 'check_contents' indicates to
check the contents of the node (usually the source code in a file or
directory) using the validation module specified in the key
'check_contents_module'. The key 'check_encoding' should be used for
files only; it indicates to check that the character encoding of the
file is ASCII or UTF-8. In grading, the key 'points_encoding'
specifies the number of points attributed to this criteria.

The presence of a 'points' key in a node identifies it as a grading
criteria. A criteria node is terminal: any sub-node is ignored. Its
name is not written in the results. A criteria is graded automatically
using the grading module specified in the key 'grade_criteria_module'
and the unit tests file specified in the key 'tests'.

The nodes are processed in lexicographic order. A prefix 'n-', where 'n'
is an integer (with or without leading zeros) appended to their names
modifies the order. The prefix is otherwise ignored in processing and
in printing.

Although nodes are presented in a hierarchical structure, the actual
objects they represent (e.g. files and directories) must appear at the
root of the directory. Recursive nodes, on the other hand, may contain
the objects in the nodes below. Therefore, a map such as

node_a
  recursive: true
  node_b

would identify a subdirectory 'node_a' containing a file 'node_b'.
Recursive nodes may be nested.

See the tool-specific documentation for the list of nodes and keys
that are required, optional, and ignored for this tool.

See '\${SCRIPTNAME} --help-module' for details on grading and validation
modules.")\n"
    exit ${DOC_REQUEST}
fi

###
### Modules documentation
###
if [ "$1" = "--help-module" ]
then
    ## modules information
    print_msg "$(eval_gettext "GRADING AND VALIDATION MODULES

Grading and validation modules are shell scripts that run automatic
tests on source code or other resources. Grading modules run unit
tests and report results for grading criterias. Validation modules
check if requirements are met or not.

MODULE LIBRARIES

\${SCRIPTNAME} looks for modules in the current directory, in the
user library identified by the environment variable 'ROGER_LIBS_USER',
and finally in the system library identified by the environment
variable 'ROGER_LIBS'. These environment variables contain paths
separated by the character ':'.

By default, 'ROGER_LIBS' is set to 'PREFIX/lib/\${SCRIPTNAME}', where
'\${SCRIPTNAME}' is in 'PREFIX/bin', and 'ROGER_LIBS_USER' is unset.

GRADING MODULES

A grading module takes the name of a tests file in argument and
outputs two values to the standard output, separated by one or many
spaces: the number of tests passed and the number of tests executed,
in this order, or '0 0'. The module also writes diagnostic messages to
the standard error and returns an exit code that identifies the type 
of diagnostic messages, if any; see 'Exit codes' below.

A module may also accept its own arguments before the tests file name.
These are passed to the module through the configuration file.

The system provides the following grading modules:

- grade-r-script: reports the validity of the unit tests in argument
  using function 'run_test_file' from the R package tinytest in an R
  session started with '--quiet --no-restore --nosave --no-echo'.
  Default module for tests files with an extension '.R' or '.r'.
- grade-shell-script: reports the validity of the unit tests in
  argument in a new instance of the shell mentioned in the shebang, if
  any. By default, '/bin/sh' is used; the other supported shells are
  '/bin/bash' and '/bin/zsh'. Default module for tests files with an
  extension '.sh'.
- grade-repos-authors: writes the list of unique authors in a Git
  repository to the standard output, sorted alphabetically, omitting 
  names matching the extended regular expression pattern in argument. 
  This module does not use the tests file name.
- grade-repos-commits: compares the number of commits in a Git
  repository to the target specified in argument. This module does not
  use the tests file name.

VALIDATION MODULES

A validation module takes the current node name in argument, writes
progress messages to the standard output, diagnostic messages to the
standard error, and returns an exit code that identifies the type of
diagnostic messages, if any; see 'Exit codes' below.

A module may also accept its own arguments before the node name. These
are passed to the module through the configuration file.

The system provides the following validation modules:

- validate-r-script: checks that the R script in argument is portable
  and parses with 'source' in an R session started with '--quiet
  --no-restore --nosave'. Default module for files with an extension
  '.R' or '.r'.
- validate-r-rmarkdown: checks that the R Markdown file in argument
  renders without errors in an R session started with '--quiet
  --no-restore --nosave'. Default module for files with an extension
  '.Rmd' or '.rmd'.
- validate-r-shiny: checks that the compilation of the Shiny app in
  argument (a directory name) completes without errors in an R session
  started with '--quiet --no-restore --nosave'.
- validate-r-package: checks that validation of the R package in the
  in argument (a directory name) with 'R CMD check' completes without
  errors or warnings.
- validate-shell-script: checks the syntax of the shell script in
  argument with the shell mentioned in the shebang ('/bin/sh',
  '/bin/bash' and '/bin/zsh' are supported). Default module for files
  with an extension '.sh'.
- validate-repos-authors: writes the list of unique authors in a Git
  repository to the standard output, sorted alphabetically, omitting
  names matching the extended regular expression pattern in argument.
  This module does not use the node name.
- validate-repos-commits: checks that the repository contains the
  minimum number of commits specified in argument. This module does
  not use the node name.

EXIT CODES

Grading and validation modules return the following exit codes:

  0 - success, no diagnostic messages;
  1 - error in module execution;
  \${ROGER_ERRORCODE} - success, diagnostic messages identify errors;
  \${ROGER_WARNINGCODE} - success, diagnostic messages identify warnings;
  \${ROGER_NOTECODE} - success, diagnostic messages identify notes.")\n"
    exit ${DOC_REQUEST}
fi

###
### Version info
###
if [ "$1" = "--version" ]
then
    ## main script version info
    echo "Roger the Omnigrader ${VERSION} (${DATE})
Copyright (C) 2021-${YEAR} Vincent Goulet

Roger is free software and comes with ABSOLUTELY NO WARRANTY. You are
welcome to redistribute it under the terms of the GNU General Public
License versions 2 or 3. For more information about these matters see
https://www.gnu.org/licenses/."
    exit ${DOC_REQUEST}
fi

###
### Script actions
###

## Tool selected at the command line.
tool="$1"

## Create name of helper script.
case "${tool}" in
    checkreq|clone|grade|harvest|push|switch|validate)
	cmd="${SCRIPTNAME}-${tool}"
	;;
    *)
	print_msg "$(eval_gettext "\${SCRIPTNAME}: unsupported tool '\${tool}'")\n"
	exit 1
	;;
esac
shift

## Defer to helper script.
exec "${cmd}" "$@"

### Local Variables: ***
### mode: sh ***
### End: ***
