## Immutable global variables
SCRIPTNAME=$(basename -- "$0")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
DOC_REQUEST=70

## Default global variables
RESULTS=${grading_file}

if [ $# -eq 0 ] || [ "$1" = "-h" ]
then
    ## push usage information
    print_msg "$(eval_gettext "Usage: \${SCRIPTNAME} \${TOOL} [options...] <branch> [<repository>]
-a <file>, --add-file=<file>
                           Add <file> to commit
-c, --create               Create new branch
-f <file>, --file=<file>   Publish <file> instead of \${grading_file}
--from-head                Create new branches from HEAD
-h                         Print this usage information
--help                     Print help text
-m <msg>, --message=<msg>  Use commit message <msg>
-q, --quiet                Suppress progress messages")\n"
    exit ${DOC_REQUEST}
fi

if [ "$1" = "--help" ]
then
    ## push documentation
    print_msg "$(eval_gettext "NAME

\${SCRIPTNAME} \${TOOL} - publish grading results in a branch

SYNOPSIS

\${SCRIPTNAME} \${TOOL} [-a <file>|--add-file=<file>] [-c|--create]
           [-f <file>|--file=<file>] [--from-head] [-h] [--help]
           [-m <msg>|--message=<msg>] [-q|--quiet]
           <branch> [<repository>]

DESCRIPTION

Publish the grading results in a branch on origin for the repositories
in argument (or, by default, the working directory). New branches are
based off the commits listed in results files by default.

A progress indicator is written to the standard error.

The following options are available:

-a <file>, --add-file=<file>
       By default, only \${grading_file} or the file specified with
       option '-f' is added to the branch. This option allows to add
       another file. If multiple '-a' options are given, their values
       are concatenated to form a list of files.

-c, --create
       Create the branch in argument. The current branch is used
       otherwise.

-f <file>, --file=<file>
       Use <file> as grading results file instead of \${grading_file}.

--from-head
       Create the branch from HEAD rather than from a specific SHA.
       Implies '-c'.

-h
       Print usage information.

--help
       Print this help text.

-m <msg>, --message=<msg>
       The default commit message is

         \"\${commit_msg}\".

       This option specifies an alternate commit message as with 'git
       commit'. If multiple '-m' options are given, their values are
       concatenated as separate paragraphs.

-q, --quiet
       Suppress all output. Progress is not reported to the standard
       error stream.

EXAMPLES

The command

  \${SCRIPTNAME} \${TOOL} -c grading project

creates the branch 'grading' in the repository 'project' off the
commit SHA found in the results file \${grading_file}, commits the
change with the default message and pushes to 'origin'.

The command

  \${SCRIPTNAME} \${TOOL} -f foo.txt -c grading

pushes the results file 'foo.txt' to the branch 'grading' of the
repository in the current working directory.

The command

  \${SCRIPTNAME} \${TOOL} -f foo.txt -a bar.txt grading project

adds the results file 'foo.txt' and the file 'bar.txt' to the existing
branch 'grading' of the repository 'project'.

The command

  \${SCRIPTNAME} \${TOOL} -c grading [0-9]*/

pushes the grading results of all repositories with a name starting by
a digit to the branch 'grading'.

The command

  \${SCRIPTNAME} \${TOOL} -f foo.txt --from-head bar

pushes the file 'foo.txt' to a new branch 'bar' created from the
current HEAD of the repository in the current working directory. The
file 'foo.txt' does not need to contain grading results.")\n"
    exit ${DOC_REQUEST}
fi

###
### Add command build function
###
##
##  Arrays are the only safe way to process the list of additional
##  files with -a|--add-files. This function builds a 'add_with_files'
##  function with additional file names in place using the only array
##  available in POSIX sh: $@.
##
mkaddcmd ()
{
    local clear=1
    local use_next_arg=0
    
    for arg
    do
	if [ "${clear}" = 1 ]
	then
	    set --
	    clear=0
	fi
	if [ "${use_next_arg}" = 1 ] 
	then
	    set -- "$@" "\"${arg}\""
	    use_next_arg=0
	    continue
	fi
	case "${arg}"
	in
	    -a)
		use_next_arg=1
		;;
	    --add-file=*)
		set -- "$@" "\"${arg#*=}\""
		;;
	esac
    done

    eval "add_with_files () { git add $@ \"\$@\"; }"
}

###
### Commit command build function
###
##
##  Arrays are the only safe way to process commit messages specified
##  with -m|--message. This function builds a 'commit_with_msg'
##  function with additional commit messages in place using the only
##  array available in POSIX sh: $@.
##
mkcommitcmd ()
{
    local clear=1
    local use_default_msg=1
    local use_next_arg=0
    
    for arg
    do
	if [ "${clear}" = 1 ]
	then
	    set --
	    clear=0
	fi
	if [ "${use_next_arg}" = 1 ] 
	then
	    set -- "$@" -m "\"${arg}\""
	    use_default_msg=0
	    use_next_arg=0
	    continue
	fi
	case "${arg}"
	in
	    -m)
		use_next_arg=1
		;;
	    --message=*)
		set -- "$@" -m "\"${arg#*=}\""
		use_default_msg=0
		;;
	esac
    done

    if [ "${use_default_msg}" = 1 ]
    then
	set -- "-m" "\"${commit_msg}\""
    fi

    eval "commit_with_msg () { git commit $@ \"\$@\"; }"
}

###
### Script actions
###

## Write messages to the standard error stream.
exec 1>&2

## Scan the options to build the 'git add' command with additional
## file names in place.
mkaddcmd "$@"

## Scan the options to build the 'git commit' command with commit
## messages in place.
mkcommitcmd "$@"

## Process all other options.
while [ $# -gt 0 ]
do
    case "$1" in
	-a)
	    shift;;
	--add-file=*)
	    ;;
	-c|--create)
	    CREATE_BRANCH=true
	    ;;
	-f)
	    RESULTS="$2"; shift
	    ;;
	--file=*)
	    RESULTS="${1#*=}"
	    ;;
	--from-head)
	    CREATE_BRANCH=true
	    GET_SHA=false
	    ;;
	-m)
	    shift;;
	--message=*)
	    ;;
	-q|--quiet)
	    exec 1>/dev/null
	    ;;
	*)
	    break;;
    esac
    shift
done

## Get the name of the branch; now the first argument.
BRANCH="$1"; shift

## Quit if no branch name is given in argument.
! ${BRANCH+false} ||
    error_exit "${msg_missing_branch_name}"

## The repositories to push are now the remaining arguments; push the
## working directory if there are none.
[ $# -eq 0 ] && set -- "${PWD}"

## The tool displays a progress count.
i=0

for REPOS in "$@"
do
    i=$((i + 1))
    (
	## Print the name of the directory being processed.
	repos="$(resolve_directory "${REPOS}")"
	print_msg "$(eval_gettext "Publishing results for \${repos}") (${i}/$#)... "

	## Change current directory.
	cd -- "${REPOS}" >/dev/null 2>&1 || exit 1
	
        ## Check that the results file exists; continue to next
        ## repository if not.
        check_results_file_exists "${RESULTS}" || exit 0

	## Either create the branch to hold the results or check that
	## the branch does already exist.
	if [ "${CREATE_BRANCH:-false}" = true ]
	then
	    create_results_branch "${BRANCH}" \
				  "${GET_SHA:-true}" \
				  "${RESULTS}" || exit 0
	else
	    check_results_branch "${BRANCH}" || exit 0
	fi

	## Switch branch.
	switch_branch "${BRANCH}" || exit 0
	
	## Publish results.
	add_results "${RESULTS}" || exit 0
	commit_results || exit 0
	push_results || exit 0

	## Confirm all went well.
	print_msg "${msg_ok}\n"
    ) || print_msg "${msg_error}\n"
done

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
