## Immutable global variables
SCRIPTNAME=$(basename -- "$0")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
DOC_REQUEST=70

if [ $# -eq 0 ] || [ "$1" = "-h" ]
then
    ## switch usage information
    print_msg "$(eval_gettext "Usage: \${SCRIPTNAME} \${TOOL} [options...] <branch> [<repository>]
-h                         Print this usage information
--help                     Print help text
-q, --quiet                Suppress progress messages")\n"
    exit ${DOC_REQUEST}
fi

if [ "$1" = "--help" ]
then
    ## switch documentation
    print_msg "$(eval_gettext "NAME

\${SCRIPTNAME} \${TOOL} - switch branches

SYNOPSIS

\${SCRIPTNAME} \${TOOL} [-h] [--help] [-q|--quiet] <branch> [<repository>]

DESCRIPTION

Switch branches for the repositories in argument (or, by default, the
working directory). If <branch> is 'main' or 'master', this returns
the repositories to their main branch.

A progress indicator is written to the standard error.

The following options are available:

-h
       Print usage information.

--help
       Print this help text.

-q, --quiet
       Suppress all output. Progress is not reported to the standard
       error stream.

EXAMPLE

If '\${SCRIPTNAME} grade' used with option '-d' left the repositories with
names starting with a digit in a detached HEAD state, then

  \${SCRIPTNAME} \${TOOL} main [0-9]*/

will return all repositories to their main branch.")\n"
    exit ${DOC_REQUEST}
fi

###
### Script actions
###

## Write messages to the standard error stream.
exec 1>&2

## Process options.
while [ $# -gt 0 ]
do
    case "$1" in
	-q|--quiet)
	    exec 1>/dev/null
	    ;;
	*)
	    break;;
    esac
    shift
done

## Get the name of the branch; now the first argument.
BRANCH="$1"; shift

## Quit if no branch name is given in argument.
! ${BRANCH+false} ||
    error_exit "${msg_missing_branch_name}"

## The repositories to process are now the remaining arguments;
## process the working directory if there are none.
[ $# -eq 0 ] && set -- "${PWD}"

## The tool displays a progress count.
i=0

for REPOS in "$@"
do
    i=$((i + 1))
    (
	## Print the name of the repository being processed.
	repos="$(resolve_directory "${REPOS}")"
	print_msg "$(eval_gettext "Switching branch for \${repos}") (${i}/$#)... "

	## Change current directory.
	cd -- "${REPOS}" >/dev/null 2>&1 || exit 1

	## If <branch> is 'main' or 'master', use the true main branch
	## name of the repository.
	if [ "${BRANCH}" = "main" ] || [ "${BRANCH}" = "master" ]
	then
	    BRANCH=$(get_main_name)
	fi

	if ! git switch -q "${BRANCH}"
	then
	    print_msg "${msg_error}\n"
	    prepend "${ROGER_ERRORPROMPT}" <<EOF
${msg_error_switching}
EOF
	    continue
	fi

	## Confirm all went well.
	print_msg "${msg_ok}\n"
    ) || print_msg "${msg_error}\n"
done

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
