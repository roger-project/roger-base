## Immutable global variables
SCRIPTNAME=$(basename -- "$0")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
DOC_REQUEST=70

## Default global variables needed from here.
SCRIPTCONF="${TOOL}conf"

###
### Documentation
###
if [ "$1" = "-h" ]
then
    ## validate usage information
    print_msg "$(eval_gettext "Usage: \${SCRIPTNAME} \${TOOL} [options...] [<directory>]
-c <file>, --config-file=<file>
                           Use <file> as configuration file
-h                         Print this usage information
--help                     Print help text
-n, --no-check-local-repos Ignore the state of the local repository")\n"
    exit ${DOC_REQUEST}
fi

if [ "$1" = "--help" ]
then
    ## validate documentation
    print_msg "$(eval_gettext "NAME

\${SCRIPTNAME} \${TOOL} - validate the repository and files of a project

SYNOPSIS

\${SCRIPTNAME} \${TOOL} [-c <file>|--config-file=<file>] [-h] [--help]
               [-n|--no-check-local-repos] [<directory>]

DESCRIPTION

Perform automated validation of the project in the directory in
argument (or, by default, the working directory) and print results to
the standard output.

The <directory> may be a Git repository. Validation for repositories
is performed on the local copy of the files and the state of the
repository as published on 'origin'. The checks on the state of a Git
are the following:

  - the repository is in the correct project, if applicable;
  - the repository name matches a specific pattern, if applicable;
  - the repository is not empty;
  - the main branch exists;
  - a specific tag exists, if applicable;

The typical checks for a file of source code or raw text are the
following:

  - the file exists and is not empty;
  - the file is tracked by Git, if applicable;
  - the character encoding of the file is ASCII or UTF-8;
  - for files containing source code, the code is portable and
    parsable in the target programming language.

Other types of validation are possible by means of validation modules;
see '\${SCRIPTNAME} --help-module' for details.

The project to validate may contain any combination of Git branches,
directories and files of source code or raw text. The material to
validate is specified in a configuration file; see 'Configuration'
below.

The following options are available:

-c <file>, --config-file=<file>
       Use <file> as grading configuration file instead of the default
       '\${SCRIPTCONF}'.

-h
       Print usage information.

--help
       Print this help text.

-n, --no-check-local-repos
       Omit to check for uncommitted changes or commits not pushed to
       'origin'. These checks are useful for a repository in
       development, but not for a cloned repository.

CONFIGURATION

See '\${SCRIPTNAME} --help-config' for general information on the
validation configuration file.

The '__global' node for this tool uses the following nodes and keys
(elements between [ ] are optional):

__global
  [repository]
    [check_repos: <true|false>]
    [project_name: <name>]
    [pattern_name: <pattern>]
    [main_branch: <name>]
    [tag_name: <name>]
  [defaults]
    [check_exists_laden: <true|false>]
    [check_contents: <true|false>]
    [check_contents_module: <filename> [<args>]]
    [check_encoding: <true|false>]

If the key 'check_repos' is true, the directory being checked must be
a valid Git repository, at the top level of the repository, and not
contain uncommitted or unpushed changes. If either condition fails,
validation of a project stops.

The other nodes of the configuration identify elements to validate using
the following structure:

<node>
  [description: <description>]
  [recursive: <true|false>]
  [check_branch: <true|false>]
  [check_exists_laden: <true|false>]
  [check_contents: <true|false>]
  [check_contents_module: <filename> [<args>]]
  [check_encoding: <true|false>]

Omitted boolean keys are 'false' by default. The 'check_*' keys are
processed in the order above.

The name or a node or its 'description' key is printed in the results,
except for nodes with the special name '.PHONY'. The text in preceded
by a newline and a prefix: '###' for the first level; '##' for the
second level; '#' for the third level; nothing for all deeper levels.

If the key 'check_branch' is 'true', the tool will check that the
branch exists and attempt to switch to it. Validation of the node and
its descendants stops if either action fails.

If the key 'check_exists_laden' is 'true', this tool checks that the
node exists, is not be empty and is tracked by Git. If either of the
first two conditions fails, validation of the node and its descendants
stops.

If the key 'check_contents' is 'true' and 'check_contents_module' is
missing, the validation module is inferred from the extension of the
node (when a file).

Progress messages (including those from validation modules) are
written to the standard output. Diagnostic messages, if any, are
reported after the progress messages, prefixed by a type-specific
marker:

  '\${ROGER_ERRORPROMPT}' for errors;
  '\${ROGER_WARNINGPROMPT}' for warnings;
  '\${ROGER_NOTEPROMPT}' for notes.

FILES

This tool requires that the configuration file \${SCRIPTCONF} be
present in the current working directory (unless a different file is
specified with option '-c').

ENVIRONMENT

The tool assumes that '\${SCRIPTNAME} checkreq' completes without error for the
project to validate.

EXAMPLES

The command

  \${SCRIPTNAME} \${TOOL}

validates the project in the current working directory.

The command

  \${SCRIPTNAME} \${TOOL} -c validadeconf-prototype project

validates the project in directory 'project' based on the
configuration found in the file 'validateconf-prototype'.")\n"
    exit ${DOC_REQUEST}
fi

###
### Script actions
###

## Process options.
while [ $# -gt 0 ]
do
    case $1 in
	-c)
	    SCRIPTCONF="$2"; shift
	    ;;
	--config-file=*)
	    SCRIPTCONF="${1#*=}"
	    ;;
	-n|--no-check-local-repos)
	    CHECK_LOCAL_REPOS=false
	    ;;
	*)
	    break;;
    esac
    shift
done

## Get the name of the directory to validate; now the first argument.
REPOS="$1"

## Validate working directory if no directory is given in argument.
[ -z "${REPOS}" ] && REPOS="${PWD}"

## Expand ~ in ${SCRIPTCONF} if needed.
case "${SCRIPTCONF}" in
    "~/"*) SCRIPTCONF="${HOME}/${SCRIPTCONF#"~/"}";;
esac

## Quit if the configuration file is not found.
[ ! -f "${SCRIPTCONF}" ] &&
    error_exit "${msg_config_not_found}"

## Setup the validation map in a temporary directory (solution valid
## for Linux/Windows and macOS: https://unix.stackexchange.com/a/84980).
MAPDIR="$(mktemp -d 2>/dev/null || mktemp -d -t "${SCRIPTNAME}")"
mkmap_config "${MAPDIR}" "${SCRIPTCONF}" ||
    error_exit "$(gettext "error setting up the validation map")"

## Fetch global variables from the validation map.
GIT_REPOS="false"
if [ -d "${MAPDIR}/__global/repository" ]
then
    GIT_REPOS="true"
    CHECK_REPOS="$(map_get_key "${MAPDIR}/__global/repository" "check_repos")"
    CHECK_REPOS="${CHECK_REPOS:-false}"
    PROJECT_NAME="$(map_get_key "${MAPDIR}/__global/repository" "project_name")"
    REPOS_NAME_PATTERN="$(map_get_key "${MAPDIR}/__global/repository" "pattern_name")"
    MAIN="$(map_get_key "${MAPDIR}/__global/repository" "main_branch")"
    TAG_NAME="$(map_get_key "${MAPDIR}/__global/repository" "tag_name")"
fi

## Fetch default values of keys from the validation map. Each of the
## variables below may end up empty.
if [ -d "${MAPDIR}/__global/defaults" ]
then
    CHECK_EXISTS_LADEN="$(map_get_key "${MAPDIR}/__global/defaults" "check_exists_laden")"
    CHECK_CONTENTS="$(map_get_key "${MAPDIR}/__global/defaults" "check_contents")"
    CHECK_CONTENTS_MODULE="$(map_get_key "${MAPDIR}/__global/defaults" "check_contents_module")"
    CHECK_ENCODING="$(map_get_key "${MAPDIR}/__global/defaults" "check_encoding")"
fi

## Setup the module map in a temporary directory (solution valid for
## Linux/Windows and macOS: https://unix.stackexchange.com/a/84980).
MODULEDIR="$(mktemp -d 2>/dev/null || mktemp -d -t "${SCRIPTNAME}")"
mkmap_modules "${MODULEDIR}" "${ROGER_LIBS}" "${ROGER_USER_LIBS}" "${ROGER_WD}" ||
    error_exit "$(gettext "error setting up the module map")"

## Validate the directory in a sub-shell.
(
    ## Change current directory.
    cd -- "${REPOS}" > /dev/null 2>&1 || exit 1
    
    ## Validation header.
    print_msg "[$(gettext "Validation of directory") ${PWD##*/}]\n"
    
    ## Validate Git repository, if applicable.
    if [ "${CHECK_REPOS}" = "true" ]
    then
	## If not set in the configuration, get the name of the main
	## branch.
	if [ -z "${MAIN}" ]
	then
	    MAIN=$(get_main_name)
	fi

	## Validate the repository; quit in case of errors.
	validate_repos "${PROJECT_NAME}" \
		       "${REPOS_NAME_PATTERN}" \
		       "${CHECK_LOCAL_REPOS:-true}" \
		       "${MAIN}" \
		       "${TAG_NAME}" \
	    || exit 1
    fi

    ## Perform validation operations specified in the validation map;
    ## quit in case of errors.
    validate_map "0" "${MAPDIR}" "" \
		 "${MODULEDIR}" "${GIT_REPOS}" \
		 "${CHECK_EXISTS_LADEN}" \
		 "${CHECK_CONTENTS}" \
		 "${CHECK_CONTENTS_MODULE}" \
		 "${CHECK_ENCODING}" || exit 1

    ## If the directory is a Git repository, return to main branch
    ## prior to quitting.
    if [ "${CHECK_REPOS}" = "true" ]
    then
	switch_to_branch "${MAIN}" || exit 1
    fi
)

exit $?

### Local Variables: ***
### mode: sh ***
### End: ***
