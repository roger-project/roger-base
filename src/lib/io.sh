###
### running_on_windows
###
##
##  Determine if the script is running on Windows (identified by the
##  presence of an environment variable $WINDIR).
##
running_on_windows ()
{
    [ -n "${WINDIR}" ]
}

###
### print_msg
###
##
##  Expand the escape sequences in <string> and writes the result to
##  standard output.
##
##  The code below defines either the version of 'print_msg' to be
##  used on UTF-8 compliant OSes, or the version to be used on
##  Windows, where the encoding of messages may require conversion to
##  UTF-8.
##
##  The Windows specific version is needed at least until Windows 10,
##  where messages from legacy applications (such as gettext) are not
##  encoded in UTF-8, but rather in the ANSI code page
##  (https://stackoverflow.com/a/57134096). This version of
##  'print_msg' also deletes trailing chariot returns (\r).
##
if running_on_windows
then
    print_msg ()
    {
    	local encoding=
    	
    	encoding=$(printf "%b" "$1" | file -b --mime-encoding -)
    	
    	printf "%b" "$1" | if [ "${encoding}" != "ascii" ] && [ "${encoding}" != "utf-8" ]
    	then
    	    iconv -f "${encoding}" -t "utf-8" | tr -d '\r'
    	else
    	    cat -
    	fi
    }
else
    print_msg ()
    {
	printf "%b" "$1"
    }
fi

###
### error <string>
###
##
##  Write the error message <string> to the standard error through
##  'print_msg' (where text encoding conversion may occur on Windows).
##
error ()
{
    print_msg 1>&2 "${1}\n"
}

###
### error_exit <string>
###
##
##  Write the error message "${SCRIPTNAME}: <string>" using 'error'
##  and force exit from the calling script with an error code of 1.
##
error_exit ()
{
    error "${SCRIPTNAME}: ${1}"
    exit 1
}

###
### format_result <description> <result>
###
##
##  Write to standard output a constant length string containing
##  <description> followed by <result>.
##
format_result ()
{
    ## Bash measures string lengths in bytes rather than in
    ## characters. This make a difference for Unicode characters.
    ## Compose the text in two columns by manually inserting the
    ## correct number of spaces.
    ## https://unix.stackexchange.com/a/592479
    local cw=55
    local nws=
    nws=$((cw - ${#1}))

    ## Ensure that at least two spaces separate <description> and
    ## <result>.
    printf "%s%*s%s" "$1" $(( nws < 2 ? 2 : nws )) "" "$2"
}

###
### set_prefix <n>
###
##
##  Return the prefix to put in front of diagnostic messages depending
##  on the return code <n> in argument
##
set_prefix ()
{
    case "$1"
    in
	"${ROGER_ERRORCODE}")
	    echo "${ROGER_ERRORPROMPT}"
	    ;;
	"${ROGER_WARNINGCODE}")
	    echo "${ROGER_WARNINGPROMPT}"
	    ;;
	"${ROGER_NOTECODE}")
	    echo "${ROGER_NOTEPROMPT}"
	    ;;
	*)
	    ;;
    esac
}

###
### prepend <string>
###
##
##  Write the standard input to standard output with <string>
##  prepended to every line.
##
prepend ()
{
    while IFS= read -r line
    do
	print_msg "$1${line}\n"
    done
}
