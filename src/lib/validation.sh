###
### validate_dir_is_repos
###
##
##  Validate that the current directory is a Git repository.
##
validate_dir_is_repos ()
{
    local msg=
    
    msg="$(gettext "Checking that the directory is a Git repository")"
    print_msg "${msg}... "
    if ! check_dir_is_repos
    then
        print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "specify a Git repository to validate")
EOF
	return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_repos_toplevel
###
##
##  Validate that the current directory is the top level of the Git
##  repository.
##
validate_repos_toplevel ()
{
    local msg=
    
    msg="$(gettext "Checking that the directory is the top level of the repository")"
    print_msg "${msg}... "
    if ! check_repos_toplevel
    then
        print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "validate the top level of the repository, not a sub-directory")
EOF
	return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_repos_uncommitted
###
##
##  Validate that there no uncommitted changes in the local
##  repository.
##
validate_repos_uncommitted ()
{
    local msg=
    
    msg="$(gettext "Checking for uncommitted changes")"
    print_msg "${msg}... "
    if check_repos_uncommitted
    then
        print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "commit changes before validation")
EOF
	return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_repos_unpushed
###
##
##  Validate that there are no commits unpushed to 'origin' in the local
##  repository.
##
validate_repos_unpushed ()
{
    local msg=
    
    msg="$(gettext "Checking for unpushed commits")"
    print_msg "${msg}... "
    if check_repos_unpushed
    then
        print_msg "${msg_error}\n"
        prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "push changes to origin before validation")
EOF
	return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_repos_valid_main <name>
###
##
##  Validate that <name> is a valid main branch name.
##
##  The valid names are 'main' and 'master'.
##
validate_repos_valid_main ()
{
    [ $# -ne 1 ] && return 1
    local name="$1"
    local msg=
    
    msg="$(gettext "Checking the name of the main branch")"
    print_msg "${msg}... ${name}... "
    if [ "${name}" != "main" ] && [ "${name}" != "master" ]
    then
        print_msg "${msg_error}\n"
        prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "define a main branch 'main' or 'master' on origin")
EOF
	return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_repos_on_main <branch>
###
##
##  Validate that the current local branch is the main branch
##  <branch>.
##
validate_repos_on_main ()
{
    [ $# -ne 1 ] && return 1
    local branch="$1"
    local msg=
    
    msg="$(eval_gettext "Checking that the current branch is \${branch}")"    
    print_msg "${msg}... "
    if ! check_current_branch "${branch}"
    then
        print_msg "${msg_error}\n"
        prepend "${ROGER_ERRORPROMPT}" <<EOF
$(eval_gettext "checkout branch \${branch} before validation")
EOF
	return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_repos_project_name <name>
###
##
##  Validate that the name of the Git project containing the current
##  repository on 'origin' is <name>.
##
validate_repos_project_name ()
{
    [ $# -ne 1 ] && return 1
    local name="$1"
    local msg=
    
    msg="$(gettext "Checking the name of the project")"
    print_msg "${msg}... "
    if ! check_repos_project_name "${name}"
    then
        print_msg "... ${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(eval_gettext "repository not in project '\${name}'")
EOF
        return "${ROGER_ERRORCODE}"
    fi
    print_msg "... ${msg_ok}\n"
}

###
### validate_repos_name <pattern>
###
##
##  Validate that the remote name of the repository matches the
##  regular expression <pattern>.
##
validate_repos_name ()
{
    [ $# -ne 1 ] && return 1
    local pat="$1"
    local msg=
    
    msg="$(gettext "Checking the name of the repository")"
    print_msg "${msg}... "
    if ! check_repos_name "${pat}"
    then
        print_msg "... ${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(eval_gettext "repository name does not match pattern '\${pat}'")
EOF
        return "${ROGER_ERRORCODE}"
    fi
    print_msg "... ${msg_ok}\n"
}

###
### validate_repos_laden
###
##
##  Check that the local repository contains objects.
##
validate_repos_laden ()
{
    local msg=
    
    msg="$(gettext "Checking that the repository contains objects")"
    print_msg "${msg}... "
    if check_repos_empty
    then
        print_msg "${msg_error}\n"
        return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_repos_tag_exists <name>
###
##
##  Validate that the tag <name> exists on 'origin'.
##
validate_repos_tag_exists ()
{
    [ $# -ne 1 ] && return 1
    local name="$1"
    local msg=
    
    msg="$(eval_gettext "Checking that tag '\${name}' exists")"
    print_msg "${msg}... "
    if ! check_repos_tag_exists "${name}"
    then
        print_msg "${msg_error}\n"
        prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "push the tag to origin with 'git push --tags'")
$(gettext "validation continues for the current branch")
EOF
        return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_repos_branch_exists <branch>
###
##
##  Validate that the branch <branch> exists in the repository.
##
validate_repos_branch_exists ()
{
    [ $# -ne 1 ] && return 1
    local branch="$1"
    local msg=
    
    msg="$(eval_gettext "Checking that the branch '\${branch}' exists")"
    print_msg "${msg}... "
    if ! check_repos_branch_exists "${branch}"
    then
        print_msg "${msg_error}\n"
        return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_repos <project> <pattern> <check_local> <main> <tag_name>
###
##
##  Validate the Git repository in the current directory:
##
##  - repository is located in a Git project <project>;
##  - repository name matches <pattern>;
##  - no uncommitted changes or unpushed commits if <check_local> is
##    "true";
##  - main branch named <main>;
##  - tag <tag_name> exists, if specified (non empty).
##
validate_repos ()
{
    [ $# -ne 5 ] && return 1
    local project_name="$1"
    local repos_name_pattern="$2"
    local check_local="$3"
    local main="$4"
    local tag_name="$5"

    ## Header for the respository validation step.
    print_msg "\n### $(gettext "Git repository")\n"
    
    ## Check that the directory is a Git repository; quit if not the
    ## case.
    validate_dir_is_repos || return 1

    ## Check that the directory is the top level of the repository;
    ## quit if not the case.
    validate_repos_toplevel || return 1
    
    if [ "${check_local}" = "true" ]
    then
	## Check if there are uncommitted changes in the local
	## repository; quit if the case.
	validate_repos_uncommitted || return 1
	
	## Check if there are unpushed commits in the local
	## repository; quit if the case.
	validate_repos_unpushed || return 1
    fi
	
    ## Check that the repository contains a main branch; quit if
    ## not the case.
    validate_repos_valid_main "${main}" || return 1

    ## Check that the main branch is the current branch; quit if
    ## not the case.
    validate_repos_on_main "${main}" || return 1

    ## Check that the name of the Git project containing the
    ## repository matches the name in the configuration, if
    ## applicable; quit if not the case.
    if [ "${project_name}" ]
    then
	validate_repos_project_name "${project_name}" || return 1
    fi

    ## Check that the name of the repository matches the regular
    ## expression pattern in the configuration, if applicable; quit if
    ## not the case.
    if [ "${repos_name_pattern}" ]
    then
	validate_repos_name "${repos_name_pattern}" || return 1
    fi

    ## Check that the repository is laden (contains files); quit if
    ## not the case.
    validate_repos_laden || return 1

    ## Check that the main branch exists on the server; quit if
    ## not the case.
    validate_repos_branch_exists "origin/${main}" || return 1

    ## Check that the repository contains the required tag and
    ## check out this tag, if applicable. Continue with validation
    ## nevertheless.
    if [ "${tag_name}" ] && validate_repos_tag_exists "${tag_name}"
    then
	switch_to_tag "${tag_name}" || return 1
	prepend "${ROGER_WARNINGPROMPT}" <<EOF
$(eval_gettext "repository at tag '\${tag_name}' from this point on")
EOF
    fi
}

###
### validate_file_exists <filename>
###
##
##  Validate that the file (or directory) <filename> exists.
##
validate_file_exists ()
{
    [ $# -ne 1 ] && return 1
    local filename="$1"
    local msg=
    
    msg="$(eval_gettext "Checking that \${filename} exists")"
    print_msg "${msg}... "
    if ! check_file_exists "${filename}"
    then
        print_msg "${msg_error}\n"
        return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_file_laden <filename>
###
##
##  Validate that the file (or directory) <filename> is laden (not
##  empty).
##
validate_file_laden ()
{
    [ $# -ne 1 ] && return 1
    local filename="$1"
    local msg=
    
    msg="$(eval_gettext "Checking that \${filename} is not empty")"
    print_msg "${msg}... "
    if [ -d "${filename}" ]
    then
	if [ -z "$(ls -A ${filename})" ]
	then
            print_msg "${msg_error}\n"
            return "${ROGER_ERRORCODE}"
	fi
    else
	if [ ! -s "${filename}" ]
	then
            print_msg "${msg_error}\n"
            return "${ROGER_ERRORCODE}"
	fi
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_file_tracked <filename>
###
##
##  Validate that the file <filename> is tracked.
##
validate_file_tracked ()
{
    [ $# -ne 1 ] && return 1
    local filename="$1"
    local msg=
    
    msg="$(eval_gettext "Checking that \${filename} is tracked by Git")"
    print_msg "${msg}... "
    if ! check_file_tracked "${filename}"
    then
        print_msg "${msg_warning}\n"
	prepend "${ROGER_WARNINGPROMPT}" <<EOF
$(gettext "index with 'git add'")
EOF
        return "${ROGER_ERRORCODE}"
    fi
    print_msg "${msg_ok}\n"
}

###
### validate_file_encoding <filename>
###
##
##  Validate that the character encoding of the file <filename> is
##  UTF-8 or ASCII.
##
validate_file_encoding ()
{
    [ $# -ne 1 ] && return 1
    local filename="$1"
    local msg=
    
    msg="$(eval_gettext "Checking the encoding of \${filename}")"
    print_msg "${msg}... "
    if ! check_file_encoding "${filename}"
    then
        print_msg "... ${msg_warning}\n"
	prepend "${ROGER_WARNINGPROMPT}" <<EOF
$(gettext "the allowed encodings are 'ascii' and 'utf-8'")
EOF
        return "${ROGER_ERRORCODE}"
    fi
    print_msg "... ${msg_ok}\n"
}

###
### validate_contents <dictionary> <module> <filename>
###
##
##  Validate the contents of <filename> using the validation script
##  <module> listed in the directory <dictionary>.
##
##  The standard output of the module (usually progress messages) is
##  passed through unchanged, followed by the standard error, if any,
##  prefixed by a marker.
##
validate_contents ()
{
    [ $# -ne 3 ] && return 1
    local dictionary="$1"
    local module="$2"
    local filename="$3"
    local errors=$(mktemp)
    local code=

    ## Validate using the validation module, letting standard output
    ## through, and keeping standard error in a temporary file.
    run_validation_module "${dictionary}" "${module}" "${filename}" \
			  2> "${errors}"
    code="$?"

    ## Act upon the return code of the module.
    case "${code}"
    in
	0)
	    return 0
	    ;;
	1)
	    cat "${errors}"
	    return 1
	    ;;
	${ROGER_ERRORCODE}|${ROGER_WARNINGCODE}|${ROGER_NOTECODE})
	    prepend "$(set_prefix "${code}")" < "${errors}"
	    return 0
	    ;;
	*)
	    error "${msg_invalid_exit_code}: ${code}"
	    return 1
    esac
}

###
### validate_map <level>
###              <mapdir>
###              <node_prefix>
###              <moduledir> <git_repos>
###              <default_check_exists_laden>
###              <default_check_contents>
###              <default_check_contents_module>
###              <default_check_encoding>
###
##
##  Validate the map located in <mapdir> using, when needed, the
##  modules in <moduledir>. The argument <level> keeps track of the
##  level within the map to adjust printing of text in the output. The
##  argument <node_prefix> keeps track of directory names for
##  recursive nodes. If <git_repos> is "true", then nodes checked for
##  existence will also be checked for being tracked by Git. Arguments
##  <default_*> contain default values for certain keys. These are
##  specified in the node '__global/defaults' of the grading map.
##
##  This is a recursive function that traverses the whole map
##  directory structure to validate each node in turn.
##
validate_map ()
{
    [ $# -ne 9 ] && return 1
    local level="$1"
    local mapdir="$2"
    local node_prefix="$3"
    local moduledir="$4"
    local git_repos="$5"
    ## Default values of keys defined globally to avoid masking issues
    ## in the subshell triggered by the pipe and while loop combo.
    default_check_exists_laden="$6"
    default_check_contents="$7"
    default_validation_module="$8"
    default_check_encoding="$9"
    ## End default values
    local text_prefix=
    local recursive=
    local node_base=
    local node_desc=
    local check_branch=

    ## Set the text prefix to use in output for the various levels of
    ## the validation map. The highest level is 0, then 1, and so on.
    case ${level}
    in
	0)
	    text_prefix="\n### ";;
	1)
	    text_prefix="\n## ";;
	2)
	    text_prefix="\n# ";;
	*)
	    text_prefix="\n";;
    esac

    ## If the node at the head of the current map is identified as
    ## recursive, add its name to 'node_prefix' for later processing
    ## of the nodes below.
    recursive="$(map_get_key "${mapdir}" "recursive")"
    recursive="${recursive:-false}"
    if [ "${recursive}" = "true" ]
    then
	node_base="${node##*/}"		 # basename of the node
	node_base="${node_base#[0-9]*-}" # without sorting prefix
	node_prefix="${node_prefix}${node_base}/"
    fi

    ## Validate every node at the current level.
    map_get_node "${mapdir}" | while read -r node
    do
	## Reset keys to default values to avoid leakage from previous
	## nodes.
	check_exists_laden="${default_check_exists_laden}"
	check_contents="${default_check_contents}"
	validation_module="${default_validation_module}"
	check_encoding="${default_check_encoding}"

	## Build the base node name.
	node_base="${node##*/}"		 # basename of the node
	node_base="${node_base#[0-9]*-}" # without sorting prefix
	node_base="${node_prefix}${node_base}" # with node prefix

        ## For nodes named other than '.PHONY', print the node name,
        ## preceded by the level-dependent text prefix.
        if [ "${node_base}" != ".PHONY" ]
        then
            node_desc="$(map_get_key "${node}" "description")"
            node_desc="${node_desc:-"${node_base}"}"
            print_msg "${text_prefix}${node_desc}\n"
        fi
        
        ## If the node is identified as a Git branch, perform specific
        ## branch level operations.
        check_branch="$(map_get_key "${node}" "check_branch")"
        check_branch="${check_branch:-false}"
        if [ "${check_branch}" = "true" ]
        then
	    ## If the branch name is 'main' or 'master', use the true
            ## main branch name from now on.
            if [ "${node_base}" = "main" ] || [ "${node_base}" = "master" ]
            then
        	## Guard against the possibility that the main branch
        	## name is still unset because the validity of the
        	## repository was not checked.
        	node_base=$(get_main_name)
            fi

            ## Check that the branch exists; continue to next node if
            ## not.
            validate_repos_branch_exists "origin/${node_base}" || continue
            
            ## Switch to the branch to validate.
            switch_to_branch "${node_base}" || return 1
        fi
	    
        ## If the node is identified as a file or directory that
        ## should exist and not be empty, perform these specific
        ## operations. If the directory being processed is a Git
        ## repository, also check that the node is tracked by Git.
	##
	## From here on, we must take care to overwrite the (eventual)
	## default value of a key only if that key is specified for
	## the node.
	key="$(map_get_key "${node}" "check_exists_laden")" &&
	    check_exists_laden="${key}"
        check_exists_laden="${check_exists_laden:-false}"
        if [ "${check_exists_laden}" = "true" ]
        then
	    ## Check that the file (or directory) exists; continue to
	    ## next node if not.
	    validate_file_exists "${node_base}" || continue

	    ## Check that the file (or directory) is laden; continue
	    ## to next file if not.
	    validate_file_laden "${node_base}" || continue

	    ## Check that the file (or at least one file in a
	    ## directory) is registered by Git; continue in any case.
	    if [ "${git_repos}" = "true" ]
	    then
		validate_file_tracked "${node_base}"
	    fi
	fi

        ## Check the contents of the node (usually the source code in
        ## a file or directory) with the specified module, if
        ## appropriate.
        key=$(map_get_key "${node}" "check_contents") &&
            check_contents="${key}"
        check_contents="${check_contents:-false}"
        if [ "${check_contents}" = "true" ]
        then
	    key="$(map_get_key "${node}" "check_contents_module")" &&
		validation_module="${key}"
            validate_contents "${moduledir}" \
			      "${validation_module}" \
			      "${node_base}"
        fi

	## Check the encoding of the file, if appropriate.
        key="$(map_get_key "${node}" "check_encoding")" && 
	    check_encoding="${key}"
        check_encoding="${check_encoding:-false}"
        if [ "${check_encoding}" = "true" ]
	then
	    validate_file_encoding "${node_base}"
	fi
        	
        ## Recurse into sub-nodes. Update first three arguments, keep
        ## the other ones unchanged.
	shift; shift; shift
	set -- $(( level+1 )) "${node}" "${node_prefix}" "$@"
        validate_map "$@"
    done
}
