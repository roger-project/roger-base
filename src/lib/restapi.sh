###
### getrepos <uri> <filename_header> <filename_content>
###
##
##  Retrieve the list of repositories at <uri> using the special
##  purpose function 'curl_with_options'. Store the headers in file
##  <filename_header> and the content in file <filename_content>.
##
##  STANDARD OUTPUT
##
##  Nothing.
##
##  STANDARD ERROR
##
##  Message explaining the source of the error, if any.
##
##  ENVIRONMENT
##
##  The function 'curl_with_options' must exist.
##
getrepos ()
{
    [ $# -ne 3 ] && return 1
    local uri="$1"
    local header="$2"
    local content="$3"

    ## The standard error of curl (the http code) is saved in a
    ## variable, letting standard output through to later redirect it
    ## to a file.
    { http_code=$(curl_with_options -D "${header}" \
				    -w "%{stderr}%{http_code}" \
				    --url "${uri}" 2>&1 1>&3 3>&-); } \
	3> "${content}"

    case "$?" in
	0)
	    case "${http_code}" in
		200)
		;;
		401)
		    error "$(gettext "authenticated user does not have the required permissions for this project")"
		    return 1
		    ;;
		404)
		    error "$(gettext "the project does not exist or invalid url")"
		    return 1
		    ;;
		500)
		    error "$(gettext "incorrect resource url or unexpected server error")"
		    return 1
		    ;;
		*)
		    error "$(gettext "unknown http code")"
		    return 1
		    ;;
	    esac
	    ;;
	6)
	    error "$(gettext "could not resolve repository"): ${machine}"
	    return 1
	    ;;
	7)
	    error "$(gettext "failed to connect to repository"): ${machine}"
	    return 1
	    ;;
	*)  
	    error "$(gettext "internal curl error")"
	    return 1
	    ;;
    esac
}

###
### get_link_next <filename>
###
##
##  Extract from the HTTP headers file <filename> the 'rel="next"'
##  link in the string of the form:
##
##    link: ... <https://...>; rel="next", ...
##               ^^^^^^^^^^^
##               |- extracted link
##
get_link_next ()
{
    sed -E -n '/^link:/s/.*<([^>]*)>; ?rel="next".*/\1/p' "$1"
}

###
### getrepos_bitbucket <machine> <project>
###
##
##  Retrieve the clone urls of all the repositories in the BitBucket
##  project <project> on server <machine>.
##
##  REFERENCE
##
##  https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html
##
##  STANDARD OUTPUT
##
##  List of repositories.
##
##  STANDARD ERROR
##
##  Message explaining the source of the error, if any.
##
getrepos_bitbucket ()
{
    [ $# -ne 2 ] && return 1
    local machine="$1"
    local project="$2"

    ## In the BitBucket API, pagination information appears in the
    ## JSON data; headers are not needed.
    ##
    ## The 'limit' query parameter indicates how many results to
    ## return per page. The 'start' query parameter indicates which
    ## item should be used as the first item in the page of results.
    local limit=100		# standard maximum
    local start=0		# start with the first page of results

    ## Create uri to the BitBucket REST API 1.0 to retrieve the list
    ## of repositories in a project.
    local apislug="rest/api/1.0/projects"
    local uri="https://${machine}/${apislug}/${project}/repos?limit=${limit}"

    ## Create a temporary file to hold the content of the list of
    ## repositories request.
    local jsondata=$(mktemp)

    while :
    do
	## Retrieve the list of repositories in the project in JSON
	## format, discarding the headers.
	getrepos "${uri}&start=${start}" "/dev/null" "${jsondata}" ||
	    return 1

	## Extract and return the clone url of the repositories. The
	## clone url is in a JSON array of the form:
	##
	## "clone":[{"href":"https://<user>@<url>","name":"http"}],...
	## 
	## We need to remove the '<user>@' part.
	grep -E -o '"clone":[^,]*' "${jsondata}" | \
	    awk 'BEGIN { FS="\"" } 
                {
                    sub("://.*@", "://", $6)
                    print $6
                }'

	## Stop if the last page of results is reached; indicated by
	## the string '"isLastPage":true' in the response.
	grep -E -q '"isLastPage": ?true' "${jsondata}" && break

	## Find the starting point for the next page of results using
	## the 'nextPageStart' attribute of the response.
	start=$(grep -E -o '"nextPageStart": ?[0-9]+' "${jsondata}" | \
		    cut -d: -f2)
    done
}

###
### getrepos_gitlab <machine> <group>
###
##
##  Retrieve the clone urls of all the repositories in the GitLab
##  group <group> on server <machine>.
##
##  REFERENCE
##
##  https://docs.gitlab.com/ee/api
##
##  STANDARD OUTPUT
##
##  List of repositories.
##
##  STANDARD ERROR
##
##  Message explaining the source of the error, if any.
##
getrepos_gitlab ()
{
    [ $# -ne 2 ] && return 1
    local machine="$1"
    local group="$2"

    ## In the GitLab API, offset-based pagination information appears
    ## in link headers.
    ##
    ## The 'per_page' query parameter indicates how many results to
    ## return per page. The start page is always the first one; urls
    ## to the following ones, if any, are obtained from the link
    ## headers of the response.
    local per_page=100		# maximum allowed by GitLab API

    ## Create uri to the GitLab REST API to retrieve the list of
    ## repositories (or projects) in a group. 
    local apislug="api/v4/groups"
    local uri="https://${machine}/${apislug}/${group}/projects?simple=yes&per_page=${per_page}"

    ## Create temporary files to hold the header and content of the
    ## list of repositories request.
    local headers=$(mktemp)
    local jsondata=$(mktemp)

    while :
    do
	## Retrieve the list of repositories in the project in JSON
	## format.
	getrepos "${uri}" "${headers}" "${jsondata}" ||
	    return 1

	## Extract and return the clone url of the repositories. The
	## clone url is in a JSON string of the form:
	##
	## "http_to_repo":"https://...",...
	##
	grep -E -o '"http_url_to_repo":[^,]*' "${jsondata}" | \
	    awk 'BEGIN { FS="\"" } { print $4 }'

	## Set uri for the next page of results.
	uri=$(get_link_next "${headers}")
	
	## Stop if the uri is empty; last page of results reached.
	[ -z "${uri}" ] && break
    done
}


###
### getrepos_github <machine> <org>
###
##
##  Retrieve the clone urls of all the repositories in the GitHub
##  organization <org> on server <machine>.
##
##  REFERENCE
##
##  https://docs.github.com/en/rest
##
##  STANDARD OUTPUT
##
##  List of repositories.
##
##  STANDARD ERROR
##
##  Message explaining the source of the error, if any.
##
getrepos_github ()
{
    [ $# -ne 2 ] && return 1
    local machine="$1"
    local org="$2"

    ## In the GitHub API, pagination information appears in link
    ## headers.
    ##
    ## The 'per_page' query parameter indicates how many results to
    ## return per page. The start page is always the first one; urls
    ## to the following ones, if any, are obtained from the link
    ## headers of the response.
    local per_page=100		# maximum allowed by GitHub API

    ## Create uri to the GitHub REST API to retrieve the list of
    ## repositories in an organization. 
    local apislug="api"
    local uri="https://${apislug}.${machine}/orgs/${org}/repos?per_page=${per_page}"

    ## Create temporary files to hold the header and content of the
    ## list of repositories request.
    local headers=$(mktemp)
    local jsondata=$(mktemp)

    while :
    do
	## Retrieve the list of repositories in the project in JSON
	## format.
	getrepos "${uri}" "${headers}" "${jsondata}" ||
	    return 1

	## Extract and return the clone url of the repositories. The
	## clone url is in a JSON string of the form:
	##
	## "clone_url": "https://...",...
	grep -E -o '"clone_url": ?[^,]*' "${jsondata}" | \
	    awk 'BEGIN { FS="\"" } { print $4 }'

	## Set uri for the next page of results.
	uri=$(get_link_next "${headers}")
	
	## Stop if the uri is empty; last page of results reached.
	[ -z "${uri}" ] && break
    done
}
