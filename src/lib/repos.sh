###
### count_objects
###
##
##  Count the number of loose (unpacked) objects in the repository or,
##  if this value is zero, the number of in-pack objects.
##
##  STANDARD OUTPUT
##
##  Number of objects.
##
count_objects ()
{
    git count-objects -v | awk 'BEGIN { n = 0 } 
                                /^count/ { if ($2 > 0) { n = $2; exit } } 
                                /^in-pack/ { n = $2 > 0 ? $2 : n } 
                                END { print n }'
}

###
### switch_to_tag <name>
###
##
##  Switch to tag <name>.
##
switch_to_tag ()
{
    [ $# -ne 1 ] && return 1
    local name="$1"

    if ! git switch -q --detach "${name}" >/dev/null 2>&1
    then
	print_msg "${msg_error_switching_to} '${name}'"
	return 1
    fi
}

###
### switch_to_branch <name> 
###
##
##  Switch to branch <name>.
##
switch_to_branch ()
{
    [ $# -ne 1 ] && return 1
    local name="$1"

    if ! git switch -q "${name}" >/dev/null 2>&1
    then
	print_msg "${msg_error_switching_to} '${name}'"
	return 1
    fi
}

###
### check_dir_is_repos
###
##
##  Check that the current directory is a Git repository.
##
##  'git rev-parse --is-inside-work-tree' returns 'true' if the
##  current directory is in the work tree, 'false' if it is the '.git'
##  repository and nothing with an error code of 128 if the directory
##  is not a repository.
##
check_dir_is_repos ()
{
    git rev-parse --is-inside-work-tree >/dev/null 2>&1
}

###
### check_repos_toplevel
###
##
##  Check that the current directory is the top level of the Git
##  repository.
##
check_repos_toplevel ()
{
    ## The current working directory is the top level of the Git
    ## repository if the path to the top-level directory is empty.
    [ -z "$(git rev-parse --show-cdup)" ]
}

###
### check_repos_uncommitted
###
##
##  Check that there no uncommitted changes in the local repository.
##
check_repos_uncommitted ()
{
    git status --porcelain | grep -qv '^??'
}

###
### check_repos_unpushed
###
##
##  Check that there are no commits unpushed to 'origin' in the local
##  repository.
##
check_repos_unpushed ()
{
    local branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)

    [ "$(git log origin/"${branch}"..HEAD 2>/dev/null | head -c1 | wc -c)" -ne "0" ]
}

###
### check_repos_project_name <name>
###
##
##  Check that the name of the Git project containing the current
##  repository on 'origin' is <name>.
##
##  STANDARD OUTPUT
##
##  Name of the project.
##
check_repos_project_name ()
{
    local fullname="$(dirname -- "$(git config --get remote.origin.url)")"
    local name=${fullname##*/}	# last component of the url

    printf "%s" "${name}"

    [ "${name}" = "$1" ]
}

###
### check_repos_name <pattern>
###
##
##  Check that the remote name of the repository matches the
##  extended regular expression <pattern>.
##
##  STANDARD OUTPUT
##
##  Name of the repository.
##
check_repos_name ()
{
    local name="$(basename -s .git "$(git config --get remote.origin.url)")"

    printf "%s" "${name}"

    grep -E -q "$1" <<EOF
${name}
EOF
}

###
### check_repos_empty
###
##
##  Check that the local repository is empty.
##
check_repos_empty ()
{
    [ "$(count_objects)" -eq "0" ]
}

###
### check_repos_tag_exists <name>
###
##
##  Check that the tag <name> exists on 'origin'.
##
check_repos_tag_exists ()
{
    ## The result of 'git ls-remote --tags' is of the form
    ##
    ## <sha>   refs/tags/<name>
    ##
    ## and we need to check against the exact name of the tag.
    git ls-remote --tags origin | cut -d / -f 3 | grep -E -q "^${1}$"
}

###
### get_main_name
###
##
##  Get the name of the main branch on 'origin'.
##
get_main_name ()
{
    git remote show origin | grep "HEAD" | sed 's/.*: //'
}

###
### check_current_branch <branch>
###
##
##  Check that the current local branch is <branch>.
##
check_current_branch ()
{
    test "$(git branch --show-current)" = "$1"
}

###
### check_repos_branch_exists <pattern>
###
##
##  Check that the current repository contains a branch with a name
##  matching <pattern> (in the 'git show-ref' sense).
##
##  To specifically check for the existence of a remote branch, use
##  'origin/' in <pattern>.
##
check_repos_branch_exists ()
{
    git show-ref -q "$1"
}

###
### check_file_tracked <filename>
###
##
##  Check that the file <filename> is tracked.
##
check_file_tracked ()
{
    git ls-files --error-unmatch "$1" >/dev/null 2>&1
}

###
### checkout_by_date <timestamp>
###
##
##  Check out the latest version of the repository prior to the date
##  and time <timestamp>.
##
##  VALUE
##
##  Result of 'git checkout' if the repository contains at least one
##  commit; 2 otherwise.
##
checkout_by_date ()
{
    if [ -z "$(git rev-list --all)" ]
    then
        return 2
    fi

    local sha=$(git rev-list -n 1 --before="$1" HEAD)
    git checkout "${sha}" >/dev/null 2>&1
}

###
### checkout_by_tag <tag> <timestamp>
###
##
##  Check out the tag reference <tag> if, and only if, it points to a
##  commit prior to the date and time <timestamp>.
##
##  VALUE
##
##  Result of 'git checkout' if the repository contains at least one
##  commit and <tag> is prior to <timestamp>; 2 if <tag> is posterior
##  to <timestamp>; 3 if the repository is empty.
##
checkout_by_tag ()
{
    if [ -z "$(git rev-list --all)" ]
    then
        return 3
    fi

    local tagsha=$(git rev-list -n 1 "$1" 2>/dev/null | cut -c1-7)
    local sha=$(git rev-list -n 1 --before="$2" "$1" 2>/dev/null | cut -c1-7)

    if [ "${tagsha}" != "${sha}" ]
    then
	return 2
    fi

    git checkout "${tagsha}" >/dev/null 2>&1
}
