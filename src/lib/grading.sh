###
### grade_repos_valid_main <name>
###
##
##  Check that <name> is a valid main branch name. Write a result of 0
##  with a return code of 1 if not the case.
##
##  The valid names are 'main' and 'master'.
##
grade_repos_valid_main ()
{
    [ $# -ne 1 ] && return 1
    local name="$1"
    local msg=

    if [ "${name}" != "main" ] && [ "${name}" != "master" ]
    then
        msg="$(gettext "No branch 'main' or 'master'")"
        print_msg "$(format_result "\n${msg}" "0")\n"
        return 1
    fi
}

###
### grade_repos_update
###
##
##  Update the repository.
##
grade_repos_update ()
{
    if ! git pull -q --all --tags
    then
	print_msg "$(gettext "error updating the repository")"
	return 1
    fi
}

###
### grade_repos_by_date <timestamp>
###
##
##  Check out the latest version of the repository prior to the date
##  and time <timestamp>, and write the SHA value of the commit.
##
grade_repos_by_date ()
{
    [ $# -ne 1 ] && return 1
    local date="$1"

    checkout_by_date "${date}"
    case $? in
	0)
	    local sha="$(git rev-parse --short HEAD)"
	    print_msg "${msg_grading_sha} ${sha}\n"
	    ;;
	1|128)
            print_msg "$(gettext "Error checking out the version of the repository")\n"
	    return 1
	    ;;
	2)
	    print_msg "$(gettext "No commit in the repository")\n"
	    return 1
	    ;;
	*)
	    print_msg "$(gettext "Unknown error in grade_by_date")\n"
	    return 1
	    ;;
    esac
}

###
### grade_repos_by_tag <tag> <timestamp>
###
##
##  Check out the tag reference <tag> if, and only if, it points to a
##  commit prior to the date and time <timestamp>, and write the SHA
##  value of the commit.
##
grade_repos_by_tag ()
{
    [ $# -ne 2 ] && return 1
    local tag="$1"
    local date="$2"

    checkout_by_tag "${tag}" "${date}"
    case $? in
	0)
	    local sha="$(git rev-parse --short HEAD)"
	    print_msg "${msg_grading_sha} ${sha}\n"
	    ;;
	1|128)
            print_msg "$(gettext "Error checking out the version of the repository")\n"
	    return 1
	    ;;
	2)
	    local tagsha="$(git rev-parse --short "${tag}")"
	    print_msg "$(eval_gettext "tag \${tag} points to a commit \${tagsha} posterior to ${date}")"
	    return 1
	    ;;
	3)
	    print_msg "$(gettext "No commit in the repository")\n"
	    return 1
	    ;;
	*)
	    print_msg "$(gettext "Unknown error in grade_by_tag")\n"
	    return 1
	    ;;
    esac
}

###
### grade_repos_laden
###
##
##  Check that the repository contains objects, Write a result of 0
##  with a return code of 1 if not the case.
##
grade_repos_laden ()
{
    local msg=

    if check_repos_empty
    then
        msg="$(gettext "Empty repository")"
        print_msg "$(format_result "${msg}" "0")\n"
        return 1
    fi
}

###
### grade_repos <main> <timestamp> <tag_name>
###
##
##  Grade the Git repository in the current directory with main branch
##  <main>, in its state at the last commit before <timestamp> and, if
##  specified, for a specific tag <tag_name>.
##
grade_repos ()
{
    [ $# -ne 3 ] && return 1
    local main="$1"
    local timestamp="$2"
    local tag_name="$3"

    ## Check that the repository contains a main branch; quit if not
    ## the case.
    grade_repos_valid_main "${main}" || return 1

    ## Safety precaution: switch to the main branch.
    switch_to_branch "${main}" || return 1

    ## Update the repository.
    grade_repos_update || return 1

    ## Checkout the repository either by tag or by date; quit
    ## in case of error.
    if [ "${tag_name}" ]
    then
	print_msg "\n### ${msg_checkout_by_tag} ${tag_name}\n"
	grade_repos_by_tag "${tag_name}" "${timestamp}"
    else
	print_msg "\n### ${msg_checkout_by_date} ${timestamp}\n"
	grade_repos_by_date "${timestamp}"
    fi || return 1

    ## Check that the repository is laden (contains files); quit if
    ## not the case.
    grade_repos_laden || return 1

    ## Write a message confirming that the directory is a valid Git
    ## repository.
    print_msg "${msg_valid_repository}\n"
}

###
### grade_repos_branch_exists <branch>
###
##
##  Check that the branch <branch> exists in the repository. Write a
##  result of 0 with a return code of 1 if not the case.
##
grade_repos_branch_exists ()
{
    [ $# -ne 1 ] && return 1
    local branch="$1"
    local msg=

    if ! check_repos_branch_exists "${branch}"
    then
	msg="${msg_branch_not_found}: ${branch}"
        print_msg "$(format_result "${msg}" "0")\n"
        return 1
    fi
}

###
### grade_file_exists <filename>
###
##
##  Check that the file (or directory) <filename> exists. Write a
##  result of 0 with a return code of 1 if not the case.
##
grade_file_exists ()
{
    [ $# -ne 1 ] && return 1
    local filename="$1"
    local msg=

    if ! check_file_exists "${filename}"
    then
	msg="${msg_file_not_found}: ${filename}"
        print_msg "$(format_result "${msg}" "0")\n"
        return 1
    fi
}

###
### grade_file_laden <filename>
###
##
##  Check that the file or directory <filename> is laden (not empty).
##  Write a result of 0 with a return code of 1 if not the case.
##
grade_file_laden ()
{
    [ $# -ne 1 ] && return 1
    local filename="$1"
    local msg=

    ## Checks differ for directories and regular files.
    if [ -d "${filename}" ]
    then
	[ -z "$(ls -A "${filename}")" ] && \
	    { msg="${msg_dir_empty}: ${filename}"; }
    else
	[ ! -s "${filename}" ] && \
	    { msg="${msg_file_empty}: ${filename}"; }
    fi

    if [ "${msg}" ]
    then
        print_msg "$(format_result "${msg}" "0")\n"
        return 1
    fi
}

###
### grade_criteria <description> <dictionary> <module> <testfile> <points>
###
##
##  Write the <description> and the result on <points> for the grading
##  criteria evaluated with the grading script <module> listed in the
##  directory <dictionary> using the unit tests in <testfile>.
##
##  If <testfile> and <module> are both empty, then only <description>
##  and "/<points>" are written.
##
##  The result is followed by the standard error of the module, if
##  any, prefixed by a marker.
##
##  The grade is proportional to the number of passed tests.
##
grade_criteria ()
{
    [ $# -ne 5 ] && return 1
    local description="$1"
    local dictionary="$2"
    local module="$3"
    local testfile="$4"
    local points="$5"
    local test_results=
    local errors=$(mktemp)
    local grade=
    local code=

    if [ "${testfile}" ] || [ "${module}" ]
    then
	## Get raw grading results.
	test_results=$(run_grading_module "${dictionary}" \
					  "${module}" \
					  "${testfile}" \
					  2> "${errors}")
	code="$?"

	## Quit if the module failed (other non-zero exit codes are
	## allowed to identify the type of diagnostic messages).
	[ "${code}" -eq 1 ] &&
	    {
		error "$(gettext "error running the grading module")"
		return 1
	    }

	## Compute the grade using the results from the first line
	## containing exactly two integer values. If the second value
	## (the number of executed tests) is 0, return a blank grade.
	grade=$(echo "${test_results}" | \
		    LC_NUMERIC=C awk -v points="${points}" \
			'$0 ~ / *[0-9]+ +[0-9]+ */ \
		         {
			    if ($2 > 0)
			      printf "%.2f", ($1 <= $2 ? $1/$2 : 1) * points
			    exit
			 }' \
			-)
    fi

    ## Write the <description> and result.
    print_msg "$(format_result "${description}" "${grade:-}/${points}")\n"

    ## Write the diagnostic messages, prefixed by a marker.
    prepend "$(set_prefix "${code}")" < "${errors}"
}

###
### grade_contents <dictionary> <module> <filename>
###
##
##  Grade (actually validate only) the file <filename> using the
##  validation script <module> listed in the directory <dictionary>.
##  If the check fails, write a result of 0 and return with an error
##  code.
##
##  The standard output of the module (usually progress messages) is
##  dropped; only the standard error, if any, is passed through,
##  prefixed by a marker.
##
grade_contents ()
{
    [ $# -ne 3 ] && return 1
    local dictionary="$1"
    local module="$2"
    local filename="$3"
    local errors=$(mktemp)
    local msg=
    local code=

    ## Validate using the validation module, dropping standard output
    ## and keeping standard error in a temporary file.
    run_validation_module "${dictionary}" "${module}" "${filename}" \
			  1>/dev/null 2> "${errors}"
    code="$?"

    ## Act upon the return code of the module.
    case "${code}"
    in
	0)
	    return 0
	    ;;
	1)
	    cat "${errors}"
	    return 1
	    ;;
	${ROGER_ERRORCODE}|${ROGER_WARNINGCODE}|${ROGER_NOTECODE})
	    msg="${msg_contents_invalid}: ${filename}"
            print_msg "$(format_result "${msg}" "0")\n"
	    prepend "$(set_prefix "${code}")" < "${errors}"
	    return 1
	    ;;
	*)
	    error "${msg_invalid_exit_code}: ${code}"
	    return 1
    esac
}

###
### grade_file_encoding <filename> <points>
###
##
##  Write the result on <points> if the character encoding of the file
##  <filename> is UTF-8 or ASCII, or 0 and the encoding otherwise.
##
grade_file_encoding ()
{
    [ $# -ne 2 ] && return 1
    local filename="$1"; local points="$2"
    local encoding=
    local result=
    local msg=
    local errors=

    if ! encoding="$(check_file_encoding "${filename}")"
    then
	result=0
	errors="$(eval_gettext "file encoding: \${encoding}")"
    fi

    msg="$(gettext "Character encoding")"
    print_msg "$(format_result "${msg}" "${result:-${points}}/${points}")\n"
    [ -z "${errors}" ] || prepend "${ROGER_ERRORPROMPT}" <<EOF
${errors}
EOF
}

###
### grade_map <level>
###           <mapdir>
###           <node_prefix>
###           <moduledir>
###           <default_check_exists_laden>
###           <default_check_contents>
###           <default_check_contents_module>
###           <default_check_encoding>
###           <default_points_encoding>
###
##
##  Grade the map located in <mapdir> using, when needed, the modules
##  in <moduledir>. The argument <level> keeps track of the level
##  within the map to adjust printing of text in the results.
##  Arguments <default_*> contain default values for certain keys.
##  These are specified in the node '__global/defaults' of the grading
##  map.
##
##  This is a recursive function that traverses the whole map
##  directory structure to grade each node in turn.
##
grade_map ()
{
    [ $# -ne 9 ] && return 1
    local level="$1"
    local mapdir="$2"
    local node_prefix="$3"
    local moduledir="$4"
    ## Default values of keys defined globally to avoid masking issues
    ## in the subshell triggered by the pipe and while loop combo.
    default_check_exists_laden="$5"
    default_check_contents="$6"
    default_validation_module="$7"
    default_check_encoding="$8"
    default_points_encoding="$9"
    ## End default values
    local text_prefix=
    local recursive=
    local node_base=
    local node_desc=
    local grading_module=
    local test_file=
    local points=
    local check_branch=

    ## Set the text prefix to use in results for the various levels of
    ## the grading map. The highest level is 0, then 1, and so on.
    case ${level}
    in
	0)
	    text_prefix="\n### ";;
	1)
	    text_prefix="\n## ";;
	2)
	    text_prefix="\n# ";;
	*)
	    text_prefix="\n";;
    esac

    ## If the node at the head of the current map is identified as
    ## recursive, add its name to 'node_prefix' for later processing
    ## of the nodes below.
    recursive="$(map_get_key "${mapdir}" "recursive")"
    recursive="${recursive:-false}"
    if [ "${recursive}" = "true" ]
    then
	node_base="${node##*/}"		 # basename of the node
	node_base="${node_base#[0-9]*-}" # without sorting prefix
	node_prefix="${node_prefix}${node_base}/"
    fi

    ## Grade every node at the current level.
    map_get_node "${mapdir}" | while read -r node
    do
	## Reset keys to default values to avoid leakage from previous
	## nodes.
	check_exists_laden="${default_check_exists_laden}"
	check_contents="${default_check_contents}"
	validation_module="${default_validation_module}"
	check_encoding="${default_check_encoding}"
	points_encoding="${default_points_encoding}"

        ## A node with a 'points' key is special: it is a criteria to
        ## grade using a grading module. We don't write the name of
        ## the node in the results (as the nodes at this level are
        ## merely used to separate the criteria and have no real
        ## meaning). Criteria nodes are always terminal.
        points="$(map_get_key "${node}" "points")"
        if [ "${points}" ]
        then
            node_desc="$(map_get_key "${node}" "description")"
            grading_module="$(map_get_key "${node}" "grade_criteria_module")"
            test_file="$(map_get_key "${node}" "tests")"
            [ "${test_file}" ] && test_file="${ROGER_WD}/${test_file}"
            grade_criteria "${node_desc}" \
			   "${moduledir}" \
			   "${grading_module}" \
			   "${test_file}" \
			   "${points}"
	    continue
        fi

	## From here on, the node is not a criteria.
        node_base="${node##*/}"		 # basename of the node
        node_base="${node_base#[0-9]*-}" # without sorting prefix
	node_base="${node_prefix}${node_base}" # with node prefix

        ## For nodes named other than '.PHONY', print the description
        ## key, if any, or else the node name. In either case, the
        ## text in preceded by the level-dependent text prefix.
        if [ "${node_base}" != ".PHONY" ]
        then
            node_desc="$(map_get_key "${node}" "description")"
            node_desc="${node_desc:-"${node_base}"}"
            print_msg "${text_prefix}${node_desc}\n"
        fi

        ## If the node is identified as a Git branch, perform specific
        ## branch level operations.
        check_branch="$(map_get_key "${node}" "check_branch")"
        check_branch="${check_branch:-false}"
        if [ "${check_branch}" = "true" ]
        then
            ## If the branch name is 'main' or 'master', use the true
            ## main branch name from now on.
            if [ "${node_base}" = "main" ] || [ "${node_base}" = "master" ]
            then
        	## Guard against the possibility that the main branch
        	## name is still unset because the validity of the
        	## repository was not checked.
        	node_base=$(get_main_name)
            fi

            ## Check that the branch exists; continue to next node if
            ## not.
            grade_repos_branch_exists "origin/${node_base}" || continue

            ## Switch to the branch to grade.
            switch_to_branch "${node_base}" || return 1
        fi

        ## If the node is identified as a file or directory that
        ## should exist and not be empty, perform these specific
        ## operations.
	##
	## From here on, we must take care to overwrite the (eventual)
	## default value of a key only if that key is specified for
	## the node.
	key="$(map_get_key "${node}" "check_exists_laden")" &&
	    check_exists_laden="${key}"
        check_exists_laden="${check_exists_laden:-false}"
        if [ "${check_exists_laden}" = "true" ]
        then
            ## Check that the file (or directory) exists; continue to
            ## next node if not.
            grade_file_exists "${node_base}" || continue

            ## Check that the file (or directory) is laden; continue
            ## to next node if not.
            grade_file_laden "${node_base}" || continue
        fi

        ## Check the contents of the node (usually the source code in
        ## a file or directory) with the specified module, if
        ## appropriate. Continue to next node if the check fails.
        key="$(map_get_key "${node}" "check_contents")" &&
            check_contents="${key}"
        check_contents="${check_contents:-false}"
        if [ "${check_contents}" = "true" ]
        then
	    key="$(map_get_key "${node}" "check_contents_module")" &&
		validation_module="${key}"
            grade_contents "${moduledir}" \
			   "${validation_module}" \
			   "${node_base}" || continue
        fi

        ## Grade the encoding, if appropriate.
        key="$(map_get_key "${node}" "check_encoding")" && 
	    check_encoding="${key}"
        check_encoding="${check_encoding:-false}"
        if [ "${check_encoding}" = "true" ]
        then
            key="$(map_get_key "${node}" "points_encoding")" && 
		points_encoding="${key}"
            grade_file_encoding "${node_base}" \
				"${points_encoding}"
        fi
	
        ## Recurse into sub-nodes. Update first three arguments, keep
        ## the other ones unchanged.
	shift; shift; shift
	set -- $(( level+1 )) "${node}" "${node_prefix}" "$@"
        grade_map "$@"
    done
}
