###
### mkmap_config <dirname> <conffile>
###
##
##  Create the grading map in directory <dirname> of the filesystem
##  using the information in the configuration file <conffile>.
##
##  The map indicates the material to grade, the grading criteria, the
##  unit tests to use and the number of points attached to each
##  criteria.
##
##  The map consists of nodes and key-value pairs:
##
##  node
##    key: value
##
##  Nodes can contain many key-value pairs and can be nested any
##  number of levels deep. Terminal nodes require a key-value pair.
##
##  A configuration map such as
##
##  [category]
##    [subject]
##      key: value
##
##  result in a file <dirname>/[category]/[subject]/key containing the
##  text 'value'.
##
##  Inspired by https://stackoverflow.com/a/691023
mkmap_config ()
{
    [ $# != 2 ] && return 1
    local mapdir="$1"; local conffile="$2"
    local node=
    local indent=

    ## Error if the special node '__global' is missing from the
    ## configuration.
    if ! grep -q "^__global$" "${conffile}"
    then
	error "$(gettext "category '__global' missing from configuration")"
	return 1
    fi

    ## Read the configuration file line by line, omitting lines
    ## starting with a '#' symbol (possibly preceded by whitespace)
    ## and blank lines.
    grep -E -v '^\s*(#|$)' "${conffile}" | while IFS='' read -r line
    do
	## Line is not indented by ${indent} spaces: move up one node
	## level and remove one level of expected indentation.
	while grep -q -v "^${indent}" <<EOF
${line}
EOF
      	do
	    node="${node%/*}"	  # delete one directory level
	    indent="${indent%  }" # delete two spaces
	done

	## Leading spaces are no longer needed from here on.
	line="$(printf "%s" "${line}" | xargs)"

	## Determine whether the line contains a key-value pair
	## (indicated by the presence of the character ':' on the
	## line) or a new node.
	if grep -q ":" <<EOF
${line}
EOF
	then
	    ## Write the value to the file named after the key.
	    map_put_key "${mapdir}/${node}" "${line%%:*}" "${line#*: }"
	else
	    ## Create the node and add two spaces to the expected
	    ## indendation.
	    node="${node}/${line}"
	    map_put_node "${mapdir}/${node}"
	    indent="${indent}  "
	fi
    done
}

###
### mkmap_modules <dirname> <syslib> <userlib> <wd>
###
##
##  Create the map (or dictionary) of available modules (files) in the
##  system library <syslib>, the user library <userlib> and the
##  working directory <wd>.
##
##  The strings <syslib> and <userlib> can contain many directories
##  separated by the character ':'.
##
##  For each module found in the above directories, this will create
##  one file in <dirname>, named after the module and containing the
##  full path to said module.
##
##  Inspired by https://stackoverflow.com/a/4046092
##
mkmap_modules ()
{
    [ $# != 4 ] && return 1
    local mapdir="$1"
    local syslib="$2"; local userlib="$3"; local wd="$4"

    ## Run through the system library, user library and working
    ## directory, in that order, such that the latest module met is
    ## the one that will appear in the map.
    for lib in "${syslib}" "${userlib}" "${wd}"
    do
	## Omit empty libraries. 
	[ -z "${lib}" ] && continue

	## Split the directories in the library and process each
	## directory in turn.
	printf "%s" "${lib}" | \
	awk -F: 'END { for (i = 1; i <= NF; i++) print $i }' | \
	while IFS= read -r dir 
	do
	    ## Find the files in the directory, excluding backup (*~)
	    ## files, and for each make an entry in the map. (Note:
	    ## hidden files and implicitly excluded with the path
	    ## given as '<dir>/*'.)
	    find "${dir}"/* -name "*~" -o -prune -type f -print | \
	    while IFS= read -r f
	    do
		map_put_key "${mapdir}" "${f##*/}" "${f}"
	    done
	done
    done
}

###
### map_get_node <node>
###
##
##  Get the list of nodes (sub-directories) in directory <node>,
##  excluding the special node '__global'.
##
map_get_node ()
{
    [ $# != 1 ] && return 1
    local node="$1"

    find "${node}" -mindepth 1 -maxdepth 1 -type d \
	 -not -name "__global" | sort
}

###
### map_put_node <node>
###
##
##  Create the directory <node>.
##
map_put_node ()
{
    [ $# != 1 ] && return 1
    local node="${1%/}"
    
    [ -d "${node}" ] || mkdir -p "${node}"
}

###
### map_get_key <node> <key>
###
##
##  Get the value in the file <key> in the directory <node>.
##
map_get_key ()
{
    [ $# != 2 ] && return 1
    local node="${1%/}"; local key="$2"
    
    [ -f "${node}/${key}" ] && cat "${node}/${key}"
}

###
### map_put_key <node> <key> <value>
###
##
##  Write <value> in a file <key> in the directory <node>.
##
map_put_key ()
{
    [ $# != 3 ] && return 1
    local node="${1%/}"; local key="$2"; local value="$3"
    
    printf > "${node}/${key}" "%b" "${value}"
}
