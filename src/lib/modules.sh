###
### check_tests_file <filename>
###
##
##  Check that the string <filename> is non empty and that the file
##  exists.
##
check_tests_file ()
{
    [ "${testfile}" ] && [ -f "${testfile}" ]
}

###
### uncommented_string_in_file <pattern> <filename>
###
##
##  Check that the (extended regex) pattern <pattern> matches an
##  uncommented string in file <filename>.
##
uncommented_string_in_file ()
{
    grep -E "$1" "$2" | grep -E -q -v "(^#)|(#.*($1))"
}
