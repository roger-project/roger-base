###
### check_results_file_exists <filename>
###
##
##  Check that the results file <filename> exists.
##
##  The method used is slower than '[ -f <filename> ]', but it ensures
##  that the case is respected on a non-case sensive filesystem (like
##  macOS).
##
check_results_file_exists ()
{
    [ $# -ne 1 ] && return 1
    local filename="$1"

    if [ -z "$(find . -maxdepth 1 -name "${filename}")" ]
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "results file not found")
EOF
	return 1
    fi
}

###
### create_results_branch <branch> <get_sha> <filename>
###
##
##  Create the branch <branch> to hold the results. If <get_sha> is
##  'true', branch from the commit with SHA listed in <filename>;
##  otherwise, branch from HEAD (and <filename> is unused).
##
create_results_branch ()
{
    [ $# -ne 3 ] && return 1
    local branch="$1"
    local get_sha="$2"
    local filename="$3"

    ## The branch should not already exist.
    if check_repos_branch_exists "${branch}"
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(eval_gettext "branch '\${branch}' already exists")
EOF
	return 1
    fi
	
    ## Create the branch, either from the commit with SHA listed
    ## in the results file or from HEAD.
    if [ "${get_sha}" = true ]
    then
	create_branch_from_sha "${branch}" "${filename}" || return 1
    else
	create_branch "${branch}" || return 1
    fi
}

###
### create_branch_from_sha <branch> <filename>
###
##
##  Create a new branch <branch> from the commit with SHA found in the
##  results file <filename>.
##
create_branch_from_sha ()
{
    [ $# -ne 2 ] && return 1
    local branch="$1"; local filename="$2"
    local sha=

    sha=$(extract_sha "${filename}")
    if [ -z "${sha}" ]
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "commit sha not found")
EOF
	return 1
    fi

    if ! git branch "${branch}" "${sha}" >/dev/null 2>&1
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(eval_gettext "error creating branch '\${branch}' from commit '\${sha}'")
EOF
	return 1
    fi
}

###
### extract_sha <filename>
###
##
##  Extract the commit SHA used in grading from the results file
##  <filename>.
##
extract_sha ()
{
    ## Insure against spurious carriage return (\r) inserted at the
    ## end of the line.
    awk '/ [[:xdigit:]]{7,}\r?$/ { print $NF; exit }' "$1" | tr -d '\r'
}

###
### create_branch <branch>
###
##
##  Create a new branch <branch>.
##
create_branch ()
{
    [ $# -ne 1 ] && return 1
    local branch="$1"

    if ! git branch "${branch}" >/dev/null 2>&1
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(eval_gettext "error creating branch '\${branch}'")
EOF
	return 1
    fi
}

###
### check_results_branch <branch>
###
##
##  Verify that the results branch <branch> does exist in the
##  repository.
##
check_results_branch ()
{
    [ $# -ne 1 ] && return 1
    local branch="$1"
    
    if ! check_repos_branch_exists "${branch}"
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(eval_gettext "branch '\${branch}' does not exist")
EOF
	return 1
    fi
}

###
### switch_branch <branch>
###
##
##  Switch to branch <branch>.
##
switch_branch ()
{
    [ $# -ne 1 ] && return 1
    local branch="$1"

    if ! git switch -q "${branch}" 2>/dev/null
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
${msg_error_switching}
EOF
	return 1
    fi
}

###
### add_results <filename>
###
##
##  Index <filename> using the special purpose function
##  'add_with_files'.
##
##  ENVIRONMENT
##
##  The function 'add_with_files' must exist.
##
add_results ()
{
    [ $# -eq 0 ] && return 1

    ## (Note: DO NOT quote $@.)
    if ! add_with_files $@
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "error adding files to the index")
EOF
	return 1
    fi
}

###
### commit_results
###
##
##  Commit results using the special purpose function
##  'commit_with_msg'.
##
##  ENVIRONMENT
##
##  The function 'commit_with_msg' must exist.
##
commit_results ()
{
    if ! commit_with_msg -q
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "error committing files")
EOF
	return 1
    fi
}

###
### push_results
###
##
##  Push the current branch to origin and add upstream reference.
##
push_results ()
{
    if ! git push -q --set-upstream origin "$(git branch --show-current)" >/dev/null 2>&1
    then
	print_msg "${msg_error}\n"
	prepend "${ROGER_ERRORPROMPT}" <<EOF
$(gettext "error pushing branch to origin")
EOF
	return 1
    fi
}
