###
### check_file_exists <path>
###
##
##  Validate that the file (or directory) <path> exists.
##
##  The method used is slower than '[ -f <filename> ]', but it ensures
##  that the case is respected on non-case sensitive filesystems.
##
check_file_exists ()
{
    local filename="$(basename "$1")"
    local dirname="$(dirname "$1")"
    
    [ -n "$(find "${dirname}" -maxdepth 1 -name "${filename}")" ]
}

###
### check_file_encoding <filename>
###
##
##  Check that the character encoding of the file <filename> is UTF-8
##  or ASCII.
##
##  STANDARD OUTPUT
##
##  Type of encoding as returned by 'file'.
##
check_file_encoding ()
{
    local encoding=$(file -b --mime-encoding "$1")

    printf "%s" "$encoding"

    grep -Eq "utf-8|ascii" <<EOF
$encoding
EOF
}

###
### resolve_directory <dirname>
###
##
##  Write the full name of <dirname> (in case it is given as a
##  relative path).
##
##  Parentheses are used in the definition below to run the function
##  in a subshell.
##
resolve_directory ()
(
    cd -- "$1" >/dev/null 2>&1 || return 1
    printf "%s" "${PWD##*/}"
)
