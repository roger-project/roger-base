###
### run_module <dictionary> <module> <filename>
###
##
##  Run the script <module> with <filename> in argument using the path
##  recorded in <dictionary>.
##
##  STANDARD OUTPUT
##
##  Standard output of the module.
##
##  STANDARD ERROR
##
##  Standard error of the module.
##  
run_module ()
{
    ## Function to reliably drop the first word of a string, including
    ## if the string contains only one word (where parameter expansion
    ## does not cut it).
    dropfirst () { shift; echo "$@"; }
    
    local dictionary="$1"
    local module="$2"
    local filename="$3"
    local module_name="${module%% *}"	       # safe
    local module_args="$(dropfirst ${module})" # safe
    local module_path=

    ## Get the full path of the module as recorded in the dictionary
    ## of available modules.
    module_path="$(map_get_key "${dictionary}" "${module_name}")"

    ## Quit if no module found.
    if [ -z "${module_path}" ]
    then
	error "$(eval_gettext "module '\${module_name}' not found")"
	return 1
    fi

    ## Build a command to the module (using its full path) with, if
    ## any, the module arguments given in the configuration file, and
    ## the filename.
    set -- "${module_path}"
    [ "${module_args}" ] && set -- "$@" "${module_args}"
    set -- "$@" "$filename"

    ## Run the module.
    "$@"
}

###
### run_grading_module <dictionary> <module> <testfile>
###
##
##  Run the unit tests in <testfile> using the grading script <module>
##  listed in the directory <dictionary>, reporting the number of
##  tests passed and the number of tests executed.
##
##  If <testfile> is empty, <module> is invoked with an empty
##  argument.
##
##  If <module> is empty, the grading module is inferred from the
##  extension of <testfile>:
##
##    EXTENSION     MODULE
##    .R, .r        grade-r-script
##    .sh           grade-shell-script
##
##  STANDARD OUTPUT
##
##  Standard output of the grading module.
##
##  STANDARD ERROR
##
##  Standard error of the grading module.
##  
run_grading_module ()
{
    local dictionary="$1"
    local module="$2"
    local testfile="$3"; local extension="${testfile##*.}"

    ## If <module> is missing or empty, try to infer the grading
    ## module from the extension of <testfile>.
    if [ -z "${module}" ]
    then
	case ${extension} in
	    R|r)
		module="grade-r-script"
		;;
	    sh)
		module="grade-shell-script"
		;;
	    *)
		error "$(gettext "tests file not associated with a grading module")"
		return 1
	    ;;
	esac
    fi

    ## Run the module.
    run_module "${dictionary}" "${module}" "${testfile}"
}

###
### run_validation_module <dictionary> <module> <filename>
###
##
##  Validate the file <filename> using the validation script <module>
##  listed in the directory <dictionary>.
##
##  If <testfile> is empty, <module> is invoked with an empty
##  argument.
##
##  If <module> is empty, the validation module is inferred from the
##  extension of <filename>:
##
##    EXTENSION     MODULE
##    .R, .r        validate-r-script
##    .Rmd, .rmd    validate-r-rmarkdown
##    .sh           validate-shell-script
##
##  STANDARD OUTPUT
##
##  Standard output of the validation module.
##
##  STANDARD ERROR
##
##  Standard error of the validation module.
##
run_validation_module ()
{
    local dictionary="$1"
    local module="$2"
    local filename="$3"; local extension="${filename##*.}"

    ##  If <module> is missing or empty, try to infer the grading
    ##  module from the extension of <filename>.
    if [ -z "${module}" ]
    then
	case ${extension} in
	    R|r)
		module="validate-r-script"
		;;
	    Rmd|rmd)
		module="validate-r-rmarkdown"
		;;
	    sh)
		module="validate-shell-script"
		;;
	    *)
		error "$(gettext "file not associated with a validation module")"
		return 1
	    ;;
	esac
    fi

    ## Run the module.
    run_module "${dictionary}" "${module}" "${filename}"
}
