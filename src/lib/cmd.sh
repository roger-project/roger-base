###
### check_cmd <cmd>
###
##
##  Check that <cmd> is found from the command line.
##
check_cmd ()
{
    command -v "$1" > /dev/null 2>&1
}
