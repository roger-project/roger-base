## Immutable global variables
SCRIPTNAME=$(basename -- "$0")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
DOC_REQUEST=70

###
### Documentation
###
if [ $# -eq 0 ] || [ "$1" = "-h" ]
then
    ## clone usage information
    print_msg "$(eval_gettext "Usage: \${SCRIPTNAME} \${TOOL} [options...] <pattern> <project>
--api=<bitbucket|gitlab|github>
                           Set REST API of the Git server
-h                         Print this usage information
--help                     Print help text
-m <uri>, --machine=<uri>  Set URI of the Git server to <uri>
-q, --quiet                Suppress progress messages
--rogerrc-file=<filename>  Use <filename> for rogerrc file
[...]                      Other curl options
--                         Mark the end of options")\n"
    exit ${DOC_REQUEST}
fi

if [ "$1" = "--help" ]
then
    ## clone documentation
    print_msg "$(eval_gettext "NAME

\${SCRIPTNAME} \${TOOL} - clone the repositories in a Git project

SYNOPSIS

\${SCRIPTNAME} \${TOOL} [--api=<bitbucket|gitlab|github>] [-h] [--help]
            [-m <uri>|--machine=<uri>] [-q|--quiet] 
            [--rogerrc-file=<filename>] [<curl options>]
            [--] <pattern> <project>

DESCRIPTION

Clone all the repositories in a Git project with URLs matching an
extended regular expression pattern. The list of repositories is
retrieved through a REST API.

A progress indicator is written to the standard error.

The following options are available:

--api=<bitbucket|gitlab|github>
       Set the REST API for the Git server. Overrides the value in the
       rogerrc file for the matching host; see 'Server identification
       and authentication' below. The supported values are:
       'bitbucket' for API version 1.0 of BitBucket Server; 'gitlab'
       for API version 4 of GitLab; 'github' for the API of GitHub.

-h
       Print usage information.

--help
       Print this help text.

-m <uri>, --machine=<uri>
       Host, port and context parts of the Git server URI. Overrides
       the value in the rogerrc file; see 'Server identification and
       authentication' below.

-q, --quiet
       Suppress all output. Progress is not reported to the standard
       error stream.

--rogerrc-file=<filename>
       Path (absolute or relative) to the rogerrc file; see 'Server
       identification and authentication' below.

<curl options>
       All other options are passed to curl as is. The default set of
       curl options is '-n -s'. Option '-m' of this tool masks the
       curl one; use the long form '--max-time' instead.

--
       Mark the end of options, notably the curl ones.

TERMINOLOGY

A Git \"project\" is a collection of repositories that may each have a
different owner. This is the terminology of BitBucket Server. The
equivalent in GitLab is a \"group\"; the closest equivalent in GitHub is
an \"organization\".

SERVER IDENTIFICATION AND AUTHENTICATION

This tool requires the URI, authentication information (username
and password) and type of REST API of the Git server. 

The server identification and type of REST API are taken from the
rogerrc file, by default '.rogerrc' in the user's home directory. The
structure of this file is similar to a netrc file, but without
authentication information. Two tokens are recognized: 'machine' and
'api'. The 'machine' token specifies the host, port and context parts
of the server URI. The 'api' token specifies one of the supported REST
API.

For example, for a project hosted on Université Laval's Faculté des
sciences et de génie BitBucket server the contents of the rogerrc file
would be:

  machine projets.fsg.ulaval.ca/git api bitbucket

For authentication, the tool uses by default option '-n' of curl to
obtain a username and password from a netrc file. See the curl
documentation for details and other authentication methods.

REFERENCE

Atlassian Bitbucket REST API Reference
https://docs.atlassian.com/bitbucket-server/rest/6.9.0/bitbucket-rest.html

GitLab API Docs
https://docs.gitlab.com/ee/api

GitHub Docs REST API
https://docs.github.com/en/rest")\n"
    exit ${DOC_REQUEST}
fi

###
### Curl command build function
###
##
##  Arrays are the only safe way to process commit messages specified
##  with -m|--message. This function builds a 'curl_with_options'
##  function with additional curl options in place using the only
##  array available in POSIX sh: $@.
##
mkcurlcmd ()
{
    local curloptions="-s -n"
    local clear=1
    local lookup_next_arg=0
    
    for arg
    do
	if [ "${clear}" = 1 ]
	then
	    set -- "${curloptions}"
	    clear=0
	fi
	if [ "${lookup_next_arg}" = 1 ] 
	then
	    case "${arg}" in
		--|-*) ;;
		*)     set -- "$@" "\"${arg}\"";;
	    esac
	    lookup_next_arg=0
	fi
	case "${arg}"
	in
	    --api=*|-m|--machine=*|-q|--quiet|--rogerrc-file=*)
		;;
	    --)
		break;;
	    --*|-*)
		set -- "$@" "${arg}"
		lookup_next_arg=1
		;;
	esac
    done

    eval "curl_with_options () { curl $@ \"\$@\"; }"
}

###
### Script actions
###

## Default values of options.
RESTAPI=
MACHINE=
ROGERRC=~/.rogerrc

## Write messages to the standard error stream.
exec 1>&2

## Scan the options a first time to build the 'curl' command with
## options messages in place.
mkcurlcmd "$@"

## Process options a second time for all other options.
while [ $# -gt 0 ]
do
    case "$1" in
	--api=*)
	    RESTAPI="${1#*=}"
	    ;;
	-m)
	    MACHINE="$2"; shift
	    ;;
	--machine=*)
	    MACHINE="${1#*=}"
	    ;;
	-q|--quiet)
	    exec 1>/dev/null
	    ;;
	--rogerrc-file=*)
	    ROGERRC="${1#*=}"
	    case "${ROGERRC}" in
		"~/"*) ROGERRC="${HOME}/${ROGERRC#"~/"}";;
	    esac
	    ;;
	--)
	    shift; break;;
	--*|-*)
	    case "$2" in
		--|-*) ;;
		*)     shift;;
	    esac
	    ;;
	*)
	    break;;
    esac
    shift
done

## Get the pattern for the names of the repositories to clone; now the
## first argument. Quit if neither a pattern or a project key is given
## in argument.
PATTERN="$1"; shift
[ -z "${PATTERN}" ] &&
    error_exit "$(gettext "missing pattern and project key")"

## Get the project key; now the first argument. Quit if no project key
## is given in argument.
PROJECTKEY="$1"
[ -z "${PROJECTKEY}" ] &&
    error_exit "$(gettext "missing project key")"

## If the Git server URI is not set with option -m, read it from the
## rogerrc file. Quit if the file does not exist or if no URI is found
## in the file.
if [ -z "${MACHINE}" ]
then
    [ ! -f "${ROGERRC}" ] &&
	error_exit "${msg_missing_rogerrc}: ${ROGERRC}"
    MACHINE=$(awk '/^machine/ { print $2 }' "${ROGERRC}")
    [ -z "${MACHINE}" ] &&
	error_exit "$(gettext "missing server URI")"
fi

## If the Git server REST API is not set with option --api, read it
## from the rogerrc file for the matching host. Quit if the file does
## not exist or if no API is found in the file.
if [ -z "${RESTAPI}" ]
then
    [ ! -f "${ROGERRC}" ] &&
	error_exit "${msg_missing_rogerrc}: ${ROGERRC}"
    RESTAPI=$(awk -v host="${MACHINE}" \
		  '/^machine/ && $2 == host { if ($3 == "api") print $4 }' \
		  "${ROGERRC}")
    [ -z "${RESTAPI}" ] &&
	error_exit "$(gettext "missing server REST API")"
fi

## Set the auxiliary function to retrieve the list of repositories in
## the project.
case "${RESTAPI}" in
    bitbucket)
	GETREPOS="getrepos_bitbucket"
	;;
    github)
	GETREPOS="getrepos_github"
	;;
    gitlab)
	GETREPOS="getrepos_gitlab"
	;;
    *)
	error_exit "$(gettext "unsupported REST API")"
	;;
esac

## Fetch the urls of the repositories.
REPOS=$(${GETREPOS} "${MACHINE}"     \
		    "${PROJECTKEY}") || exit 1

## Filter the list of repositories to those matching the pattern in
## argument.
REPOS=$(grep -E "${PATTERN}" <<EOF
${REPOS}
EOF
     )

## Stop if there are no repositories to clone.
[ -z "${REPOS}" ] &&
    error_exit "$(eval_gettext "no repository matching '\${PATTERN}'")"

## The tool displays a progress count. Count the number of
## repositories (in a portable way) and initialize the counter.
nrepos=$(wc -l <<EOF | awk '{print $1}'
${REPOS}
EOF
     )
i=0

for repos in ${REPOS}
do
    i=$((i + 1))
    (
	## Print the name of the directory being processed.
	r="${repos##*/}"
	print_msg "$(eval_gettext "Cloning repository \${r}") (${i}/${nrepos})... "

	## Clone the repository
	git clone -q "${repos}" 2>/dev/null || exit 1

	## Confirm all went well.
	print_msg "${msg_ok}\n"
    ) || print_msg "${msg_error}\n"
done

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
