###
### validate-repos-authors <pattern> <filename>
###
##
##  Write the list of unique authors in a Git repository to standard
##  output, sorted alphabetically, omitting names matching the
##  extended regular expression pattern <pattern>. Argument <filename>
##  is unused.
##
##  STANDARD OUTPUT
##
##  Progress messages.
##
##  STANDARD ERROR
##
##  The list of authors, one per line.
##  - or -
##  Warning that no authors were found, followed by the pattern used
##  to omit authors.
##

[ $# -ne 2 ] && exit 1
pattern="$1"

## Write the start of the progress message to stdout.
print_msg "$(gettext "Checking the authors in the repository")... "

## Extract the list of unique authors in the repository, sorted
## alphabetically, and exclude the names matching the pattern.
authors=$(git log --pretty="%an" | sort | uniq | \
	      grep -v -E "${pattern}")

## If no authors are found, terminate the progress message with a
## warning and write a message and the pattern to stderr.
if [ -z "${authors}" ]
then
    print_msg "${msg_warning}\n"
    error "$(gettext "no author found")"
    error "$(gettext "exclusions"): ${pattern}"
    exit "${ROGER_WARNINGCODE}"
fi

## Authors are found: terminate the progress message with a note and
## write the list of authors to stderr.
print_msg "${msg_note}\n"
error "${authors}"
exit "${ROGER_NOTECODE}"

### Local Variables: ***
### mode: sh ***
### End: ***
