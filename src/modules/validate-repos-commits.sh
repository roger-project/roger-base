###
### validate-repos-commits <min> <filename>
###
##
##  Check that the repository contains the minimum required number
##  of commits <min>. Argument <filename> is unused.
##
##  STANDARD OUTPUT
##
##  Progress messages.
##
##  STANDARD ERROR
##
##  Message stating the minimum number of commits expected in the
##  repository.
##

[ $# -ne 2 ] && exit 1
min="$1"

## Write the start of the progress message to stdout.
print_msg "$(gettext "Checking the number of commits")... "

## Get the number of commits in the repository.
ncommits="$(git rev-list --count --all)"

## Write the number of commits found to stdout.
print_msg "${ncommits}... "

## If the minimum number of commits is not reached, terminate the
## progress message with an warning marker, write the target to
## stderr and exit with an error code.
if [ "${ncommits}" -lt "${min}" ]
then
    print_msg "${msg_warning}\n"
    error "$(gettext "minimum required number of commits"): ${min}"
    exit "${ROGER_WARNINGCODE}"
fi

## Write the end of the progress message to stdout.
print_msg "${msg_ok}\n"

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
