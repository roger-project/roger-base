###
### validate-r-rmarkdown <filename>
###
##
##  Check that the R Markdown file <filename> renders without errors
##  in an R session started with '--quiet --no-restore --nosave'.
##
##  STANDARD OUTPUT
##
##  Nothing.
##
##  STANDARD ERROR
##
##  Messages from the preliminary checks or R error messages, if any.
##

[ $# -ne 1 ] && exit 1
filename="$1"
errors=

## Write the start of the progress message to stdout.
print_msg "$(eval_gettext "Checking the R Markdown document in \${filename}")... "

## Check that R is found in the path; exit with an error if not the
## case.
if ! check_cmd R
then
    print_msg "${msg_error}\n"
    error "$(gettext "R command not found; configure your terminal adequately")"
    exit "${ROGER_ERRORCODE}"
fi

## Check that the file does not try to render itself; exit with an
## error if the case.
if uncommented_string_in_file "render\((\"${filename}\")|('${filename}')\)" "${filename}"
then
    print_msg "${msg_error}\n"
    error "$(gettext "the file tries to render itself with 'render'")"
    exit "${ROGER_ERRORCODE}"
fi

## Render the file.
##
## The standard error of the R process is saved in a variable, letting
## standard output through (and then to /dev/null, here).
## (https://stackoverflow.com/a/13806684, adapted for POSIX shell)
{
    errors="$(R --quiet --no-restore --no-save 2>&1 1>&3 3>&- <<EOF
options("conflicts.policy" = list(warn = FALSE))
library("rmarkdown")
render("${filename}", encoding = "UTF-8", quiet = TRUE)
EOF
	)";
} 3>/dev/null

## If there are any error messages, terminate the progress message
## with an error marker, write the messages to stderr and exit with an
## error code.
if [ "${errors}" ]
then
    print_msg "${msg_error}\n"
    error "${errors}"
    exit "${ROGER_ERRORCODE}"
fi

## Write the end of the progress message to stdout.
print_msg "${msg_ok}\n"

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
