###
### grade-repos-commits <min> <testfile>
###
##
##  Check that the repository contains the minimum required number of
##  commits <min>. Argument <testfile> is unused.
##
##  STANDARD OUTPUT
##
##  Number of commits in the repository and required minimum number of
##  commits, in this order, separated by a space.
##
##  STANDARD ERROR
##
##  If the minimum number of commits in the repository is not reached,
##  a message stating the target.
##

[ $# -ne 2 ] && exit 1
min="$1"

## Get the number of commits in the repository.
ncommits="$(git rev-list --count --all)"

## Report the number of commits and required minimum.
print_msg "${ncommits} ${min}"

## If the minimum number of commits is not reached, write the target
## to stderr flagged as a warning.
if [ "${ncommits}" -lt "${min}" ]
then
    error "$(gettext "minimum required number of commits"): ${min}"
    exit "${ROGER_WARNINGCODE}"
fi

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
