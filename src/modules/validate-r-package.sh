###
### validate-r-package <dirname>
###
##
##  Validate that checking the R package in directory <dirname> with
##  'R CMD check' completes without errors or warnings.
##
##  STANDARD OUTPUT
##
##  Nothing.
##
##  STANDARD ERROR
##
##  Messages from the preliminary checks, 'R CMD build' error
##  messages, if any, or 'R CMD check' log in case of errors or
##  warnings.
##

[ $# -ne 1 ] && exit 1
dirname="$1"

## Write the start of the progress message to stdout.
print_msg "$(eval_gettext "Checking the R package in \${dirname}")... "

## Check that R is found in the path; exit with an error if not the
## case.
if ! check_cmd R
then
    print_msg "${msg_error}\n"
    error "$(gettext "R command not found; configure your terminal adequately")\n"
    exit "${ROGER_ERRORCODE}"
fi

## Temporary file to hold the standard error of the R process.
errors=$(mktemp)

## Build the package.
if ! R CMD build "$1" >${errors}
then
    print_msg "${msg_error}\n"
    error "$(cat ${errors})\n"
    exit "${ROGER_ERRORCODE}"
fi

## Extract the package file name from the output of 'R CMD build'. The
## file name is enclosed in single quotes by R using 'sQuote'. In a
## UTF-8 locale, the quotes are the left and right single quotation
## mark (\u2018, \u2019), a.k.a. "fancy quotes" or "smart quotes".
## Otherwise, the standard single quote (or apostrophe) is used.
##
## We need to remove the quotes around the file name. The first two
## characters in the 'gsub' call below are the Unicode smart quotes
## \u2018 and \u2019. The sequence «'\''» matches the regular single
## quote. The grave accent is included for good measure; I don't think
## R uses it.
pkgfile=$(awk '/^\* building/ \
                { \
		    gsub(/[‘’'\''`]/, "", $3); \
		    print $3 \
		}' ${errors})

## Safety measure: check that the package file exists.
if [ ! -f "${pkgfile}" ]
then
    print_msg "${msg_error}\n"
    error "$(eval_gettext "package archive '\${pkgfile}' not found")"
    exit "${ROGER_ERRORCODE}"
fi
    
## Check the package.
if ! R CMD check "${pkgfile}" >${errors} 2>/dev/null || \
	grep -E -q "WARNING" ${errors}
then
    print_msg "${msg_error}\n"
    error "$(cat ${errors})\n"
    exit "${ROGER_ERRORCODE}"
fi

## Write the end of the progress message to stdout.
print_msg "${msg_ok}\n"

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
