###
### validate-r-shiny <dirname>
###
##
##  Validate that compilation of the Shiny app in directory <dirname>
##  completes without errors in an R session started with '--quiet
##  --no-restore --nosave'.
##
##  STANDARD OUTPUT
##
##  Nothing.
##
##  STANDARD ERROR
##
##  Messages from the preliminary checks or R error messages, if any.
##

[ $# -ne 1 ] && exit 1
dirname="$1"

## Write the start of the progress message to stdout.
print_msg "$(eval_gettext "Checking the Shiny app in \${dirname}")... "

## Check that R is found in the path; exit with an error if not the
## case.
if ! check_cmd R
then
    print_msg "${msg_error}\n"
    error "$(gettext "R command not found; configure your terminal adequately")"
    exit "${ROGER_ERRORCODE}"
fi

## Temporary file to hold the standard error of the R process.
errors=$(mktemp)

## Launch the app in the background and wait one second to check if it
## worked or not. An active R process indicates success and we may
## then kill the process.
##
## The standard error of the R process is printed only in the case
## of failure to launch the app.
##
## Launch the app in a sub-shell to get rid of standard process
## management messages.
if ! (
	R --quiet --no-restore --no-save \
	  >/dev/null 2>${errors} <<EOF & sleep 1 && \
	  kill -0 "$!" >/dev/null 2>&1 && kill "$!" >/dev/null 2>&1
options("conflicts.policy" = list(warn = FALSE))
library("shiny")
runApp("${dirname}", quiet = TRUE, launch.browser = FALSE)
EOF
    ) >/dev/null 2>&1
then
    print_msg "${msg_error}\n"
    error "$(cat ${errors})"
    exit "${ROGER_ERRORCODE}"
fi

## Write the end of the progress message to stdout.
print_msg "${msg_ok}\n"

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
