###
### validate-shell-script <filename>
###
##
##  Validate the syntax of the shell script <filename> with the shell
##  mentioned in the shebang.
##
##  STANDARD OUTPUT
##
##  Nothing.
##
##  STANDARD ERROR
##
##  Messages from the shell.
##

[ $# -ne 1 ] && exit 1
filename="$1"
shebang=
errors=

## Write the start of the progress message to stdout.
print_msg "$(eval_gettext "Checking the shell script \${filename}")... "

case "$(sed '1s/[ \t]*$//;1s/#!//;q' "${filename}")" in
    /bin/sh)
	errors="$(/bin/sh -n "${filename}" 2>&1)"
	;;
    /bin/bash)
	errors="$(/bin/bash -n "${filename}" 2>&1)"
	;;
    /bin/zsh)
	errors="$(/bin/zsh -n "${filename}" 2>&1)"
	;;
    *)
	errors="$(gettext "unknown or unsupported shell")"
	;;
esac

## If there are any error messages, terminate the progress message
## with an error marker, write the messages to stderr and exit with an
## error code.
if [ "${errors}" ]
then
    print_msg "${msg_error}\n"
    error "${errors}"
    exit "${ROGER_ERRORCODE}"
fi

## Write the end of the progress message to stdout.
print_msg "${msg_ok}\n"

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
