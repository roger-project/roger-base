###
### grade-repos-authors <pattern> <testfile>
###
##
##  Write the list of unique authors in a Git repository to standard
##  error, sorted alphabetically, omitting names matching the extended
##  regular expression pattern <pattern>. Argument <testfile> is
##  unused.
##
##  STANDARD OUTPUT
##
##  "0 0" since this criteria is impossible to grade.
##
##  STANDARD ERROR
##
##  The list of authors, one per line.
##  - or -
##  Warning that no authors were found, followed by the pattern used
##  to omit authors.
##

[ $# -ne 2 ] && exit 1
pattern="$1"

## Extract the list of unique authors in the repository, sorted
## alphabetically, and exclude the names matching the pattern.
authors=$(git log --pretty="%an" | sort | uniq | \
	      grep -v -E "${pattern}")

## Write the "results", always the same.
print_msg "0 0"

## If no authors are found, write a warning message and the pattern to
## stderr.
if [ -z "${authors}" ]
then
    error "$(gettext "no author found")\n"
    error "$(gettext "exclusions"): ${pattern}\n"
    exit "${ROGER_WARNINGCODE}"
fi

## Authors are found: write the list of authors to stderr as a note.
error "${authors}"
exit "${ROGER_NOTECODE}"

### Local Variables: ***
### mode: sh ***
### End: ***
