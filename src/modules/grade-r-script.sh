###
### grade-r-script <testfile>
###
##
##  Check the validity of the unit tests in the R script <testfile>
##  using function 'run_test_file' from package tinytest. Therefore,
##  <testfile> must be compatible with this function.
##
##  STANDARD OUTPUT
##
##  Number of tests passed and number of tests executed, in this
##  order, separated by a space, or "0 0" if no executed test.
##
##  STANDARD ERROR
##
##  Standard error of the R process.
##

[ $# -ne 1 ] && exit 1
testfile="$1"

## Quit with an error if no tests file found.
check_tests_file "${testfile}" ||
    { error "${msg_missing_tests_file}"; exit 1; }

## Create a symbolic link to <testfile> in the current working
## directory. (On Windows, do a full copy as symlinks are not
## guaranteed to work.) Needed by 'tinytest::run_test_file' that uses
## the directory of the tests file as working directory.
testfile="___${testfile##*/}"
if running_on_windows
then
    cp -p "$1" "${testfile}" 1>/dev/null || exit 1
else
    ln -s "$1" "${testfile}" 1>/dev/null || exit 1
fi

## The standard error of the R process is saved in a variable, letting
## standard output through. (https://stackoverflow.com/a/13806684,
## adapted for POSIX shell)
##
## The only standard output that we want are the test summaries.
## Therefore, any eventual output while running the tests is
## discarded with 'capture.output'.
##
## When at least one test executes, the result of the expression
## 'summary(run_test_file(...))' is a 3-column array containing: the
## number of tests executed ("Results"); the number of failed tests
## ("fails"); the number of passed tests ("passes"). We only need the
## totals of the last line.
##
## If tests do not execute for some reason, 'run_test_file' works as
## expected, but not 'summary' on the resulting object. Treat this
## case specially to return "0 0".
{
    errors="$(R --quiet --no-restore --no-save --no-echo 2>&1 1>&3 3>&- <<EOF
library("tinytest") 
out <- tryCatch(
{
    capture.output(res <- run_test_file("${testfile}", verbose = 0),
                   type = "output", file = nullfile())
    cat(summary(res)["Total", c("passes", "Results")], fill = TRUE)
    0 # return code for success
},
error = function(e) {
    message(e)
    cat("0 0", fill = TRUE)
    1 # return code for error
}  
)
quit(status = out)
EOF
	)"

    ## Add an error message when no test executed. Done here to avoid
    ## sending non-ascii characters (from translations) in the R
    ## script above.
    if [ $? -ne 0 ]
    then
	errors="$(gettext "errors in the R script; no test executed")\n${errors}"
    fi
} 3>&1

## Cleanup
rm "./${testfile}" 1>/dev/null || exit 1

## Write error messages to stderr, if any.
if [ "${errors}" ]
then
    error "${errors}"
    exit "${ROGER_ERRORCODE}"
fi

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
