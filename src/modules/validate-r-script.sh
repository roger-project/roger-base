###
### validate-r-script <filename>
###
##
##  Check that the R script <filename> is portable and parses with
##  'source' in an R session started with '--quiet --no-restore
##  --nosave'.
##
##  The script is considered portable if none of the following is
##  found in the file:
##
##  - calls to 'setwd';
##  - calls to 'install.packages';
##  - local paths ('~/', '../' or Windows path of the form 'C:\' or
##    'C:/');
##  - calls to 'source(<filename>)'.
##
##  STANDARD OUTPUT
##
##  Progress messages.
##
##  STANDARD ERROR
##
##  Messages from the preliminary checks or R error messages, if any.
##

[ $# -ne 1 ] && exit 1
filename="$1"
errors=

## Write the start of the progress message to stdout.
print_msg "$(eval_gettext "Checking the R source code in \${filename}")... "

## Check that R is found in the path; exit with an error if not the
## case.
if ! check_cmd R
then
    print_msg "${msg_error}\n"
    error "$(gettext "R command not found; configure your terminal adequately")"
    exit "${ROGER_ERRORCODE}"
fi

## Check that the source code is portable; accumulate diagnostic
## messages and exit with an error if not the case.
uncommented_string_in_file "setwd\([^)]*\)" "${filename}" && \
    errors+="$(gettext "script contains calls to 'setwd'")\n"
uncommented_string_in_file "install\.packages\([^)]*\)" "${filename}" && \
    errors+="$(gettext "script contains calls to 'install.packages'")\n"
uncommented_string_in_file "(~/)|(\.\./)|([A-Za-z]:[/\][^/\])" "${filename}" && \
    errors+="$(gettext "script contains paths to local resources")\n"
uncommented_string_in_file "source\((\"${filename}\")|('${filename}')\)" "${filename}" && \
    errors+="$(gettext "script calls itself with 'source'")\n"

if [ "${errors}" ]
then
    print_msg "${msg_error}\n"
    error "${errors}\n"
    exit "${ROGER_ERRORCODE}"
fi

## Parse script with 'source'.
##
## The standard error of the R process is saved in a variable, letting
## standard output through (to /dev/null, here).
## (https://stackoverflow.com/a/13806684, adapted for POSIX shell)
{
    errors="$(R --quiet --no-restore --no-save 2>&1 1>&3 3>&- <<EOF
options("conflicts.policy" = list(warn = FALSE))
source("${filename}")
EOF
	)";
} 3>/dev/null

## If there are any error messages, terminate the progress message
## with an error marker, write the messages to stderr and exit with an
## error code.
if [ "${errors}" ]
then
    print_msg "${msg_error}\n"
    error "${errors}\n"
    exit "${ROGER_ERRORCODE}"
fi

## Write the end of the progress message to stdout.
print_msg "${msg_ok}\n"

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
