###
### grade-shell-script <testfile>
###
##
##  Check the validity of the unit tests in the shell script
##  <testfile> using the shell mentioned in the shebang, /bin/sh by
##  default.
##
##  STANDARD OUTPUT
##
##  Number of tests passed and number of tests executed, in this
##  order, separated by a space, or "0 0" if no executed test.
##
##  STANDARD ERROR
##
##  Standard error of the unit test script.
##

[ $# -ne 1 ] && exit 1
testfile="$1"

## Quit with an error if no tests file found.
check_tests_file "${testfile}" ||
    { error "${msg_missing_tests_file}"; exit 1; }

## The standard error of the shell is saved in a variable, letting
## standard output through. (https://stackoverflow.com/a/13806684,
## adapted for POSIX shell)
{
    case "$(sed '1s/[ \t]*$//;1s/#!//;q' "${testfile}")" in
	/bin/bash)
	    errors="$(/bin/bash "${testfile}" 2>&1 1>&3 3>&-)"
	    ;;
	/bin/zsh)
	    errors="$(/bin/zsh "${testfile}" 2>&1 1>&3 3>&-)"
	    ;;
	*)
	    errors="$(/bin/sh "${testfile}" 2>&1 1>&3 3>&-)"
	    ;;
    esac;
} 3>&1

## Write error messages to stderr, if any.
if [ "${errors}" ]
then
    error "${errors}"
    exit "${ROGER_ERRORCODE}"
fi

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
