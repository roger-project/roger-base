## Immutable global variables
SCRIPTNAME=$(basename -- "$0")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
DOC_REQUEST=70

###
### Documentation
###
if [ "$1" = "-h" ]
then
    ## checkreq usage information
    print_msg "$(eval_gettext "Usage: \${SCRIPTNAME} \${TOOL} [options...]
-f <file>, --file=<file>   Use <file> as requirements file
-h                         Print this usage information
--help                     Print help text")\n"
    exit ${DOC_REQUEST}
fi

if [ "$1" = "--help" ]
then
    ## checkreq documentation
    print_msg "$(eval_gettext "NAME

\${SCRIPTNAME} \${TOOL} - check for required grading tools

SYNOPSIS

\${SCRIPTNAME} \${TOOL} [-f <file>|--file=<file>] [-h] [--help]

DESCRIPTION

Check for the availability of the commands and R packages required to
grade a project listed in a requirement file, by default
'requirements.txt'.

The following options are available:

-f <file>, --file=<file>
       Use <file> as requirements file.

-h
       Print usage information.
  
--help
       Print this help text.

FORMAT OF THE REQUIREMENTS FILE

The requirements file lists the tools required to grade projects from
the command line and within R. The format of the file is as follows:

- a line starting with # is considered as a comment and is ignored;
- only one word is allowed per line;

- if the word starts by 'rpackage:', the text following the colon ':'
  is taken as the name of an R package;
- any other word is taken as the name of a command that must be 
  available from the command line.")\n"
    exit ${DOC_REQUEST}
fi

###
### Script actions
###

## Default name of the requirements file.
FILE="requirements.txt"

## Process options.
while [ $# -gt 0 ]
do
    case "$1" in
	-f)
	    FILE="$2"; shift
	    ;;
	--file=*)
	    FILE="${1#*=}"
	    ;;
	*)
	    break;;
    esac
    shift
done

## Expand ~ in ${FILE} if needed.
case "${FILE}" in
    "~/"*) FILE="${HOME}/${FILE#"~/"}";;
esac

## Check that requirements file exists; quit otherwise.
[ -f "${FILE}" ] || \
    error_exit "${SCRIPTNAME}: $(gettext "requirements file not found"): ${FILE}"

## Check availability of commands.
print_msg "$(gettext "Checking commands")\n"
sed -e '/^#/d ; /^rpackage:/d ; /^[[:space:]]*$/d' "${FILE}" | 
    while read -r cmd
    do
	print_msg "  $(gettext "availability of command") '${cmd}'... "
	if ! check_cmd "${cmd}"
	then
	    print_msg "${msg_error}\n"
	    code=1
	else
	    print_msg "${msg_ok}\n"
	fi
    done

## Check availability of R packages. R is repeatedly called for each
## package. This strategy is not very efficient, but it allows to
## write the messages outside the shell side and, therefore, to avoid
## sending non-ascii characters (coming from translations) to the R
## script.
print_msg "$(gettext "Checking R packages")\n"
grep '^rpackage:' "${FILE}" | sed 's/rpackage://' |
    while read -r pkg
    do
	print_msg "  $(gettext "availability of package") '${pkg}'... "
	if R --quiet --no-restore --no-save --no-echo <<EOF
options(warn = -1, conflicts.policy = list(warn = FALSE))
quit(status = require("${pkg}", quietly = TRUE, character.only = TRUE))
EOF
	then
	    print_msg "${msg_error}\n"
	    code=1
	else
	    print_msg "${msg_ok}\n"
	fi
    done

exit ${code:-0}

### Local Variables: ***
### mode: sh ***
### End: ***
