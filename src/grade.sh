## Immutable global variables
SCRIPTNAME=$(basename -- "$0")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
DOC_REQUEST=70

## Other default global variables needed from here.
SCRIPTCONF="${TOOL}conf"
DATETIMELIMIT=$(date "+%Y-%m-%d %H:%M:%S")

###
### Documentation
###
if [ "$1" = "-h" ]
then
    ## grade usage information
    print_msg "$(eval_gettext "Usage: \${SCRIPTNAME} \${TOOL} [options...] [<directory>]
-c <file>, --config-file=<file>
                           Use <file> as configuration file
-d, --detached-head        Leave repository in detached HEAD state
-h                         Print this usage information
--help                     Print help text
-l <timestamp>, --time-limit=<timestamp>
                           Grade the repository as of <timestamp>
-o <file>, --output-file=<file>
                           Write the results in <file>
-q, --quiet                Suppress progress messages")\n"
    exit ${DOC_REQUEST}
fi

if [ "$1" = "--help" ]
then
    ## grade documentation
    print_msg "$(eval_gettext "NAME

\${SCRIPTNAME} \${TOOL} - grade a project

SYNOPSIS

\${SCRIPTNAME} \${TOOL} [-c <file>|--config-file=<file>] 
            [-d|--detached-head] [-h] [--help]
            [-l <timestamp>|--time-limit=<timestamp>]
            [-o <file>|--output-file=<file>] [-q|--quiet]
            [<directory>]

DESCRIPTION

Perform automated grading of the projects in the directories in
argument (or, by default, the working directory) and print results to
the standard output.

The <directory> may be a Git repository. Grading for repositories is
performed at the last commit before a timestamp, possibly for a
specific tag.

Grading is based on criterias and the execution of unit tests using
grading modules; see '\${SCRIPTNAME} --help-module' for details on grading
modules.

The projects to grade may contain any combination of Git branches,
directories and files of source code or raw text. The material to
grade is specified in a configuration file; see 'Configuration' below.

Results are written to the standard output. A progress indicator is
written to the standard error.

The following options are available:

-c <file>, --config-file=<file>
       Use <file> as grading configuration file instead of the default
       '\${SCRIPTCONF}'.

-d, --detached-head
       Leave the repository in detached HEAD state after grading. By
       default the tool returns to the main branch after automatic
       grading is completed. With this option, the repository is left
       in the state in which it was graded. This is useful if manual
       intervention is needed before grading is deemed complete. Use
       '\${SCRIPTNAME} switch' to return the repository to the main
       branch.

-h
       Print usage information.

--help
       Print this help text.

-l <timestamp>, --time-limit=<timestamp>
       Grade the repository as of <timestamp> instead of the current
       date and time. The value of <timestamp> is a character string
       representing a date in ISO 8601 format: \"YYYY-MM-DD HH:MM:SS\".

-o <file>, --output-file=<file>
       Write the results in specified <file> in <directory>. If <file>
       is '-', output is written to the standard output.

-q, --quiet
       Suppress all output other than gradings results. Progress is
       not reported to the standard error stream.

CONFIGURATION

See '\${SCRIPTNAME} --help-config' for general information on the
grading configuration file.

The '__global' node for this tool uses the following nodes and keys
(elements between [ ] are optional):

__global
  title: <title of the project>
  [repository]
    [check_repos: <true|false>]
    [main_branch: <name>]
    [tag_name: <name>]
  [defaults]
    [check_exists_laden: <true|false>]
    [check_contents: <true|false>]
    [check_contents_module: <filename> [<args>]]
    [check_encoding: <true|false>]
    [points_encoding: <value>]

If the key 'check_repos' is true and the directory is not a valid Git
repository, a grade of 0 is written to the results and grading of a
project stops.

The other nodes of the configuration identify elements to grade using
the following structure:

<node>
  [description: <description>]
  [recursive: <true|false>]
  [subtotal: <true|false>]
  [check_branch: <true|false>]
  [check_exists_laden: <true|false>]
  [check_contents: <true|false>]
  [check_contents_module: <filename> [<args>]]
  [check_encoding: <true|false>]
  [points_encoding: <value>]
  [<criteria>]
    description: <description>
    [grade_criteria_module: <filename> [<args>]]
    tests: <filename>
    points: <value>

Omitted boolean keys are 'false' by default. The 'check_*' keys are
processed in the order above.

The name or a node or its 'description' key is printed in the results,
except for nodes with the special name '.PHONY'. The text in preceded
by a newline and a prefix: '###' for the first level; '##' for the
second level; '#' for the third level; nothing for all deeper levels.

If the key 'check_branch' is 'true', the tool will check that the
branch exists and attempt to switch to it. Grading of the node and its
descendants stops if either action fails.

If the key 'check_exists_laden' is 'true', this tool only checks that
the node exists and is not be empty. If either of the conditions
fails, grading of the node and its descendants stops.

If the key 'check_contents' is 'true' and 'check_contents_module' is
missing, the validation module is inferred from the extension of the
node (when a file). If the validation fails, a grade of 0 is written
to the results and grading of the node and its descendants stops.

A criteria is graded when at least one of the 'tests' and
'grade_criteria_module' keys of the criteria node is specified.
Otherwise, the description and number of points are written to the
results without further action. If the 'grade_criteria_module' key is
missing, the grading module is inferred from the extension of the
tests file. If the 'tests' key is missing, the grading module is
invoked with an empty argument.

The grade for a criteria is proportional to the number of tests passed
as reported by the grading module. If the number of executed tests is
0, the grade is left blank. Diagnostic messages, if any, are reported
after the result.

Diagnostic messages from grading and validation modules are reported
in the results, prefixed by a type-specific marker:

  '\${ROGER_ERRORPROMPT}' for errors;
  '\${ROGER_WARNINGPROMPT}' for warnings;
  '\${ROGER_NOTEPROMPT}' for notes.

FILES

This tool requires that the following files be present in the current
working directory:

- \${SCRIPTCONF}, the grading configuration file (unless a different
  file is specified with option '-c');
- the unit test files specified in the configuration file.

ENVIRONMENT

The tool assumes that '\${SCRIPTNAME} checkreq' completes without error for the
project to grade.

Automatic grading of R scripts requires the 'tinytest' package.

EXAMPLES

The command

  \${SCRIPTNAME} \${TOOL}

grades the project in the current working directory as of its most
recent cloning. Grading results are written to the standard output.

The command

  \${SCRIPTNAME} \${TOOL} -c gradeconf-conception project

grades the project in directory 'project' based on the configuration
found in the file 'gradeconf-conception'.

The command

  \${SCRIPTNAME} \${TOOL} --time-limit=\"2020-04-30 23:59:59\" project

grades the project in directory 'project' in its version as of
2020-04-30 23:59:59.

The command

  \${SCRIPTNAME} \${TOOL} -o GRADING.txt [0-9]*/

grades the projects in all the subdirectories of the current working
directory with names starting with a digit and, for every project,
writes the results in a file 'GRADING.txt' in the corresponding
directory.")\n"
    exit ${DOC_REQUEST}
fi

###
### Script actions
###

## Process options.
while [ $# -gt 0 ]
do
    case $1 in
	-c)
	    SCRIPTCONF="$2"; shift
	    ;;
	--config-file=*)
	    SCRIPTCONF="${1#*=}"
	    ;;
	-d|--detached-head)
	    SWITCHTOMAIN=false
	    ;;
	-l)
	    DATETIMELIMIT="$2"; shift
	    ;;
	--time-limit=*)
	    DATETIMELIMIT="${1#*=}"
	    ;;
	-o)
	    OUTPUT="$2"; shift
	    ;;
	--output-file=*)
	    OUTPUT="${1#*=}"
	    ;;
	-q|--quiet)
	    exec 2>/dev/null
	    ;;
	*)
	    break;;
    esac
    shift
done

## The directories to grade are now the remaining arguments; grade the
## working directory if there are none.
[ $# -eq 0 ] && set -- "${PWD}"

## Expand ~ in ${SCRIPTCONF} if needed.
case "${SCRIPTCONF}" in
    "~/"*) SCRIPTCONF="${HOME}/${SCRIPTCONF#"~/"}";;
esac

## Quit if the configuration file is not found.
[ ! -f "${SCRIPTCONF}" ] &&
    error_exit "${msg_config_not_found}"

## Setup the grading map in a temporary directory (solution valid for
## Linux/Windows and macOS: https://unix.stackexchange.com/a/84980).
MAPDIR="$(mktemp -d 2>/dev/null || mktemp -d -t "${SCRIPTNAME}")"
mkmap_config "${MAPDIR}" "${SCRIPTCONF}" ||
    error_exit "$(gettext "error setting up the grading map")"

## Fetch global variables in the grading map.
TITLE=$(map_get_key "${MAPDIR}/__global" "title")
GIT_REPOS="false"
if [ -d "${MAPDIR}/__global/repository" ]
then
    GIT_REPOS="true"
    CHECK_REPOS="$(map_get_key "${MAPDIR}/__global/repository" "check_repos")"
    CHECK_REPOS="${CHECK_REPOS:-false}"
    MAIN="$(map_get_key "${MAPDIR}/__global/repository" "main_branch")"
    TAG_NAME="$(map_get_key "${MAPDIR}/__global/repository" "tag_name")"
fi

## Fetch default values of keys from the grading map. Each of the
## variables below may end up empty.
if [ -d "${MAPDIR}/__global/defaults" ]
then
    CHECK_EXISTS_LADEN="$(map_get_key "${MAPDIR}/__global/defaults" "check_exists_laden")"
    CHECK_CONTENTS="$(map_get_key "${MAPDIR}/__global/defaults" "check_contents")"
    CHECK_CONTENTS_MODULE="$(map_get_key "${MAPDIR}/__global/defaults" "check_contents_module")"
    CHECK_ENCODING="$(map_get_key "${MAPDIR}/__global/defaults" "check_encoding")"
    POINTS_ENCODING="$(map_get_key "${MAPDIR}/__global/defaults" "points_encoding")"
fi

## Setup the module map in a temporary directory (solution valid for
## Linux/Windows and macOS: https://unix.stackexchange.com/a/84980).
MODULEDIR="$(mktemp -d 2>/dev/null || mktemp -d -t "${SCRIPTNAME}")"
mkmap_modules "${MODULEDIR}" "${ROGER_LIBS}" "${ROGER_USER_LIBS}" "${ROGER_WD}" ||
    error_exit "$(gettext "error setting up the module map")"

## If OUTPUT is undefined or null, or equal to "-", output results to
## the standard output.
${OUTPUT:+false} || [ "${OUTPUT}" = "-" ] && OUTPUT=/dev/stdout

## The tool displays a progress count.
i=0

## Grade every directory in turn in a sub-shell.
for REPOS in "$@"
do
    i=$((i + 1))
    (
	## Print the name of the directory being processed.
	repos="$(resolve_directory "${REPOS}")"
	print_msg >&2 "$(eval_gettext "Grading directory \${repos}") (${i}/$#)... "

	## Change current directory.
	cd -- "${REPOS}" >/dev/null 2>&1 || exit 1

	## Define the standard output.
	exec 1>"${OUTPUT}"
	
	## Grading results header. If both the stdout and stderr
	## output streams are are sent to the terminal, insert an
	## additional new line to display the header below the
	## progress indicator.
	[ -t 1 ] && [ -t 2 ] && printf "\n"
	print_msg "[${msg_directory_id}: ${PWD##*/}]\n"
	print_msg "\n${TITLE}\n"

	## Grading of Git repository, if applicable.
	if [ "${CHECK_REPOS}" = "true" ]
	then
	    ## If not set in the configuration, get the name of the
	    ## main branch.
	    if [ -z "${MAIN}" ]
	    then
		MAIN=$(get_main_name)
	    fi

	    ## Grade the repository; quit in case of errors.
	    grade_repos "${MAIN}" "${DATETIMELIMIT}" "${TAG_NAME}" || exit 1
	fi
	
	## Perform grading operations specified in the grading map;
	## quit in case of errors.
	grade_map "0" "${MAPDIR}" "" \
		  "${MODULEDIR}" \
		  "${CHECK_EXISTS_LADEN}" \
		  "${CHECK_CONTENTS}" \
		  "${CHECK_CONTENTS_MODULE}" \
		  "${CHECK_ENCODING}" \
		  "${POINTS_ENCODING}" || exit 1

	## Print header for comments in the grading file.
	print_msg "\n### ${msg_comments}\n\n"

	## If the directory is a Git repository, return to the main
	## branch (unless option '-d' was used).
	if [ "${CHECK_REPOS}" = "true" ] && [ "${SWITCHTOMAIN:-true}" = "true" ]
	then
	    switch_to_branch "${MAIN}" || exit 1
	fi

	## Confirm all went well.
	print_msg 1>&2 "${msg_ok}\n"
    ) || print_msg 1>&2 "${msg_error}\n"
done

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
