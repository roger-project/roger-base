## Immutable global variables
SCRIPTNAME=$(basename -- "$0")
TOOL="${SCRIPTNAME#*-}"
SCRIPTNAME="${SCRIPTNAME%-*}"
GRADETOOL="grade"
DOC_REQUEST=70

## Default global variables needed from here.
SCRIPTCONF="${GRADETOOL}conf"
RESULTS="${grading_file}"

###
### Documentation
###
if [ "$1" = "-h" ]
then
    ## harvest usage information
    print_msg "$(eval_gettext "Usage: \${SCRIPTNAME} \${TOOL} [options...] [<directory>]
-c <file>, --config-file=<file>
                 Use <file> as configuration file
-f <file>, --grading-file=<file>
                 Harvest results from <file> instead of \${grading_file}
-h               Print this usage information
--help           Print help text")\n"
    exit ${DOC_REQUEST}
fi

if [ "$1" = "--help" ]
then
    ## harvest documentation
    print_msg "$(eval_gettext "NAME

\${SCRIPTNAME} \${TOOL} - collect and summarize grading results

SYNOPSIS

\${SCRIPTNAME} \${TOOL} [-c <file>|--config-file=<file>] 
              [-f <file>|--grading-file=<file>] [-h] [--help]
              [<directory>]

DESCRIPTION

Collect results from grading files in the directories in argument (or,
by default, the working directory), perform subtotals specified in the
configuration and print results to the standard output in CSV format.

-c <file>, --config-file=<file>
       Use <file> as grading configuration file instead of the default
       '\${SCRIPTCONF}'.

-f <file>, --grading-file=<file>
       Extract results from <file> instead of \${grading_file}.

-h
       Print usage information.

--help
       Print this help text.

CONFIGURATION

See '\${SCRIPTNAME} --help-config' for the structure of the configuration file.

This tool uses the boolean key 'subtotal' of the configuration. When
true for a node, the points written for this node is the sum of the
points for all the criteria below it. There can only be one 'subtotal'
key in a branch of the configuration map.

FILES

This tool requires that the configuration file \${SCRIPTCONF}, or the file
specified with option '-c', be present in the current working
directory.")\n"    
    exit ${DOC_REQUEST}
fi

###
### Script actions
###

## Process options.
while [ $# -gt 0 ]
do
    case "$1" in
	-c)
	    SCRIPTCONF="$2"; shift
	    ;;
	--config-file=*)
	    SCRIPTCONF="${1#*=}"
	    ;;
	-f)
	    RESULTS="$2"; shift
	    ;;
	--grading-file=*)
	    RESULTS="${1#*=}"
	    ;;
	*)
	    break;;
    esac
    shift
done

## The directories to harvest are now the remaining arguments; harvest
## the working directory if there are none.
[ $# -eq 0 ] && set -- "${PWD}"

## Expand ~ in ${SCRIPTCONF} if needed.
case "${SCRIPTCONF}" in
    "~/"*) SCRIPTCONF="${HOME}/${SCRIPTCONF#"~/"}";;
esac

## Quit if the configuration file is not found.
[ ! -f "${SCRIPTCONF}" ] &&
    error_exit "$(gettext "configuration file not found")"

## The workhorse is an Awk program saved in a temporary file to
## simplify the call to Awk, below.
script=$(mktemp)
cat > "${script}" <<EOF |
###
### mkmap(conffile, map)
###
##
##  Setup the grading map in a muldimensional array <map> using the
##  information in the configuration file <conffile>. A map of the
##  form
##
##  foo
##     key: value
##  bar
##     key: value
##
##  results in an array with the following key-value pairs:
##
##    root: foo SUBSEP bar
##    root SUBSEP foo SUBSEP key: value
##    root SUBSEP bar SUBSEP key: value
##
##  Awk variant of the shell function of the same name.
function mkmap(conffile,    node, indent, s, ind)
{
    ## Root node name.
    node = "root"

    ## Read the configuration file line by line.
    while ((getline < conffile) > 0)
    {
	## Omit commented and empty lines.
	if (/^[ \t]*#/ || NF == 0)
	    continue

	## Line is not indented by 'indent' spaces: move up one node
	## level and remove one level of expected indentation.
	while (0 == match(\$0, "^" indent "[^ ]"))
	{
	    sub(SUBSEP "[^" SUBSEP "]*$", "", node)
	    indent = substr(indent, 3)
	}

	## Remove indentation from line text.
	s = substr(\$0, RLENGTH)

	## Remove any sorting prefix.
	sub(/^[0-9]+-/, "", s)

	## Determine whether the line contains a key-value pair
	## (indicated by the presence of the character ':' on the
	## line) or a new node.
	ind = index(s, ":")
	if (ind)
	{
	    ## Remove quotes surrounding the value.
	    value = substr(s, ind + 2)
	    gsub(/^"|"$/, "", value)

	    ## Write the value to the array, appending the key text to
	    ## the node name.
	    map[node,substr(s, 1, ind - 1)] = value
	}
	else
	{
	    ## Add a node level to the array, ignoring .PHONY nodes
	    ## (since they are impossible to identify in the results).
	    if (s != ".PHONY")
	    {
		map[node] = (node in map) ? map[node] SUBSEP s : s
		node = node SUBSEP s
	    }

	    ## Add two spaces to the expected indendation.
	    indent = indent "  "
	}
    }
}

###
### findnode(key, line)
###
##
##  Identify the node on <line> among the values of the map array
##  corresponding to <key>, if any.
##
function findnode(key, line,    a, n, i, keydesc)
{
    ## Delete leading hash marks and spaces
    sub(/^#* */, "", line)

    ## Delete end-of-line result
    sub(/ +[0-9.]*\/[0-9.]+$/, "", line)

    n = split(map[key], a, SUBSEP)
    for (i = 1; i <= n; i++)
    {
	keydesc = key SUBSEP a[i] SUBSEP "description"
	if (line == a[i] || (keydesc in map && line == map[keydesc]))
	    return a[i]
    }
    return 0
}

###
### print_points()
###
##
##  Get the result in points on the current line.
##
function print_points(    p)
{
    split(\$NF, p, "/")
    if (accumulate)
	subtotal = subtotal + p[1]
    else
	out = out OFS p[1]
}

###
### print_zeros(key)
###
##
##  Print results of 0 for all criteria below the node <key> in the
##  grading map, including one for the encoding when 'check_encoding'
##  is true in the grading map for the node.
##
function print_zeros(key,    ksub, kenc, kpoints, knext, n, a, i)
{
    ## If the node is a subtotal, write a result of 0 in the output
    ## string and stop processing.
    ksub = key SUBSEP "subtotal"
    if (ksub in map && map[ksub] == "true")
    {
	out = out OFS 0
	return 0
    }

    ## If 'check_encoding' is "true" for the node, write a result of 0 
    ## in the output string.
    kenc = key SUBSEP "check_encoding"
    if (kenc in map && map[kenc] == "true")
	out = out OFS 0

    ## If the node is a criteria with a child 'points', write a result
    ## of 0 in the output string and stop processing.
    kpoints = key SUBSEP "points"
    if (kpoints in map)
    {
	out = out OFS 0
	return 0
    }

    ## For all other cases, repeat the processing for every child of
    ## the current node.
    n = split(map[key], a, SUBSEP)
    for (i = 1; i <= n; i++)
    {
	knext = key SUBSEP a[i]
	print_zeros(knext)
    }
}

BEGIN {
    ## Use ';' as output field separator for CSV format.
    OFS = ";"

    ## Output numbers with at most 2 decimals.
    OFMT = "%.2f"

    ## Setup the grading map.
    mkmap("${SCRIPTCONF}")
}

## Skip empty lines, error messages, warning messages and notes.
NF == 0 || /^(${ROGER_ERRORPROMPT}|${ROGER_WARNINGPROMPT}|${ROGER_NOTEPROMPT})/ { 
    next
}

## A commentary section title on the current line indicates the end of
## the results in a file; skip to next file.
/^### ${msg_comments}/ {
    nextfile
}

## Write results of the previous file when changing file. Reset 'NR'
## to detect further changes of file.
FNR != NR {
    if (accumulate)
	out = out OFS subtotal
    print out
    NR = FNR
}

## The directory identifier indicates the beginning of a file. Reset
## status variables and start a new results line.
/^\[${msg_directory_id}:/ {
    key = "root"
    accumulate = subtotal = level = 0
    out = \$2
    for (i = 3; i <= NF; i++)
	out = out " " \$i
    out = substr(out, 1, length(out) - 1)
    next
}

## Write the number of points for result lines (identified by a result
## at the end of the line).
/\/[0-9]+[.,0-9]*$/ {
    print_points()
    next
}

## Write results of 0 for "abnormal cases": no main branch defined;
## file missing; empty file; invalid source code.
##
## Simple if the current level was already determined to be a
## subtotal; more complicated otherwise as it requires to go through
## all child nodes to find the number of results to write.
##
## Some work may be duplicated with the current approach. However,
## there *should* be few abnormal cases and they may occur for
## different nodes. Preparing in advance the strings of zeros for all
## possible nodes may just prove more work in the end.
/${msg_branch_not_found}|${msg_file_not_found}|${msg_file_empty}|${msg_contents_invalid}/ {
    if (accumulate)
        subtotal = 0
    else
        print_zeros(key)
    next
}

## For all other lines, first determine if the line contains the name
## or description of a node in the grading map.
{
    node = findnode(key, \$0)
}

## No node found: go back up in the grading map, provided level 0 is 
## not already reached.
!node {
    while (!node && level > 0)
    {
	if (accumulate == level)
	{
	    out = out OFS subtotal
	    accumulate = subtotal = 0
	}
	level--
	sub(SUBSEP "[^" SUBSEP "]*$", "", key)
	node = findnode(key, \$0)
    }
}

## Node found: go down one level in the grading map.
node {
    level++
    key = key SUBSEP node
    keysub = key SUBSEP "subtotal"
    if (keysub in map && map[keysub] == "true")
	accumulate = level
}

END {
    if (accumulate)
	out = out OFS subtotal
    print out
}
EOF

## On Windows, the encoding of the script may need to be converted to
## UTF-8. Also delete trailing new lines. Mostly taken from lib/io.sh;
## see there for additional details.
if running_on_windows
then
    encoding=$(file -b --mime-encoding "${script}")
	
    if [ "${encoding}" != "ascii" ] && [ "${encoding}" != "utf-8" ]
    then
	iconv -f "${encoding}" -t "utf-8" "${script}" | tr -d '\r' > "${script}"
    fi
fi

## Harvest and summarize results using the Awk script.
LC_NUMERIC=C find "$@" -name "${RESULTS}" \
     -exec /usr/bin/awk -f "${script}" {} +

exit 0

### Local Variables: ***
### mode: sh ***
### End: ***
