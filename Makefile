### -*-Makefile-*- to prepare and release roger-base
##
## Copyright (C) 2021-2025 Vincent Goulet
##
## 'make update-po' updates the translations.
##
## 'make scripts' builds the scripts.
##
## 'make tests' runs the unit tests.
##
## 'make install' installs the scripts locally.
##
## 'make pkg' builds and notarizes the macOS installer package.
##
## 'make exe' builds the Windows installation wizard.
##
## 'make tar.gz' builds the the .tar.gz package.
##
## 'make release-[pkg|exe|tar.gz]' creates a release in GitLab and assets
## for the packages.
##
## 'make all' is equivalent to 'make install'.
##
## The complete release process is actually as follows:
##
##   make update-copyright
##   make pkg
##   (wait for notarization confirmation)
##   make release-pkg
##   make tar.gz
##   make release-tar.gz
##   (on Windows)
##   make exe
##   make release-exe
##
## Author: Vincent Goulet
##
## This file is part of Roger the Omni Grader
## <https://gitlab.com/roger-project>.

## Files
MAIN = roger
DISTNAME = ${MAIN}-base
TOOLS = $(addprefix ${MAIN}-, \
	checkreq \
	clone \
	grade \
	harvest \
	push \
	switch \
	validate)
MODULES = \
	validate-r-script \
	validate-r-rmarkdown \
	validate-r-shiny \
	validate-r-package \
	validate-shell-script \
	validate-repos-authors \
	validate-repos-commits \
	grade-r-script \
	grade-shell-script \
	grade-repos-authors \
	grade-repos-commits
README = README.md
NEWS = NEWS
LICENSE = COPYING
HEADER = share/header.txt
L10N = share/l10n.sh
L10NMODULES = share/l10n-modules.sh
TRANSLATIONS = fr
TESTSDIR = tests
INNOSCRIPT = ${DISTNAME}.iss	# for update-copyright only

## Build and installation directories
INSTDIR = inst
PREFIX = /usr/local
BINDIR = ${PREFIX}/bin
LIBDIR = ${PREFIX}/lib/${MAIN}
LOCALEDIR = ${PREFIX}/share/locale
DOCDIR = ${PREFIX}/share/doc/${MAIN}
BUILDDIR = builddir

## Version info read from the main script source file. VERSIONFULL
## main contain text other than a version number such as '(in
## development)'.
VERSIONFULL = $(shell awk 'BEGIN { FS = "=" } \
	                    /^VERSION=/ { print $$2; exit }' src/${MAIN}.sh | \
	               tr -d '"')
VERSION = $(word 1, ${VERSIONFULL})
DATE = $(shell awk 'BEGIN { FS="=" } /^DATE=/ { print $$2 }' src/${MAIN}.sh)
YEAR = $(word 1,$(subst -, ,${DATE}))

## Package names
PACKAGE = ${DISTNAME}-${VERSION}
PKG = ${PACKAGE}.pkg
EXE = ${PACKAGE}.exe
TGZ = ${PACKAGE}.tar.gz

## Toolset
CP = cp -p
RM = rm -rf
MD = mkdir -p

## GitLab repository and authentication
REPOSURL = https://gitlab.com/roger-project/roger-base
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/roger-project%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Automatic variables
TAGNAME = v${VERSION}


all: install

## The last two sed commands delete the Emacs file variables at the
## end of the source file and then the resulting trailing white line.
${INSTDIR}${BINDIR}/${MAIN}: src/${MAIN}.sh src/lib/io.sh ${HEADER} ${L10N}
	[ -d ${INSTDIR}${BINDIR} ] && ${RM} ${INSTDIR}${BINDIR}
	${MD} ${INSTDIR}${BINDIR}
	printf > "$@" "%s\n\n" "#!/bin/sh" && \
	  cat >> "$@" ${HEADER} && \
	  echo "" | cat - ${L10N} | \
	    sed -e 's~@main@~${MAIN}~' \
	        -e 's~@localedir@~${LOCALEDIR}~' \
	        >> "$@" && \
	  for f in $(filter src/lib/%,$^); \
	      do echo; cat $$f; \
	  done >> "$@" && \
	  echo "" | cat - $(filter %.sh,$(filter-out share/% src/lib/%,$^)) | \
	    sed '/###.*\*\*\*$$/d' | \
	    sed -e :a -e '/^\n*$$/N;/\n$$/ba' \
	    >> "$@"
	chmod 755 "$@"

$(addprefix ${INSTDIR}${BINDIR}/,${TOOLS}): ${HEADER} ${L10N}
	[ -d ${INSTDIR}${BINDIR} ] || ${MD} ${INSTDIR}${BINDIR}
	printf > "$@" "%s\n\n" "#!/bin/sh" && \
	  cat >> "$@" ${HEADER} && \
	  echo "" | cat - ${L10N} | \
	    sed -e 's~@main@~${MAIN}~' \
	        -e 's~@localedir@~${LOCALEDIR}~' \
	        >> "$@" && \
	  for f in $(filter src/lib/%,$^); \
	      do echo; cat $$f; \
	  done >> "$@" && \
	  echo "" | cat - $(filter %.sh,$(filter-out share/% src/lib/%,$^)) | \
	    sed '/###.*\*\*\*$$/d' | \
	    sed -e :a -e '/^\n*$$/N;/\n$$/ba' \
	    >> "$@"
	chmod 755 "$@"

${INSTDIR}${LIBDIR}/%: src/modules/%.sh $(addprefix src/lib/,$(addsuffix .sh,cmd io modules)) ${HEADER}
	[ -d ${INSTDIR}${LIBDIR} ] || ${MD} ${INSTDIR}${LIBDIR}
	printf > "$@" "%s\n\n" "#!/bin/sh" && \
	  cat >> "$@" ${HEADER} && \
	  echo "" | cat - ${L10NMODULES} | \
	    sed -e 's~@main@~${MAIN}~' \
	        -e 's~@localedir@~${LOCALEDIR}~' \
	        >> "$@" && \
	  for f in $(filter src/lib/%,$^); \
	      do echo; cat $$f; \
	  done >> "$@" && \
	  echo "" | cat - $(filter %.sh,$(filter-out share/% src/lib/%,$^)) | \
	    sed '/###.*\*\*\*$$/d' | \
	    sed -e :a -e '/^\n*$$/N;/\n$$/ba' \
	    >> "$@"
	chmod 755 "$@"

${INSTDIR}${LOCALEDIR}/%/LC_MESSAGES/${MAIN}.mo: po/%.po
	[ -d $(dir $@) ] || ${MD} $(dir $@)
	msgfmt -o $@ $<

${INSTDIR}${BINDIR}/${MAIN}-checkreq: src/checkreq.sh \
	$(addprefix src/lib/,$(addsuffix .sh,cmd io))

${INSTDIR}${BINDIR}/${MAIN}-clone: src/clone.sh \
	$(addprefix src/lib/,$(addsuffix .sh,io restapi))

${INSTDIR}${BINDIR}/${MAIN}-grade: src/grade.sh \
	$(addprefix src/lib/,$(addsuffix .sh,files grading io map repos run_module))

${INSTDIR}${BINDIR}/${MAIN}-harvest: src/harvest.sh \
	$(addprefix src/lib/,$(addsuffix .sh,io))

${INSTDIR}${BINDIR}/${MAIN}-push: src/push.sh \
	$(addprefix src/lib/,$(addsuffix .sh,files io pushing repos))

${INSTDIR}${BINDIR}/${MAIN}-switch: src/switch.sh \
	$(addprefix src/lib/,$(addsuffix .sh,files io repos))

${INSTDIR}${BINDIR}/${MAIN}-validate: src/validate.sh \
	$(addprefix src/lib/,$(addsuffix .sh,files io map repos run_module validation))

po/%.po: po/${MAIN}.pot
	msgmerge --update $@ $<

po/${MAIN}.pot: $(wildcard src/*.sh) $(wildcard src/lib/*.sh) \
	$(wildcard src/modules/*.sh) ${L10N}
	xgettext --add-comments --from-code=UTF-8 -o $@ $^

${PKG}: scripts
	@if [ "$$(echo $${OSTYPE} | sed 's/[0-9.]//g')" != "darwin" ]; \
	then \
	    printf "%s\n" "run 'make pkg' on macOS"; \
	    exit 1; \
	fi
	${MAKE} -C macos

${EXE}: scripts
	@if [ "$${OSTYPE}" != "msys" ]; \
	then \
	    printf "%s\n" "run 'make exe' on Windows"; \
	    exit 1; \
	fi
	${MAKE} -C win

${TGZ}: scripts ${README} ${NEWS} ${LICENSE}
	@echo "----- Building the archive... "
	if [ -d ${BUILDDIR} ]; then ${RM} ${BUILDDIR}; fi
	${MD} $(addprefix ${BUILDDIR},${BINDIR} ${LIBDIR} ${LOCALEDIR} ${DOCDIR})
	tar -C ${INSTDIR} -cf - . | tar -C ${BUILDDIR} -xmf -
	pandoc -o ${BUILDDIR}${DOCDIR}/${README:.md=.html} ${README}
	${CP} ${NEWS} ${LICENSE} ${BUILDDIR}${DOCDIR}
	cd ${BUILDDIR} && tar -czf ../${TGZ} *
	${RM} ${BUILDDIR}

.PHONY: update-po
update-po: $(addprefix po/,$(addsuffix .po,${TRANSLATIONS}))

.PHONY: scripts
scripts: ${INSTDIR}${BINDIR}/${MAIN} \
	$(addprefix ${INSTDIR}${BINDIR}/,${TOOLS}) \
	$(addprefix ${INSTDIR}${LIBDIR}/,${MODULES}) \
	$(addprefix ${INSTDIR}${LOCALEDIR}/,$(addsuffix /LC_MESSAGES/${MAIN}.mo,${TRANSLATIONS}))

.PHONY: tests
tests: scripts
	@for f in $(wildcard ${TESTSDIR}/*.sh); \
	     do ./$$f; \
	done

.PHONY: update-copyright
update-copyright: ${HEADER} win/${INNOSCRIPT:.iss=.in} \
	Makefile macos/Makefile win/Makefile
	for f in $?; \
	    do sed -E '/^(#|;)* +Copyright \(C\)/s/-20[0-9]{2}/-${YEAR}/' \
	           $$f > $$f.tmp && \
	           ${CP} $$f.tmp $$f && \
	           ${RM} $$f.tmp; \
	done

.PHONY: install
install: scripts
	tar -C ${INSTDIR} -cf - . | tar -C / -xmf - 

.PHONY: install-msys
install-msys: PREFIX=/c/msys64${PREFIX}
install-msys: install

.PHONY: pkg
pkg: ${PKG}

.PHONY: exe
exe: ${EXE}

.PHONY: tar.gz
tar.gz: ${TGZ}

.PHONY: release-pkg release-exe release-tar.gz
release-pkg: staple check-status create-release upload-pkg create-link-pkg
release-exe: check-status create-release upload-exe create-link-exe
release-tar.gz: check-status create-release upload-tar.gz create-link-tar.gz

.PHONY: staple
staple:
	${MAKE} -C macos staple

.PHONY: check-status
check-status:
	@{ \
	    printf "%s" "checking status of local repository... "; \
	    branch=$$(git branch --list | grep ^* | cut -d " " -f 2-); \
	    if [ "$${branch}" != "master"  ] && [ "$${branch}" != "main" ]; \
	    then \
	        printf "\n%s\n" "! not on branch master or main"; exit 2; \
	    fi; \
	    if [ -n "$$(git status --porcelain | grep -v '^??')" ]; \
	    then \
	        printf "\n%s\n" "! uncommitted changes in repository"; exit 2; \
	    fi; \
	    if [ -n "$$(git log origin/master..HEAD | head -n1)" ]; \
	    then \
	        printf "\n%s\n" "unpushed commits in repository; pushing to origin"; \
	        git push; \
	    else \
	        printf "%s\n" "done"; \
	    fi; \
	}

.PHONY: create-release
create-release:
	@{ \
	    printf "%s" "checking if a release already exists... "; \
	    http_code=$$(curl -I ${APIURL}/releases/${TAGNAME} 2>/dev/null \
	                     | head -n1 | cut -d " " -f2) ; \
	    if [ "$${http_code}" = "200" ]; \
	    then \
	        printf "%s\n" "yes"; \
	        printf "%s\n" "-> using the existing release"; \
	    else \
	        printf "%s\n" "no"; \
	        printf "%s" "creating release on GitLab... "; \
	        name=$$(awk '/^# / { sub(/# +/, "", $$0); print $$0; exit }' ${NEWS}); \
	        desc=$$(awk ' \
	                      /^$$/ { next } \
	                      (state == 0) && /^# / { state = 1; next } \
	                      (state == 1) && /^# / { exit } \
	                      (state == 1) { print } \
	                    ' ${NEWS}); \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --output /dev/null --silent \
	             "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master" && \
	        curl --request POST \
	             --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	             --data tag_name="${TAGNAME}" \
	             --data name="$${name}" \
	             --data description="$${desc}" \
	             --output /dev/null --silent \
	             ${APIURL}/releases; \
	        printf "%s\n" "done"; \
	    fi; \
	}

.PHONY: upload-%
upload-%:
	@printf "%s %s %s\n" "uploading" "${PACKAGE}.$*" "to the package registry..."
	curl --upload-file "${PACKAGE}.$*" \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --silent \
	     "${APIURL}/packages/generic/${REPOSNAME}/${VERSION}/${PACKAGE}.$*"
	@printf "\n%s\n" "done"

.PHONY: create-link-%
create-link-%: create-release
	@printf "%s" "adding package link to the release... "; \
	$(eval pkg_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                           --silent \
	                           "${APIURL}/packages" \
	                      | grep -o -E '\{[^{]*"version":"${VERSION}"[^}]*}' \
	                      | grep -o '"id":[0-9]*' | cut -d: -f 2))
	$(eval file_id=$(shell curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	                            --silent \
	                            "${APIURL}/packages/${pkg_id}/package_files" \
	                       | grep -o -E '\{[^{]*"file_name":"${PACKAGE}.$*"[^}]*}' \
	                       | grep -o '"id":[0-9]*' | cut -d: -f 2))
	curl --request POST \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --data name="${PACKAGE}.$*" \
	     --data url="${REPOSURL:/=}/-/package_files/${file_id}/download" \
	     --data link_type="package" \
	     --output /dev/null --silent \
	     "${APIURL}/releases/${TAGNAME}/assets/links"
	@printf "%s\n" "done"

clean:
	${RM} ${MAIN} ${TOOLS}
	${MAKE} -C macos clean
	${MAKE} -C win clean
