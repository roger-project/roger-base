# Roger the Omni Grader

Roger is an automated grading system for R scripts... and much more.
The system is based on a set of Unix shell scripts to validate and
grade student projects, and an R package to check the coding style and
documentation of R scripts.

Roger was developed with the [Computer Science Program Grading
Criteria](https://wiki.cs.astate.edu/index.php?title=Computer_Science_Program_Grading_Criteria&oldid=10507)
of Arkansas State University in mind, but the system is very flexible
and it can easily be adapted to other contexts. As long as you have
grading criteria and a number of points attached, you should be good
to go. You do have grading criteria, right?

Roger is actively used for grading at 
[École d'actuariat](https://act.ulaval.ca) of 
[Université Laval](https://ulaval.ca).

## Base system

This is `roger-base`, the repository of the shell scripts that form
the foundations of the system.

`roger` is the unified interface to the tools below.

`roger checkreq` checks that all required software (command line
utilities, R packages) is installed on a grader's workstation prior to
grading.

`roger clone` fetches the projects to grade hosted in Git repositories.

`roger grade` is the true workhorse that grades the projects for the
criteria that can be handled automatically, mostly correctness of
results (with the help of unit tests), coding style and the presence
of documentation (assessing its contents and quality is left to
humans).

`roger harvest` collects and summarizes grading results and returns the 
data in CSV format.

`roger push` publishes grading results in Git repositories.

`roger switch` switches branches in Git repositories.

`roger validate` allows students to validate their work (files and
state of their Git repository) prior to handing it in.

## Authors

David Beauchemin was the first to write a few scripts to accelerate
grading of 100+ projects many times per semester. Jean-Christophe
Langlois wrote the first version of the R package. Samuel Fréchette
wrote the R interface to the shell scripts and unit tests. Vincent
Goulet wrote the shell scripts and is the maintainer of the project.

## Installing

macOS, Windows and plain ZIP installers for `roger-base` are available
from the [Roger the Omni Grader web site](https://roger-project.gitlab.io).

You may also install the system from the Git repository:

```
git clone https://gitlab.com/roger-project/roger-base.git
cd roger-base
make install
```

The system installs into `/usr/local/`. 

**Note for Windows users**: Roger requires a Unix shell for Windows
such as MSYS2 or Git Bash. From the shell point of view, the root of
the filesystem is the installation directory of the shell (e.g.
`C:\msys2`).
