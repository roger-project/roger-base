#!/bin/sh

## Get the absolute path of this script
SCRIPTPATH="$(cd -- "$(dirname "$0")" >/dev/null 2>&1; pwd -P)"

## Put build directory ahead of system path
PATH=${SCRIPTPATH%/*}/inst/usr/local/bin:${PATH}

## Use local translations
LOCALTEXTDOMAINDIR=${SCRIPTPATH%/*}/inst/usr/local/share/locale

## Define utility functions
. "${SCRIPTPATH}/lib/io.sh"
. "${SCRIPTPATH}/lib/repos.sh"

## Temporary directory to hold files
TESTSDIR="$(mktemp -d 2>/dev/null || mktemp -d -t roger-tests)"
# TESTSDIR=testsdir
# [ -d $TESTSDIR ] && rm -rf $TESTSDIR
# mkdir $TESTSDIR

## Names of test repositories
REPOS="1_roger_test 2_roger_test"

print_msg "Setting up push tests... "

run_test ()
{
    TEXTDOMAINDIR=$LOCALTEXTDOMAINDIR roger push "$@"
}

print_msg "setting up test repositories... "

## Create test repositories with one commit in each
for repos in ${REPOS}
do
    if list_repos | grep -q "${repos}"
    then
	delete_repos "${repos}"
    fi
    create_repos "${repos}"
done

## Clone test repositories (directly using 'git clone', as this is not
## a test for 'roger clone'), and create files in each
for repos in ${REPOS}
do
    git clone -q ${PROJECTURL}/${repos}.git ${TESTSDIR}/${repos}
    printf "### %s\n" $(git -C ${TESTSDIR}/${repos} show -s --format="%h") > ${TESTSDIR}/${repos}/sha
done

print_msg "running tests... "

## Test with default file and message (English locale)
for repos in ${REPOS}
do
    (
	cd ${TESTSDIR}/${repos} 1>/dev/null 2>&1 
	git switch -q master
	cp sha GRADING.txt
    )
done
(cd ${TESTSDIR} >/dev/null 2>&1 && LANGUAGE=en run_test -q -c test-1 ${REPOS}) && 
    {
	for repos in ${REPOS}
	do
	    git -C ${TESTSDIR}/${repos} show-ref -q "test-1" &&
		{ git -C ${TESTSDIR}/${repos} ls-tree --name-only "test-1" | grep -q -F "GRADING.txt"; }
	done
    } ||
	error_exit "default file and message"

## Test with default file, one specific message (French locale)
for repos in ${REPOS}
do
    (
	cd ${TESTSDIR}/${repos} 1>/dev/null 2>&1 
	git switch -q master
	cp sha CORRECTION.txt
    )
done
LANGUAGE=fr run_test -q -m "message spécifique" -c test-2 ${TESTSDIR}/*/ && 
    {
	for repos in ${REPOS}
	do
	    git -C ${TESTSDIR}/${repos} show-ref -q "test-2" &&
		{ git -C ${TESTSDIR}/${repos} ls-tree --name-only "test-2" | grep -q -F "CORRECTION.txt"; }
	done
    } ||
	error_exit "default file, one specific message"

## Test with default file, multiple messages (default locale)
for repos in ${REPOS}
do
    (
	cd ${TESTSDIR}/${repos} 1>/dev/null 2>&1 
	git switch -q master
	cp sha GRADING.txt
	cp sha CORRECTION.txt
    )
done
run_test -q -m "message line 1" --message="message line 2" -c test-3 ${TESTSDIR}/*/ && 
    {
	for repos in ${REPOS}
	do
	    git -C ${TESTSDIR}/${repos} show-ref -q "test-3" &&
		{ git -C ${TESTSDIR}/${repos} ls-tree --name-only "test-3" | grep -q -E "(GRADING|CORRECTION)\.txt"; }
	done
    } ||
	error_exit "default file, multiple messages"

## Test with alternate results file, default message
for repos in ${REPOS}
do
    (
	cd ${TESTSDIR}/${repos} 1>/dev/null 2>&1 
	git switch -q master
	cp sha RESULTS.txt
    )
done
(cd ${TESTSDIR} >/dev/null 2>&1 && run_test -q -f RESULTS.txt -c test-4 ${REPOS}) && 
    {
	for repos in ${REPOS}
	do
	    git -C ${TESTSDIR}/${repos} show-ref -q "test-4" &&
		{ git -C ${TESTSDIR}/${repos} ls-tree --name-only "test-4" | grep -q -F "RESULTS.txt"; }
	done
    } ||
	error_exit "alternate results file, default message"

## Test with one additional file, default message (English locale)
for repos in ${REPOS}
do
    (
	cd ${TESTSDIR}/${repos} 1>/dev/null 2>&1 
	git switch -q master
	cp sha GRADING.txt
	touch foo.txt
    )
done
LANGUAGE=en run_test -q -a foo.txt -c test-5 ${TESTSDIR}/*/ && 
    {
	for repos in ${REPOS}
	do
	    git -C ${TESTSDIR}/${repos} show-ref -q "test-5" &&
		{ git -C ${TESTSDIR}/${repos} ls-tree --name-only "test-5" | grep -q -F -e "GRADING.txt" -e "foo.txt"; }
	done
    } ||
	error_exit "additional file, default message"

## Test with two additional files (one with a space in the name),
## default message (French locale)
for repos in ${REPOS}
do
    (
	cd ${TESTSDIR}/${repos} 1>/dev/null 2>&1 
	git switch -q master
	cp sha CORRECTION.txt
	touch foo.txt bar\ baz.txt
    )
done
LANGUAGE=fr run_test -q -a foo.txt -a "bar baz.txt" -c test-6 ${TESTSDIR}/*/ && 
    {
	for repos in ${REPOS}
	do
	    git -C ${TESTSDIR}/${repos} show-ref -q "test-6" &&
		{ git -C ${TESTSDIR}/${repos} ls-tree --name-only "test-6" | grep -q -F -e "CORRECTION.txt" -e "foo.txt" -e "bar baz.txt"; }
	done
    } ||
	error_exit "two additional files, default message"

## Test with alternate file in branch created from HEAD, default
## message (French locale)
for repos in ${REPOS}
do
    (
	cd ${TESTSDIR}/${repos} 1>/dev/null 2>&1 
	git switch -q master
	touch foo.txt
    )
done
LANGUAGE=fr run_test -q -f foo.txt --from-head test-7 ${TESTSDIR}/*/ && 
    {
	for repos in ${REPOS}
	do
	    git -C ${TESTSDIR}/${repos} show-ref -q "test-7" &&
		{ git -C ${TESTSDIR}/${repos} ls-tree --name-only "test-7" | grep -q -F "foo.txt"; }
	done
    } ||
	error_exit "alternate file, from HEAD, default message"

## Test with carriage returns (\r) between the commit SHA and newline
## in the result files (it happened)
for repos in ${REPOS}
do
    (
	cd ${TESTSDIR}/${repos} 1>/dev/null 2>&1 
	git switch -q master
	printf "### %s\r\n" $(git show -s --format="%h") > GRADING.txt
    )
done
(cd ${TESTSDIR} >/dev/null 2>&1 && run_test -q -f GRADING.txt -c test-8 ${REPOS})  ||
	error_exit "carriage return in results file"

## Delete the test repositories
for repos in ${REPOS}; do delete_repos "${repos}"; done

print_msg "ok\n"

exit 0
