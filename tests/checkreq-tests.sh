#!/bin/sh

## Get the absolute path of this script
SCRIPTPATH="$(cd -- "$(dirname "$0")" >/dev/null 2>&1; pwd -P)"

## Put build directory ahead of system path
PATH=${SCRIPTPATH%/*}/inst/usr/local/bin:${PATH}

## Use local translations
LOCALTEXTDOMAINDIR=${SCRIPTPATH%/*}/inst/usr/local/share/locale

## Define utility functions
. "${SCRIPTPATH}/lib/io.sh"

## Temporary directory to hold files
TESTSDIR="$(mktemp -d 2>/dev/null || mktemp -d -t roger-tests)"
# TESTSDIR=testsdir
# [ -d $TESTSDIR ] && rm -rf $TESTSDIR
# mkdir $TESTSDIR

print_msg "Setting up checkreq tests... "

## Sample requirements file
cat > ${TESTSDIR}/requirements.txt <<EOF
roger
R
rpackage:roger
rpackage:MASS
EOF

run_test ()
{
    TEXTDOMAINDIR=$LOCALTEXTDOMAINDIR roger checkreq "$@"
}

print_msg "running tests (assuming availability of R and MASS)... "

## Test with default configuration file
(cd ${TESTSDIR} >/dev/null 2>&1 && run_test >/dev/null 2>&1) ||
    error_exit "default configuration"

## Test with specific configuration file
run_test -f ${TESTSDIR}/requirements.txt >/dev/null 2>&1 ||
    error_exit "specific configuration"

## Test with wrong configuration file
! run_test -f ${TESTSDIR}/__PHONY__ >/dev/null 2>&1 ||
    error_exit "wrong configuration"

## Tests for output in English and French locales
(cd ${TESTSDIR} >/dev/null 2>&1 && LANGUAGE=en run_test) | \
    grep -q -e "availability of command" -e "availability of package" ||
    error_exit "output in English locale"
LANGUAGE=fr run_test -f ${TESTSDIR}/requirements.txt | \
    grep -q -e "disponibilité de la commande" -e "disponibilité de la commande" ||
    error_exit "output in French locale"

print_msg "ok\n"

exit 0
