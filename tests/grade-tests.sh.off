#!/bin/sh

## Get the absolute path of this script
SCRIPTPATH="$(cd -- "$(dirname "$0")" >/dev/null 2>&1; pwd -P)"

## Put build directory ahead of system path
PATH=${SCRIPTPATH%/*}/inst/usr/local/bin:${PATH}

## Use local translations
LOCALTEXTDOMAINDIR=${SCRIPTPATH%/*}/inst/usr/local/share/locale

## Define utility functions
. "${SCRIPTPATH}/lib/io.sh"
. "${SCRIPTPATH}/lib/repos.sh"



    # 2-results
    #   description: Code outputs expected results
    #   tests: test-results.R
    #   points: 2
# https://projets.fsg.ulaval.ca/git/scm/vg/111555555_roger_test.git
# https://projets.fsg.ulaval.ca/git/scm/vg/536555555_roger_test.git

## Temporary directory to hold files
TESTSDIR="$(mktemp -d 2>/dev/null || mktemp -d -t roger-tests)"

## Sample requirements file
cat > ${TESTSDIR}/gradeconf <<EOF
__global
  title: "TEST GRADING\nBasic configuration"
  repository
    check_repos: true
    tag
      checkout_by_tag: true
      tag_name: gradetest
  encoding
    check_encoding: true
    points: 1

main
  01-valid_code.R
    code: true
    Valid Code
      sourceable
        description: "File evaluates in R"
        points: 2

  02-nonvalid_code.R
    code: true
    Non Valid Code
      sourceable
        description: "File evaluates in R"
        points: 2

  03-data.csv
    Sample test data
      results
        description: "Data file format"
        points: 2
EOF

## Expected results
cat > ${TESTSDIR}/results-en.txt <<EOF
[Directory: 111555555_roger_test]

TEST GRADING
Basic configuration

### Checking out repository tag gradetest
Grading based on commit ab95ea2

### Grading of the repository
Valid repository

### master

## valid_code.R
Character encoding                                     1/1
Valid Code
  File evaluates in R                                  /2

## nonvalid_code.R
ERROR: source code cannot be evaluated
  ! Error in 2 + "2" : non-numeric argument to binary operator
  ! Calls: source -> withVisible -> eval -> eval
  ! Execution halted

## data.csv
Character encoding                                     1/1
Sample test data
  Data file format                                     /2

### Comments

EOF

cat > ${TESTSDIR}/results-fr.txt <<EOF
[Répertoire: 111555555_roger_test]

TEST GRADING
Basic configuration

### Extraction de l'étiquette gradetest
Correction effectuée à partir de l'archive ab95ea2

### Correction du dépôt
Dépôt valide

### master

## valid_code.R
Codage de caractères                                   1/1
Valid Code
  File evaluates in R                                  /2

## nonvalid_code.R
ERREUR: évaluation du code source impossible
  ! Erreur dans 2 + "2" : argument non numérique pour un opérateur binaire
  ! Appels : source -> withVisible -> eval -> eval
  ! Exécution arrêtée

## data.csv
Codage de caractères                                   1/1
Sample test data
  Data file format                                     /2

### Commentaires

EOF

run_test ()
{
    TEXTDOMAINDIR=$LOCALTEXTDOMAINDIR roger grade "$@"
}

print_msg "Running grade tests... "

## Clone repo on which to test
URLREPO="https://projets.fsg.ulaval.ca/git/scm/vg/111555555_roger_test.git"
REPO=$(echo $URLREPO | sed -e 's|.*/||;s|\.git||')

git clone -q $URLREPO ${TESTSDIR}/${REPO} ||
  error_exit "clone test repo"

## Test with default configuration file
(cd ${TESTSDIR} && run_test ${REPO} >/dev/null 2>&1) ||
    error_exit "default configuration"

## Test with specific configuration file
run_test -c ${TESTSDIR}/gradeconf ${TESTSDIR}/${REPO} >/dev/null 2>&1 ||
    error_exit "specific configuration"

## Tests for output in English and French locales
LANGUAGE=en run_test -c ${TESTSDIR}/gradeconf ${TESTSDIR}/${REPO} | \
    diff -q "${TESTSDIR}/results-en.txt" - >/dev/null 2>&1 ||
    error_exit "output in English locale"
LANGUAGE=fr run_test -c ${TESTSDIR}/gradeconf ${TESTSDIR}/${REPO} | \
    diff -q "${TESTSDIR}/results-fr.txt" - >/dev/null 2>&1 ||
    error_exit "output in French locale"

print_msg "ok\n"

exit 0
