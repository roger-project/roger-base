#!/bin/sh

## Get the absolute path of this script
SCRIPTPATH="$(cd -- "$(dirname "$0")" >/dev/null 2>&1; pwd -P)"

## Put build directory ahead of system path
PATH=${SCRIPTPATH%/*}/inst/usr/local/bin:${PATH}

## Use local translations
LOCALTEXTDOMAINDIR=${SCRIPTPATH%/*}/inst/usr/local/share/locale

## Define utility functions
. "${SCRIPTPATH}/lib/io.sh"
. "${SCRIPTPATH}/lib/repos.sh"

## Temporary directory to hold files
TESTSDIR="$(mktemp -d 2>/dev/null || mktemp -d -t roger-tests)"
# TESTSDIR=testsdir
# [ -d $TESTSDIR ] && rm -rf $TESTSDIR
# mkdir $TESTSDIR

## Names of test repositories: one with valid files, the other with
## errors
REPOS_OK="1_roger_test"
REPOS_ERROR="2_roger_test"

print_msg "Setting up validate tests... "

run_test ()
{
    TEXTDOMAINDIR=$LOCALTEXTDOMAINDIR roger validate "$@"
}

print_msg "running tests... "

## Test that files exist and are laden only
[ -d ${TESTSDIR}/${REPOS_OK} ] || mkdir -p ${TESTSDIR}/${REPOS_OK}/bar
[ -d ${TESTSDIR}/${REPOS_ERROR} ] || mkdir ${TESTSDIR}/${REPOS_ERROR}

cat > ${TESTSDIR}/validateconf <<EOF
__global
  defaults
    check_exists_laden: true
1-hello.txt
2-foo.R
3-bar
  recursive: true
  bar.sh
EOF

cat <<EOF > ${TESTSDIR}/${REPOS_OK}/hello.txt
Hello, World!
EOF
cat <<EOF > ${TESTSDIR}/${REPOS_OK}/foo.R
x <- 2 + 3
EOF
cat <<EOF > ${TESTSDIR}/${REPOS_OK}/bar/bar.sh
echo "Hello, World!"
EOF

touch ${TESTSDIR}/${REPOS_ERROR}/foo.R

(cd ${TESTSDIR} >/dev/null 2>&1 && run_test ${REPOS_OK} > ${REPOS_OK}/VALIDATION &&
     { grep '^[^#].*hello\.txt' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; } &&
     { grep '^[^#].*foo' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; }) &&
     { grep '^[^#].*bar[^.]' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; }) &&
     { grep '^[^#].*bar\.sh' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; }) ||
    error_exit "files exist and are laden only (${REPOS_OK})"

run_test -c ${TESTSDIR}/validateconf ${TESTSDIR}/${REPOS_ERROR} > ${TESTSDIR}/${REPOS_ERROR}/VALIDATION &&
    grep -q -E '^[^#].*hello.*(ERROR|ERREUR)$' ${TESTSDIR}/${REPOS_ERROR}/VALIDATION &&
    grep -q -E '^[^#].*foo.*(ERROR|ERREUR)$' ${TESTSDIR}/${REPOS_ERROR}/VALIDATION ||
	error_exit "files exist and are laden only (${REPOS_ERROR})"
## end test

## Test for the encoding only
[ -d ${TESTSDIR}/${REPOS_OK} ] || mkdir ${TESTSDIR}/${REPOS_OK}
[ -d ${TESTSDIR}/${REPOS_ERROR} ] || mkdir ${TESTSDIR}/${REPOS_ERROR}

cat > ${TESTSDIR}/validateconf <<EOF
__global

1-hello.txt
  check_encoding: true
2-foo.R
  check_encoding: true
EOF

cat <<EOF > ${TESTSDIR}/${REPOS_OK}/hello.txt
Hello, World!
EOF
cat <<EOF | iconv -t UTF-8 > ${TESTSDIR}/${REPOS_OK}/foo.R
## gave-moé de ton amour
x <- 2 + 3
EOF

cat <<EOF | iconv -t ISO-8859-1 > ${TESTSDIR}/${REPOS_ERROR}/hello.txt
Bonjour! Ça va?
EOF
cat <<EOF | iconv -t ISO-8859-15 > ${TESTSDIR}/${REPOS_ERROR}/foo.R
## gave-moé de ton amour
x <- 2 + 3
EOF

(cd ${TESTSDIR} >/dev/null 2>&1 && run_test ${REPOS_OK} > ${REPOS_OK}/VALIDATION &&
     { grep '^[^#].*hello' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; } &&
     { grep '^[^#].*foo' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; }) ||
    error_exit "encoding only (${REPOS_OK})"

run_test -c ${TESTSDIR}/validateconf ${TESTSDIR}/${REPOS_ERROR} > ${TESTSDIR}/${REPOS_ERROR}/VALIDATION &&
    { grep -q -E '^[^#].*hello.*(WARNING|AVERTISSEMENT)$' ${TESTSDIR}/${REPOS_ERROR}/VALIDATION; } &&
    { grep -q -E '^[^#].*foo.*(WARNING|AVERTISSEMENT)$' ${TESTSDIR}/${REPOS_ERROR}/VALIDATION; } ||
	error_exit "encoding only (${REPOS_ERROR})"
## end test

## Test for an R script
[ -d ${TESTSDIR}/${REPOS_OK} ] || mkdir ${TESTSDIR}/${REPOS_OK}
[ -d ${TESTSDIR}/${REPOS_ERROR} ] || mkdir${TESTSDIR}/${REPOS_ERROR}

cat > ${TESTSDIR}/validateconf <<EOF
__global

1-foo.R
  check_contents: true
EOF

cat <<EOF > ${TESTSDIR}/${REPOS_OK}/foo.R
x <- 2 + 3
EOF

cat <<EOF > ${TESTSDIR}/${REPOS_ERROR}/foo.R
x <- 2 + a
EOF

(cd ${TESTSDIR} >/dev/null 2>&1 && run_test ${REPOS_OK} > ${REPOS_OK}/VALIDATION &&
     { grep '^[^#].*foo' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; }) ||
    error_exit "R script (${REPOS_OK})"

run_test -c ${TESTSDIR}/validateconf ${TESTSDIR}/${REPOS_ERROR} > ${TESTSDIR}/${REPOS_ERROR}/VALIDATION &&
    { grep '^[^#].*foo' ${TESTSDIR}/${REPOS_ERROR}/VALIDATION | grep -q -E -v '(ERROR|ERREUR)$' || true; } ||
	error_exit "R script (${REPOS_ERROR})"
## end test

## Test for an R script using a local validation module
[ -d ${TESTSDIR}/${REPOS_OK} ] || mkdir ${TESTSDIR}/${REPOS_OK}

cat > ${TESTSDIR}/validateconf <<EOF
__global

1-foo.R
  check_contents: true
  check_contents_module: module-error.sh
2-bar.R
  check_contents: true
  check_contents_module: module-warning.sh
3-baz.R
  check_contents: true
  check_contents_module: module-note.sh
EOF

cat > ${TESTSDIR}/module-error.sh <<"EOF"
#!/bin/sh
printf "%s" "using local module... "
printf "%s\n" "ERROR"
printf 1>&2 "%s\n" "error message"
exit 71
EOF
chmod u+x ${TESTSDIR}/module-error.sh

cat > ${TESTSDIR}/module-warning.sh <<"EOF"
#!/bin/sh
printf "%s" "using local module... "
printf "%s\n" "WARNING"
printf 1>&2 "%s\n" "warning message"
exit 72
EOF
chmod u+x ${TESTSDIR}/module-warning.sh

cat > ${TESTSDIR}/module-note.sh <<"EOF"
#!/bin/sh
printf "%s" "using local module... "
printf "%s\n" "NOTE"
printf 1>&2 "%s\n" "note message"
exit 73
EOF
chmod u+x ${TESTSDIR}/module-note.sh

touch ${TESTSDIR}/${REPOS_OK}/foo.R
touch ${TESTSDIR}/${REPOS_OK}/bar.R
touch ${TESTSDIR}/${REPOS_OK}/baz.R

(cd ${TESTSDIR} >/dev/null 2>&1 && run_test ${REPOS_OK} > ${REPOS_OK}/VALIDATION &&
     { grep '^[^#].*foo' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; }) ||
    error_exit "R script using a local module (${REPOS_OK})"
## end test

## Test for an R Markdown document (PDF)
[ -d ${TESTSDIR}/${REPOS_OK} ] || mkdir ${TESTSDIR}/${REPOS_OK}
[ -d ${TESTSDIR}/${REPOS_ERROR} ] || mkdir${TESTSDIR}/${REPOS_ERROR}

cat > ${TESTSDIR}/validateconf <<EOF
__global

1-foo.Rmd
  check_contents: true
EOF

cat <<"EOF" > ${TESTSDIR}/${REPOS_OK}/foo.Rmd
---
title: "R Markdown sample document"
author: "Vincent Goulet"
date: ""
output: pdf_document
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris
fermentum egestas neque malesuada tristique. Integer vulputate nibh eu
molestie aliquam. `r 2 + 3`

```r
2 + 3
```

Curabitur eget placerat purus $\int_0^1 sin(x)\, dx$.
$$
x = \frac{1}{2}
$$
EOF

cp ${TESTSDIR}/${REPOS_OK}/foo.Rmd ${TESTSDIR}/${REPOS_ERROR}/foo.Rmd
cat <<"EOF" >> ${TESTSDIR}/${REPOS_ERROR}/foo.Rmd
`r 2 + a`
EOF

(cd ${TESTSDIR} >/dev/null 2>&1 && run_test ${REPOS_OK} > ${REPOS_OK}/VALIDATION &&
     { grep '^[^#].*foo' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; }) ||
    error_exit "R Markdown document (PDF) (${REPOS_OK})"

run_test -c ${TESTSDIR}/validateconf ${TESTSDIR}/${REPOS_ERROR} > ${TESTSDIR}/${REPOS_ERROR}/VALIDATION &&
    { grep '^[^#].*foo' ${TESTSDIR}/${REPOS_ERROR}/VALIDATION | grep -q -E -v '(ERROR|ERREUR)$' || true; } ||
	error_exit "R Markdown document (PDF) (${REPOS_ERROR})"
## end test

## Test for an R Markdown document (HTML)
[ -d ${TESTSDIR}/${REPOS_OK} ] || mkdir ${TESTSDIR}/${REPOS_OK}
[ -d ${TESTSDIR}/${REPOS_ERROR} ] || mkdir${TESTSDIR}/${REPOS_ERROR}

cat > ${TESTSDIR}/validateconf <<EOF
__global

1-bar.Rmd
  check_contents: true
EOF

cat <<"EOF" > ${TESTSDIR}/${REPOS_OK}/bar.Rmd
---
title: "R Markdown sample document"
author: "Vincent Goulet"
date: ""
output: html_document
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris
fermentum egestas neque malesuada tristique. Integer vulputate nibh eu
molestie aliquam. `r 2 + 3`

```r
2 + 3
```

Curabitur eget placerat purus $\int_0^1 sin(x)\, dx$.
$$
x = \frac{1}{2}
$$
EOF

cp ${TESTSDIR}/${REPOS_OK}/foo.Rmd ${TESTSDIR}/${REPOS_ERROR}/bar.Rmd
cat <<"EOF" >> ${TESTSDIR}/${REPOS_ERROR}/bar.Rmd
$\invalidcommand$
EOF

(cd ${TESTSDIR} >/dev/null 2>&1 && run_test ${REPOS_OK} > ${REPOS_OK}/VALIDATION &&
     { grep '^[^#].*foo' ${REPOS_OK}/VALIDATION | grep -q -v 'ok$' || true; }) ||
    error_exit "R Markdown document (HTML) (${REPOS_OK})"

run_test -c ${TESTSDIR}/validateconf ${TESTSDIR}/${REPOS_ERROR} > ${TESTSDIR}/${REPOS_ERROR}/VALIDATION &&
    { grep '^[^#].*foo' ${TESTSDIR}/${REPOS_ERROR}/VALIDATION | grep -q -E -v '(ERROR|ERREUR)$' || true; } ||
	error_exit "R Markdown document (HTML) (${REPOS_ERROR})"
## end test

print_msg "ok\n"

exit 0
