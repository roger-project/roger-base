#!/bin/sh

## Get the absolute path of this script
SCRIPTPATH="$(cd -- "$(dirname "$0")" >/dev/null 2>&1; pwd -P)"

## Put build directory ahead of system path
PATH=${SCRIPTPATH%/*}/inst/usr/local/bin:${PATH}

## Temporary directory to hold files
TESTSDIR="$(mktemp -d 2>/dev/null || mktemp -d -t roger-tests)"
# TESTSDIR=testsdir
# [ -d $TESTSDIR ] && rm -rf $TESTSDIR
# mkdir $TESTSDIR

## Define utility functions
. "${SCRIPTPATH}/lib/io.sh"
. "${SCRIPTPATH}/lib/repos.sh"

## Names of test repositories
REPOS="1_roger_test 2_roger_test"

print_msg "Setting up switch tests... "

run_test ()
{
    roger switch "$@"
}

print_msg "setting up test repositories... "

## Create test repositories
for repos in ${REPOS}
do
    if list_repos | grep -q "${repos}"
    then
	delete_repos "${repos}"
    fi
    create_repos "${repos}"
    create_branch "${repos}" "foobar" "master"
done

## Clone test repositories (directly using 'git clone', as this is not
## a test for 'roger clone')
for repos in ${REPOS}
do
    git clone -q ${PROJECTURL}/${repos}.git ${TESTSDIR}/${repos}
done

print_msg "running tests... "

## Test switching to branch 'foobar'
(cd ${TESTSDIR} >/dev/null 2>&1 && run_test -q foobar ${REPOS}) &&
    {
	for repos in ${REPOS}
	do
	    test "$(git -C ${TESTSDIR}/${repos} branch --show-current)" = "foobar"
	done
    } ||
	error_exit "switching to foobar"

## Test switching to 'master'
run_test -q master ${TESTSDIR}/*/ &&
    {
	for repos in ${REPOS}
	do
	    test "$(git -C ${TESTSDIR}/${repos} branch --show-current)" = "master"
	done
    } ||
	error_exit "switching to master"

## Delete the test repositories
for repos in ${REPOS}; do delete_repos "${repos}"; done

print_msg "ok\n"

exit 0
