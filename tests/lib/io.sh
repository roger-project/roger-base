print_msg ()
{
    printf "%b" "$1"
}

error_exit ()
{
    printf >&2 "%s\n> %b\n" "ERROR" "$1"
    exit 1
}
