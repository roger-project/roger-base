###
### GitLab repository and authentication
###
PROJECTURL=https://gitlab.com/roger-project
PROJECTNAME=$(basename ${PROJECTURL})
PROJECTID=12410805
APIURL=https://gitlab.com/api/v4
OAUTHTOKEN=$(cat ~/.gitlab/token)

###
### list_repos
###
##  
##  List the names of the repositories in project ${PROJECTNAME}.
##
list_repos ()
{
    curl --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	 --silent \
	 --url "${APIURL}/groups/${PROJECTNAME}/projects?simple=yes" | \
	grep -E -o '"name":"[^"]*"' | \
	awk 'BEGIN { FS="\"" } { print $4 }'
}

###
### delete_repos <repos>
###
##  
##  Delete repository <repos> in project ${PROJECTNAME}.
##
delete_repos ()
{
    local repos="$1"
    
    curl --request DELETE \
	 --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	 --output /dev/null --silent \
	 --url "${APIURL}/projects/${PROJECTNAME}%2F${repos}"
}

###
### create_repos <repos>
###
##  
##  Create repository <repos> in project ${PROJECTNAME}, initialized
##  with a 'README.md' file and a default branch 'master'.
##
create_repos ()
{
    local repos="$1"
    
    curl --request POST \
	 --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	 --header "Content-type: application/json" \
	 --data "{ \"name\": \"${repos}\", \"namespace_id\": \"${PROJECTID}\", \"initialize_with_readme\": \"true\", \"default_branch\": \"master\", \"visibility\": \"private\" }" \
	 --output /dev/null --silent \
	 --url "${APIURL}/projects/"
}

###
### create_branch <repos> <branch> <from>
###
##  
##  Create the branch <branch> from the branch <from> in repository
##  <repos> of project ${PROJECTNAME}.
##
create_branch ()
{
    local repos="$1"
    local branch="$2"
    local from="$3"
    
    curl --request POST \
	 --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	 --output /dev/null --silent \
	 --url "${APIURL}/projects/${PROJECTNAME}%2F${repos}/repository/branches?branch=${branch}&ref=${from}"
}
