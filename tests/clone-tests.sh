#!/bin/sh

## Get the absolute path of this script
SCRIPTPATH="$(cd -- "$(dirname "$0")" >/dev/null 2>&1; pwd -P)"

## Put build directory ahead of system path
PATH=${SCRIPTPATH%/*}/inst/usr/local/bin:${PATH}

## Use local translations
LOCALTEXTDOMAINDIR=${SCRIPTPATH%/*}/inst/usr/local/share/locale

## Define utility functions
. "${SCRIPTPATH}/lib/io.sh"
. "${SCRIPTPATH}/lib/repos.sh"

## Temporary directory to hold files
TESTSDIR="$(mktemp -d 2>/dev/null || mktemp -d -t roger-tests)"
# TESTSDIR=testsdir
# [ -d $TESTSDIR ] && rm -rf $TESTSDIR
# mkdir $TESTSDIR

## Names of test repositories
REPOS="1_roger_test 2_roger_test"

print_msg "Setting up clone tests... "

## Sample rogerrc file
cat > ${TESTSDIR}/rogerrc <<EOF
machine gitlab.com api gitlab
EOF

run_test ()
{
    TEXTDOMAINDIR=$LOCALTEXTDOMAINDIR roger clone --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" "$@"
}

print_msg "setting up test repositories... "

## Create test repositories
for repos in ${REPOS}
do
    if list_repos | grep -q "${repos}"
    then
	delete_repos "${repos}"
    fi
    create_repos "${repos}"
done

print_msg "running tests... "

## Test with a rogerrc file
(cd ${TESTSDIR} >/dev/null 2>&1 && run_test -q --rogerrc-file=rogerrc '[0-9]_roger_test' roger-project) &&
    {
	for repos in ${REPOS}
	do
	    ls -1d ${TESTSDIR}/*/ | grep -q -F "${repos}"
	done
    } ||
	error_exit "with a rogerrc file"

## Test with the --machine option
rm -rf ${TESTSDIR}/*/
(cd ${TESTSDIR} >/dev/null 2>&1 && run_test -q --rogerrc-file=rogerrc --machine="gitlab.com" '[0-9]_roger_test' roger-project) &&
    {
	for repos in ${REPOS}
	do
	    ls -1d ${TESTSDIR}/*/ | grep -q -F "${repos}"
	done
    } ||
	error_exit "with the --machine option"

## Test with the --api option
rm -rf ${TESTSDIR}/*/
(cd ${TESTSDIR} >/dev/null 2>&1 && run_test -q --rogerrc-file=rogerrc --api=gitlab '[0-9]_roger_test' roger-project) &&
    {
	for repos in ${REPOS}
	do
	    ls -1d ${TESTSDIR}/*/ | grep -q -F "${repos}"
	done
    } ||
	error_exit "with the --machine and --api options"

## Test with the --machine and --api options
rm -rf ${TESTSDIR}/*/
(cd ${TESTSDIR} >/dev/null 2>&1 && run_test -q --machine="gitlab.com" --api=gitlab '[0-9]_roger_test' roger-project) &&
    {
	for repos in ${REPOS}
	do
	    ls -1d ${TESTSDIR}/*/ | grep -q -F "${repos}"
	done
    } ||
	error_exit "with the --machine and --api options"

## Delete the test repositories
for repos in ${REPOS}; do delete_repos "${repos}"; done

print_msg "ok\n"

exit 0
