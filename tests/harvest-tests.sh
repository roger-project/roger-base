#!/bin/sh

## Get the absolute path of this script
SCRIPTPATH="$(cd -- "$(dirname "$0")" >/dev/null 2>&1; pwd -P)"

## Put build directory ahead of system path
PATH=${SCRIPTPATH%/*}/inst/usr/local/bin:${PATH}

## Use local translations
LOCALTEXTDOMAINDIR=${SCRIPTPATH%/*}/inst/usr/local/share/locale

## Define utility functions
. "${SCRIPTPATH}/lib/io.sh"

## Temporary directory to hold files
TESTSDIR="$(mktemp -d 2>/dev/null || mktemp -d -t roger-tests)"
# TESTSDIR=testsdir
# [ -d $TESTSDIR ] && rm -rf $TESTSDIR
# mkdir $TESTSDIR

## Names of test directories
REPOS="1_roger_test 2_roger_test"

print_msg "Setting up harvest tests... "

## Sample gradeconf files
cat > ${TESTSDIR}/gradeconf <<EOF
__global
  title: "TESTS"
  defaults
    points_encoding: 1

1-foo.txt
  check_exists_laden: true
  check_encoding: true
  algorithm
    description: "Justesse de l'algorithme [A]"
    points: 20

2-bar.R
  check_exists_laden: true
  check_contents: true
  check_encoding: true
  Expression
    1-results
      description: "Justesse des résultats [R]"
      tests: tests-sc1-resultats.R
      points: 2
    3-style
      description: "Style de codage [S]"
      tests: tests-sc1-style.R
      points: 2.5
EOF

cat > ${TESTSDIR}/gradeconf-with-subtotal <<EOF
__global
  title: "TESTS"
  defaults
    points_encoding: 1

1-foo.txt
  subtotal: true
  check_exists_laden: true
  check_encoding: true
  algorithm
    description: "Justesse de l'algorithme [A]"
    points: 20

2-bar.R
  check_exists_laden: true
  check_contents: true
  check_encoding: true
  Expression
    subtotal: true
    1-results
      description: "Justesse des résultats [R]"
      tests: tests-sc1-resultats.R
      points: 5
    3-style
      description: "Style de codage [S]"
      tests: tests-sc1-style.R
      points: 5
EOF

## Sample results files
for repos in ${REPOS}
do
    [ -d ${TESTSDIR}/${repos} ] && rm -rf ${TESTSDIR}/${repos}
    mkdir ${TESTSDIR}/${repos}
    cat > ${TESTSDIR}/${repos}/GRADING.txt <<EOF
[Directory: ${repos}]

TESTS

### foo.txt
Character encoding                                     1/1
Justesse de l'algorithme [A]                         10/20

### bar.R
Character encoding                                     0/1

## Expression
Justesse des résultats [R]                             5/5
Style de codage [S]                                  3.5/5

### Comments

EOF
    sed -e 's/Directory/Répertoire/' \
	-e 's/Character encoding/Codage de caractères/' \
	-e 's/Comments/Commentaires/' \
	${TESTSDIR}/${repos}/GRADING.txt > ${TESTSDIR}/${repos}/CORRECTION.txt
    cp ${TESTSDIR}/${repos}/GRADING.txt ${TESTSDIR}/${repos}/GRADING-alt.txt
    cp ${TESTSDIR}/${repos}/CORRECTION.txt ${TESTSDIR}/${repos}/CORRECTION-alt.txt
done

## Expected results
for repos in ${REPOS}
do
    cat >> ${TESTSDIR}/EXPECTED <<EOF
${repos};1;10;0;5;3.5
EOF
    cat >> ${TESTSDIR}/EXPECTED_WITH_SUBTOTAL <<EOF
${repos};11;0;8.5
EOF
done

run_test ()
{
    TEXTDOMAINDIR=$LOCALTEXTDOMAINDIR roger harvest "$@"
}

print_msg "running tests... "

## Test with default configuration (English locale)
(cd ${TESTSDIR} >/dev/null 2>&1 &&
     LANGUAGE=en run_test ${REPOS} > RESULTS &&
     diff -q EXPECTED RESULTS) ||
    error_exit "default configuration (en)"

## Test with default configuration (French locale)
(cd ${TESTSDIR} >/dev/null 2>&1 &&
     LANGUAGE=fr run_test ${REPOS} > RESULTS &&
     diff -q EXPECTED RESULTS) ||
    error_exit "default configuration (fr)"

## Test with alternate results file (English locale)
(cd ${TESTSDIR} >/dev/null 2>&1 &&
     LANGUAGE=en run_test -f GRADING-alt.txt ${REPOS} > RESULTS &&
     diff -q EXPECTED RESULTS) ||
    error_exit "alternate results file (en)"

## Test with alternate results file (French locale)
(cd ${TESTSDIR} >/dev/null 2>&1 &&
     LANGUAGE=fr run_test -f CORRECTION-alt.txt ${REPOS} > RESULTS &&
     diff -q EXPECTED RESULTS) ||
    error_exit "alternate results file (fr)"

## Test with specific configuration and subtotal (English locale)
LANGUAGE=en run_test -c ${TESTSDIR}/gradeconf-with-subtotal ${TESTSDIR}/*/ > ${TESTSDIR}/RESULTS &&
    diff -q ${TESTSDIR}/EXPECTED_WITH_SUBTOTAL ${TESTSDIR}/RESULTS ||
    error_exit "specific configuration and subtotal (en)"

## Test with specific configuration and subtotal (French locale)
LANGUAGE=fr run_test -c ${TESTSDIR}/gradeconf-with-subtotal ${TESTSDIR}/*/ > ${TESTSDIR}/RESULTS &&
    diff -q ${TESTSDIR}/EXPECTED_WITH_SUBTOTAL ${TESTSDIR}/RESULTS ||
    error_exit "specific configuration and subtotal (fr)"

## Test with alternate results, specific configuration and subtotal (English locale)
LANGUAGE=en run_test -f GRADING-alt.txt -c ${TESTSDIR}/gradeconf-with-subtotal ${TESTSDIR}/*/ > ${TESTSDIR}/RESULTS &&
    diff -q ${TESTSDIR}/EXPECTED_WITH_SUBTOTAL ${TESTSDIR}/RESULTS ||
    error_exit "alternate results, specific configuration and subtotal (en)"

## Test with alternate results, specific configuration and subtotal (French locale)
LANGUAGE=fr run_test -f CORRECTION-alt.txt -c ${TESTSDIR}/gradeconf-with-subtotal ${TESTSDIR}/*/ > ${TESTSDIR}/RESULTS &&
    diff -q ${TESTSDIR}/EXPECTED_WITH_SUBTOTAL ${TESTSDIR}/RESULTS ||
    error_exit "alternate results, specific configuration and subtotal (fr)"

print_msg "ok\n"

exit 0
