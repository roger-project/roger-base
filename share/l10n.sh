## Localization setup
. gettext.sh

TEXTDOMAIN="@main@"
export TEXTDOMAIN
TEXTDOMAINDIR=${TEXTDOMAINDIR:-"@localedir@"}
export TEXTDOMAINDIR

## Localization of common or shared messages
msg_no_directory="$(gettext "no directory specified")"
msg_config_not_found="$(gettext "configuration file not found")"
msg_missing_rogerrc="$(gettext "rogerrc file not found")"
msg_invalid_exit_code="$(gettext "invalid module exit code")"

msg_directory_id="$(gettext "Directory")"
msg_checkout_by_tag="$(gettext "Checking out repository tag")"
msg_checkout_by_date="$(gettext "Checking out repository version as of")"
msg_comments="$(gettext "Comments")"
msg_grading_repository="$(gettext "Grading of the repository")"
msg_valid_repository="$(gettext "Valid repository")"
msg_grading_sha="$(gettext "Grading based on commit")"

export msg_ok="$(gettext "ok")"
export msg_error="$(gettext "ERROR")"
export msg_note="$(gettext "NOTE")"
export msg_warning="$(gettext "WARNING")"

msg_branch_not_found="$(gettext "Branch not found in the repository")"
msg_file_not_found="$(gettext "File not found")"
msg_file_empty="$(gettext "Empty file")"
msg_dir_empty="$(gettext "Empty directory")"
msg_contents_invalid="$(gettext "Invalid content")"
msg_missing_branch_name="$(gettext "missing branch name")"
msg_error_switching_to="$(gettext "error switching to reference")"
msg_error_switching="$(gettext "error switching branch")"

grading_file="$(gettext "GRADING.txt")"
commit_msg="$(gettext "Add grading results")"
