## Localization setup
. gettext.sh

TEXTDOMAIN="@main@"
export TEXTDOMAIN
TEXTDOMAINDIR=${TEXTDOMAINDIR:-"@localedir@"}
export TEXTDOMAINDIR

msg_missing_tests_file="$(gettext "missing tests file")"
